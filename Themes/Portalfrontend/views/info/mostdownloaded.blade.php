@extends('layouts.datatables')

@section('title')
    Más descargados | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
@stop

@section('content')
    <div class="container">
    <div class="modal fade modal-fullscreen force-fullscreen" id="docModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-reader-dialog">
    <div class="modal-content modal-reader-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Lectura del documento:</h4>
    </div>
    <div class="modal-body">
    <div class="box box-default box-solid reader-box">
    <div class="box-body no-padding">
    <!--<embed id="" type="application/pdf" width="100%" height="700px" src=""></embed>-->
    <iframe id="reader-iframe" src="" width="100%"></iframe>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    </div>
    </div>
    </div>

    @if ($data === null || $data === '')
        @include('layouts/404')
    @else
        <div class="row">
        <div class="col-md-12">
        <div class="box box-info box-solid">
        <div class="box-header with-border">
        <h3 class="box-title">Top 10 de documentos más descargados:</h3>
        </div>
        <div class="box-body">
        <div class="col-md-12">
        <div class="box-body table-responsive">
        <table class="documents-table table table-bordered table-striped">
        <thead>
        <tr>
        <th class="align-center">Tipo de documento</th>
        <th class="align-center">Nombre</th>
        <th class="align-center">Fecha de actualización</th>
        <th class="align-center">Número de descargas</th>
        <th class="align-center">Acciones</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($data as $document)
                <tr>
                    <td class="align-center">
                        <a href="{{ '/descargar/3/'.$document->document_id.'/'.$document->name.'/'.$document->date }}" download="{{ $document->name }}"><i class="fa fa-file-pdf-o fa-3x color-red"></i></a>
                    </td>
                    
                    <td>
                        <p>{{ $document->name }}</p>
                    </td>
                    
                    <td class="align-center">
                        <p>{{ $document->date }}</p>
                    </td>

                    <td class="align-center">
                        <p><b>{{ $document->downloads }}</b></p>
                    </td>
                    
                    <td>
                        <a title="Ver documento" href="#" data-id="{{ $document->document_id }}" class="btn btn-warning view-doc"><i class="fa fa-eye"></i></a>

                        <a title="Descargar documento" href="{{ '/descargar/3/'.$document->document_id.'/'.$document->name.'/'.$document->date }}" download="{{ $document->name }}" class="btn btn-success"><i class="fa fa-download"></i></a>

                        <a title="Compartir en Facebook" href="{{ 'https://www.facebook.com/sharer/sharer.php?u='.$server.'/lectura/'.$document->document_id }}" target="_blank" class="btn btn-primary"><i class="fa fa-facebook-square color-white"></i></a>

                        <a title="Compartir en Twitter" href="{{ 'https://twitter.com/home?status='.$server.'/lectura/'.$document->document_id }}" target="_blank" class="btn btn-info"><i class="fa fa-twitter color-white"></i></a>
                    </td>
                </tr>
        @endforeach
        </tbody>
        </table>
        </div>
        </div>
        </div>
        </div>
        </div>
        </div>
    @endif
    </div>
@stop
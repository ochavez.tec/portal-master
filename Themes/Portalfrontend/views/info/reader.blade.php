@extends('layouts.reader')

@section('title')
    Lectura | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
@stop

@section('content')
    <div class="container">
    	<div class="row">
    		<div class="col-md-12">
	          <div class="box box-primary box-solid">
	            <div class="box-header with-border">
	              <h3 class="box-title"></h3>
	            </div>
	            <div class="box-body" style="display:block;padding:0;">
	            	<iframe id="reader-iframe" src ="{{ asset('/pdfviewer/web/viewer.html?file=/documento/'.$pdf) }}" width="100%"></iframe>
	            </div>
	          </div>
	        </div>
    	</div>    	
    </div>
@stop
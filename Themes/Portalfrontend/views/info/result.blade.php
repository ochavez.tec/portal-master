@extends('layouts.datatables')

@section('title')
    Resultado | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
@stop

@section('content')
    <div class="container">
    <div class="modal fade modal-fullscreen force-fullscreen" id="docModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-reader-dialog">
    <div class="modal-content modal-reader-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Lectura del documento:</h4>
    </div>
    <div class="modal-body">
    <div class="box box-default box-solid reader-box">
    <div class="box-body no-padding">
    <!--<embed id="" type="application/pdf" width="100%" height="700px" src=""></embed>-->
    <iframe id="reader-iframe" src="" width="100%"></iframe>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    </div>
    </div>
    </div>
    <!-- End Modal -->

    <div class="modal fade modal-default" id="attachsModal">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span></button>
    <h4 class="modal-title">Archivos adjuntos:</h4>
    </div>
    <div class="modal-body">
    <div class="table-responsive">
    <table class="table table-bordered table-striped">
    <thead>
    <tr>
    <th class="align-center">Tipo de archivo</th>
    <th class="align-center">Nombre del archivo</th>
    <th class="align-center">Comentario</th>
    <th class="align-center">Acción</th>
    </tr>
    </thead>
    <tbody id="tbody-attachments">
    </tbody>
    </table>    
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    </div>    
    </div>
    </div> 
    <!-- End Modal -->

    @if(empty($result))
      @include('layouts/404')
    @else

    <div class="row">
    <div class="col-md-12">
    <div class="box box-info box-solid">
    <div class="box-header with-border">
    <h3 class="box-title">Resultado de la búsqueda:</h3>
    </div>
    <div class="box-body">
    <div class="col-md-12">
    <div class="box-body table-responsive">
    <table class="table table-bordered table-striped" id="documents-result-table">
    <thead>
    <tr>
    <th class="align-center">Tipo de archivo</th>
    <th class="align-center">Nombre del documento</th>
    <th class="align-center">Fecha de actualización</th>
    <th class="align-center">Vigente hasta</th>
    <th class="align-center">Unidad productora</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($result as $document)
        @if ($document['actual_version_status'] == '2')
            <tr>
                <td class="align-center">
                    @if ($document['mimetype'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                        <a href="{{ '/descargar/2/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] }}" download="{{ $document['name'] }}"><i class="fa fa-file-word-o fa-2x color-skyblue"></i></a>
                    @elseif ($document['mimetype'] == 'application/msword')
                        <a href="{{ '/descargar/1/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] }}" download="{{ $document['name'] }}"><i class="fa fa-file-word-o fa-2x color-blue"></i></a>
                    @elseif ($document['mimetype'] == 'application/pdf')
                        <a href="{{ '/descargar/3/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] }}" download="{{ $document['name'] }}"><i class="fa fa-file-pdf-o fa-2x color-red"></i></a>
                    @endif
                </td>
                
                <td>
                    <p>{{ $document['name'] }}</p>
                </td>
                
                <td class="align-center">
                    <p>{{ $document['actual_version_workflow_date_updated'] }}</p>
                </td>

                <td class="align-center">
                    <p>{{ $document['expires'] }}</p>
                </td>

                <td class="align-center">
                    <p>{{ $document['publisher_group'] }}</p>
                </td>
                
                <td>
                    <a tabindex="0" role="button" class="btn btn-info" data-toggle="popover" data-placement="left" data-trigger="focus" title="Checksum: Código de integridad del documento. Permite verificar que el contenido del documento no ha sido modificado durante la descarga." data-content="{{ $document['checksum'] }}"><i class="fa fa-lock"></i></a>
                    @if ($document['mimetype'] == 'application/pdf')
                        <a title="Ver documento" type="button" class="btn btn-warning view-doc" data-id="{{ $document['id'] }}"><i class="fa fa-eye"></i></a>
                    @endif

                    <div class="btn-group">
                      <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class="fa fa-bars"></span>
                        <span class="sr-only">Opciones</span>
                      </button>
                      <ul class="dropdown-menu dropdown-menu-right" role="menu">
                        @if ($document['attachments'])
                            <li><a href="#" data-id="{{ $document['id'] }}" class="see-attachments text-light-blue"><i class="fa fa-search"></i> Ver adjuntos</a></li>
                        @endif

                        @if ($document['mimetype'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                            <li><a title="Descargar documento" href="{{ '/descargar/2/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] }}" download="{{ $document['name'] }}" class="text-green"><i class="fa fa-download"></i> Descargar documento</a></li>
                        @elseif ($document['mimetype'] == 'application/msword')
                            <li><a title="Descargar documento" href="{{ '/descargar/1/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] }}" download="{{ $document['name'] }}" class="text-green"><i class="fa fa-download"></i> Descargar documento</a></li>
                        @elseif ($document['mimetype'] == 'application/pdf')
                            <li><a title="Descargar documento" href="{{ '/descargar/3/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] }}" download="{{ $document['name'] }}" class="text-green"><i class="fa fa-download"></i> Descargar documento</a></li>
                        @endif

                        <li><a title="Compartir en Facebook" href="{{ 'https://www.facebook.com/sharer/sharer.php?u='.$server.'/lectura/'.$document['id'] }}" target="_blank" class="text-light-blue"><i class="fa fa-facebook-square"></i> Compartir en Facebook</a></li>

                        <li><a title="Compartir en Twitter" href="{{ 'https://twitter.com/home?status='.$server.'/lectura/'.$document['id'] }}" target="_blank" class="text-aqua"><i class="fa fa-twitter"></i> Compartir en Twitter</a></li>

                      </ul>
                    </div>
                </td>
            </tr>
        @endif
    @endforeach

    </tbody>
    </table>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    @endif
@stop

@push('js-stack')
<script>
    
</script>
@endpush
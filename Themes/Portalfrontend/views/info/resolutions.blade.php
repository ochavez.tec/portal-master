@extends('layouts.datatables')

@section('title')
    Resoluciones | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
@stop

@section('content')
    <div class="container">
    <div class="modal fade modal-fullscreen force-fullscreen" id="docModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-reader-dialog">
    <div class="modal-content modal-reader-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Resolución</h4>
    </div>
    <div class="modal-body">
    <div class="box box-default box-solid reader-box">
    <div class="box-body no-padding">
    <!--<embed id="embedPDF" type="application/pdf" width="100%" height="700px" src="" onload="console.log('Hola!');"></embed>-->
    <iframe id="reader-iframe" src="" width="100%"></iframe>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    </div>
    </div>
    </div>
    @if ($subfolders === null || $subfolders === '')
      @include('layouts/404')
    @else
      @foreach ($subfolders as $period)
        <div class="row">
        <div class="col-md-12">
        <div class="box box-primary box-solid">
        <div class="box-header with-border">
        <h3 class="box-title">{{ $period['name'] }}</h3>
        <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
        </div>
        <div class="box-body">
        @isset($period['subfolders'])
          @foreach ($period['subfolders'] as $month)
            <div class="col-md-12">
            <div class="box box-info collapsed-box">
            <div class="box-header with-border">
            <h3 class="box-title">{{ $month['name'] }}</h3>
            <div class="box-tools pull-right width-full">
            <button type="button" class="btn btn-box-tool width-full" data-widget="collapse"><i class="fa fa-plus float-right"></i></button>
            </div>
            </div>
            <div class="box-body no-padding">
            <div class="box no-border">
            <div class="box-body table-responsive">
            <table class="documents-table table table-bordered table-striped">
            <thead>
            <tr>
            <th class="align-center">Tipo de archivo</th>
            <th class="align-center">Resolución</th>
            <th class="align-center">Fecha de actualización</th>
            <th class="align-center">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @isset($month['documents'])
              @foreach($month['documents'] as $document)
                @if ($document['actual_version_status'] == '2')
                    <tr>
                    <td class="align-center"><a href="#" data-id="{{ $document['id'] }}" class="view-doc"><i class="fa fa-file-pdf-o fa-3x color-red"></i></a></td>
                    <td>
                        <a href="#" data-id="{{ $document['id'] }}" class="view-doc">{{ $document['name'] }}</a></td>
                    <td class="align-center">{{ $document['actual_version_workflow_date_updated'] }}</td>
                    <td>
                        <a title="Ver documento" href="#" data-id="{{ $document['id'] }}" class="btn btn-warning view-doc"><i class="fa fa-eye"></i></a>
                        
                        <a title="Descargar documento" href="{{ '/descargar/'.$document['id'] }}" download="{{ $document['name'] }}" class="btn btn-success"><i class="fa fa-download"></i></a>

                        <a title="Compartir en Facebook" href="{{ 'https://www.facebook.com/sharer/sharer.php?u='.$server.'/lectura/'.$document['id'] }}" target="_blank" class="btn btn-primary"><i class="fa fa-facebook-square color-white"></i></a>

                        <a title="Compartir en Twitter" href="{{ 'https://twitter.com/home?status='.$server.'/lectura/'.$document['id'] }}" target="_blank" class="btn btn-info"><i class="fa fa-twitter color-white"></i></a>
                    </td>
                    </tr>
                @endif
              @endforeach
            @endisset
            </tbody>
            </table>
            </div>
            </div>
            </div>
            </div>
            </div>
          @endforeach
        @endisset
        </div>
        </div>
        </div>
        </div>
      @endforeach
    @endif
    </div>
@stop
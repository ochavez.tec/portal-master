@extends('layouts.datatables')

@section('title')
    Gestión Financiera | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
@stop

@push('css-stack')
<style>
    #div-gf {
        border:1px solid #b5b5b5;
    }

    #gf {
        opacity: 0.5;
    }
</style>
@endpush

@section('content')
    <div class="container">

    <div class="modal fade modal-fullscreen force-fullscreen" id="docModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-reader-dialog">
    <div class="modal-content modal-reader-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Lectura del documento:</h4>
    </div>
    <div class="modal-body">
    <div class="box box-default box-solid reader-box">
    <div class="box-body no-padding">
    <iframe id="reader-iframe" src="" width="100%"></iframe>
    </div>
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    </div>
    </div>
    </div>
    <!-- End Modal -->

    <div class="modal fade modal-default" id="attachsModal">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span></button>
    <h4 class="modal-title">Archivos adjuntos:</h4>
    </div>
    <div class="modal-body">
    <div class="table-responsive">
    <table class="table table-bordered table-striped">
    <thead>
    <tr>
    <th class="align-center">Tipo de archivo</th>
    <th class="align-center">Nombre del archivo</th>
    <th class="align-center">Comentario</th>
    <th class="align-center">Acción</th>
    </tr>
    </thead>
    <tbody id="tbody-attachments">
    </tbody>
    </table>    
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    </div>    
    </div>
    </div> 
    <!-- End Modal -->

    @if ($output === null || $output === '')
      @include('layouts/404')
    @else

    {!! Block::get('gestion-menu') !!}

    {!! Block::get('texto-financiera') !!}

    <div class="row">
    <div class="col-md-12">
        <?php echo $output; ?>
    </div>
    </div>

    @endif
    </div>
@stop

@push('js-stack')
<script>
    
</script>
@endpush
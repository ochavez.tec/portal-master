@extends('layouts.charts')

@section('title')
    Diamante de Transparencia
@stop

@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
@stop

@section('content')
    <div class="container">
        <div class="modal fade modal-fullscreen force-fullscreen" id="docModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-reader-dialog">
        <div class="modal-content modal-reader-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Lectura del documento:</h4>
        </div>
        <div class="modal-body">
        <div class="box box-default box-solid reader-box">
        <div class="box-body no-padding">
        <!--<embed id="" type="application/pdf" width="100%" height="700px" src=""></embed>-->
        <iframe id="reader-iframe" src="" width="100%"></iframe>
        </div>
        </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
        </div>
        </div>
        <!-- End Modal -->
    </div>

    <div class="row">
    	{!! Block::get('texto-diamante') !!}
    </div>

	<div class="row">
    	<div class="col-sm-4 col-xs-12"></div>
        <div class="col-sm-4 col-xs-12">
        	{!! Form::open(['route' => 'diamond.update', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
        	<div class="input-group date">
                <div class="input-group-addon">
                	<i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="date" class="form-control pull-right" data-inputmask="'alias': 'mm/yyyy'" data-mask id="diamond-date" /> 
            </div>
            <div class="align-center">
            	<br/>
            	<button type="submit" class="btn btn-success">Consultar</button>
            </div>
            {!! Form::close() !!}
        </div>    
    	<div class="col-sm-4 col-xs-12"></div>
    </div>
    <br/>
    <div class="row">
    	<div class="col-sm-2 col-xs-12"></div>
        <div class="col-sm-8 col-xs-12">
    		{!! $chartjs->render() !!}
		</div>
		<div class="col-sm-2 col-xs-12"></div>
    </div>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo $all_variables; ?>
            </div>
        </div>
    </div>
    <br>
    <div class="container">
    <div class="row">
    @foreach($data as $axe)
        <div class="col-md-12">
            <div class="panel panel-info">
            <div class="panel-heading">
            <h3 class="panel-title">Documentos publicados que se encuentran involucrados en el eje: {{ $axe->name }}</h3>
            </div>
            <div class="panel-body table-responsive">
                <table class="documents-table table table-bordered table-striped">
                <thead>
                <tr>
                <th class="align-center">Tipo de archivo</th>
                <th class="align-center">Nombre del documento</th>
                <th class="align-center">Fecha de actualización</th>
                <th class="align-center">Ver</th>
                </tr>
                </thead>
                <tbody>
                @if (isset($axe->docs))
                    @foreach ($axe->docs as $document)
                    <tr>
                    <td class="align-center">
                        <a href="{{ '/descargar/3/'.$document->id.'/'.$document->name.'/'.$document->updated_date }}" download="{{ $document->name }}"><i class="fa fa-file-pdf-o fa-2x color-red"></i></a>
                    </td>
                    <td>
                        <p>{{ $document->name }}</p>
                    </td>
                    <td class="align-center">
                        <p>{{ $document->updated_date }}</p>
                    </td>
                    <td>
                        <a title="Ver documento" href="#" data-id="{{ $document->id }}" class="btn btn-warning view-doc"><i class="fa fa-eye"></i></a>
                    </td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
                </table>
           
            </div>
            </div>
        </div>
    @endforeach
    </div>
    </div>
@stop

@push('js-stack')
<script>
    $(".view-doc").click(function(e){
        var doc = $(this).attr("data-id")

        $("#reader-iframe").attr("src", "");
        document.getElementById('reader-iframe').contentWindow.location.reload();
        
        $("#reader-iframe").attr("src", "/laraview/#../documento/"+doc);
        $('#docModal').modal('show');

    });

</script>
@endpush

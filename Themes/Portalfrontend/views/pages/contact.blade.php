@extends('layouts.master')

@section('title')
    {{ $page->title }} | @parent
@stop
@section('meta')
    <meta name="title" content="{{ $page->meta_title}}" />
    <meta name="description" content="{{ $page->meta_description }}" />
@stop

@section('content')
    <div class="container">
    	<div class="col-md-12">
			<div class="box box-primary box-solid">
				<div class="box-header">
					<h4 class="box-title">Consulta ciudadana</h4>
				</div>
				<div class="box-body">

					<div class="col-md-12">
						@if (Session::has('error'))
						   <div class="alert alert-danger"><p class="color-black">{{ Session::get('error') }}</p></div>
						@endif
						@if (Session::has('success'))
						   <div class="alert alert-success"><p class="color-black">{{ Session::get('success') }}</p></div>
						@endif

				    	{!! Form::open(['route' => 'consultation.store', 'method' => 'POST', 'id' => 'cc_form', 'class' => 'form-horizontal']) !!}
				    		<div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Nombre:</label>
				                <div class="col-sm-7 col-xs-12">
				                    <input type="text" class="form-control" name="name" data-toggle="tooltip" data-placement="auto" title="Ingrese su nombre" maxlength="50" value="{{ old('name') }}" required="required"/>
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Email:</label>
				                <div class="col-sm-7 col-xs-12">
				                    <input type="email" class="form-control" name="email" data-toggle="tooltip" data-placement="auto" title="Ingrese su dirección de correo electrónico" maxlength="50" value="{{ old('email') }}" required="required"/>
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Teléfono:</label>
				                <div class="col-sm-7 col-xs-12">
				                    <input type="text" class="form-control phone-number" name="phone" data-toggle="tooltip" data-placement="auto" title="Ingrese su número de teléfono" maxlength="50" value="{{ old('phone') }}" required="required"/>
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Asunto:</label>
				                <div class="col-sm-7 col-xs-12">
				                    <input type="text" class="form-control" name="subject" data-toggle="tooltip" data-placement="auto" title="Ingrese el asunto de su consulta" maxlength="50" value="{{ old('subject') }}" required="required"/>
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Mensaje: </label>
				                <div class="col-sm-7 col-xs-12">
				                    <textarea class="form-control" name="description" rows="6" data-toggle="tooltip" data-placement="auto" title="Describa su consulta" maxlength="500" required="required">{{ old('description') }}</textarea>
				                </div>
				            </div>

				            <div class="form-group">
				            	<div class="col-sm-10 col-xs-12">
				            		<div class="pull-right">{!! NoCaptcha::display(['data-size' => 'compact']) !!}</div>
				            	</div>
				            	<div class="col-sm-10 col-xs-12">
				            		<button type="submit" class="btn btn-primary pull-right">Enviar</button>
				            	</div>
				            </div>

				    	{!! Form::close() !!}

					</div>
				</div>
				<div class="overlay hidden" id="div-overlay"><i class="fa fa-refresh fa-spin"></i></div>
			</div>
		</div>
    	{!! $page->body !!}
    </div>
@stop

{!! NoCaptcha::renderJs() !!}
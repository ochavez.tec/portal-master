<!DOCTYPE html>
<html>
<head>
<style>
table { border-collapse: separate; border-spacing: 0; }
td {
    border: solid 1px #000;
    border-style: none solid solid none;
    padding: 10px;
}
tr:first-child td:first-child { border-top-left-radius: 10px; }
tr:first-child td:last-child { border-top-right-radius: 10px; }
tr:last-child td:first-child { border-bottom-left-radius: 10px; }
tr:last-child td:last-child { border-bottom-right-radius: 10px; }
tr:first-child td { border-top-style: solid; }
tr td:first-child { border-left-style: solid; }

.align-center { text-align: center; }
.no-padding { padding: 0; }
/*.td-data { padding: 0 10px 0 10px; }*/
.section-title { font-weight: bold; font-size: 16px; }
p { margin:0; padding-top:0px; padding-bottom:0px; }
span { margin:0; padding-top:0px; padding-bottom:0px; }
.page-break { page-break-after: always; }

</style>
</head>
<body>

<table style="width:100%">
  <tr>
    <td width="30%">
      <div class="align-center">
        <img class="align-center" src="{{ asset('/assets/media/logo-csj.png') }}">  
      </div>
    </td>
    <td width="70%">
      <h2 class="align-center">Órgano Judicial de El Salvador</h2>
      <p class="align-center">Unidad de Acceso a la Información Pública Formulario de Solicitud y Entrega de Información Pública</p>
    </td>
  </tr>
</table>
<br/>
<table style="width:100%">
  <tr>
    <td width="90%" class="td-data">
      <div>
        <span>Fecha de presentación de la consulta:</span><span>{{ $request_date }}</span>&nbsp;&nbsp;&nbsp;<span>Hora: {{ $request_hour }}</span>
      </div>
    </td>
    <td width="10%" class="td-data align-center" >
      <p>Correlativo</p>
      <p>{{ $request->correlative."-".$request->year }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" colspan="2" class="align-center">
      <p class="section-title">Datos del solicitante</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Nombre:</b> {{ $person->name }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Teléfono:</b> {{ $person->phone }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Email:</b> {{ $person->email }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Asunto:</b> {{ $request->subject }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Descripción:</b> {{ $request->description }}</p>
    </td>
  </tr>
</table>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
<style>
table { border-collapse: separate; border-spacing: 0; }
td {
    border: solid 1px #000;
    border-style: none solid solid none;
    padding: 10px;
}
tr:first-child td:first-child { border-top-left-radius: 10px; }
tr:first-child td:last-child { border-top-right-radius: 10px; }
tr:last-child td:first-child { border-bottom-left-radius: 10px; }
tr:last-child td:last-child { border-bottom-right-radius: 10px; }
tr:first-child td { border-top-style: solid; }
tr td:first-child { border-left-style: solid; }

.align-center { text-align: center; }
.no-padding { padding: 0; }
/*.td-data { padding: 0 10px 0 10px; }*/
.section-title { font-weight: bold; font-size: 16px; }
p { margin:0; padding-top:0px; padding-bottom:0px; }
span { margin:0; padding-top:0px; padding-bottom:0px; }
.page-break { page-break-after: always; }

</style>
</head>
<body>

<table style="width:100%">
  <tr>
    <td width="30%">
      <div class="align-center">
        <img class="align-center" src="{{ asset('/assets/media/logo-csj.png') }}">  
      </div>
    </td>
    <td width="70%">
      <h2 class="align-center">Órgano Judicial de El Salvador</h2>
      <p class="align-center">Unidad de Acceso a la Información Pública Formulario de Solicitud y Entrega de Información Pública</p>
    </td>
  </tr>
</table>
<br/>
<table style="width:100%">
  <tr>
    <td width="90%" class="td-data">
      <div>
        <span>Fecha de presentación de la solicitud:</span><span>{{ $request_date }}</span>&nbsp;&nbsp;&nbsp;<span>Hora: {{ $request_hour }}</span>
      </div>
    </td>
    <td width="10%" class="td-data align-center" >
      <p>Correlativo</p>
      <p>{{ $request->correlative."-".$request->year }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" colspan="2" class="align-center">
      <p class="section-title">Datos del solicitante</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Apellido(s):</b> {{ $person->lastname }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Nombre(s):</b> {{ $person->name }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Género:</b> {{ $person->gender }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Ocupación u oficio:</b> {{ $person->occupation }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Teléfono celular:</b> {{ $person_phone->cell_phone }}</p>
      @if($person_phone->home_phone != "")
        <p><b>Teléfono de casa:</b> {{ $person_phone->home_phone }}</p>
      @endif
      @if($person_phone->office_phone != "")
        <p><b>Teléfono de oficina:</b> {{ $person_phone->office_phone }}</p>
      @endif
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Domicilio del solicitante:</b> {{ $person->address }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Tipo de persona:</b> {{ $person->person_type }}</p>
    </td>
  </tr>
  @if($representative != null)
  <tr>
    <td width="100%" class="td-data" colspan="2">
        <p><b>Nombre del representante o apoderado:</b></p>
        <p>{{ $representative->name }}</p>
        <p><b>Domicilio del representante o apoderado:</b></p>
        <p>{{ $representative->address }}</p>
        <p><b>Número de DUI del representante o apoderado:</b></p>
        <p>{{ $representative->dui }}</p>
    </td>
  </tr>
  @endif
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Tipo de documento de identificación del solicitante:</b> {{ $person_document->document_type }}</p>
      <p><b>Número del documento:</b> {{ $person_document->document_number }}</p>
      @if($document_extended_by != '')
        <p><b>Documento extendido por:</b> {{ $document_extended_by }}</p>
      @endif
      <p></p>
    </td>
  </tr>
</table>

<div style="clear:both!important;"/></div>
<div style="page-break-after:always"></div> 
<div style="clear:both!important;"/> </div>

<table style="width:100%">

  @if($request->right_type != null)

  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Tipo de derecho:</b> {{ $request->right_type }}</p>
    </td>
  </tr>

  <tr>
    <td width="100%" colspan="2">
      <p class="section-title align-center">Datos personales a los cuales se necesita tener acceso o que se realice la rectificación, cancelación y oposición:</p>
      <p>{{ $request->description }}</p>
    </td>
  </tr>

  @else
  
  <tr>
    <td width="100%" colspan="2">
      <p class="section-title align-center">Información solicitada</p>
      <p>{{ $request->description }}</p>
    </td>
  </tr>

  @endif

  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Lugar para recibir notificaciones:</b> {{ $request_notification->notification_site }}</p>
    </td>
  </tr>
  <tr>
    <td width="100%" class="td-data" colspan="2">
      <p><b>Formas en que se desea obtener acceso a la información:</b></p>
      <ul>
        @if($information_access->access_one != "")
          <li>Opción 1: {{ $information_access->access_one }}</li>
        @endif
        @if($information_access->access_two != "")
          <li>Opción 2: {{ $information_access->access_two }}</li>
        @endif
      </ul>
    </td>
  </tr>
  <tr>
    <td width="100%" colspan="2">
      <p><b>Correo electrónico para recibir notificaciones:</b> {{ $person->email }}</p>
    </td>
  </tr>
  @if($request_notification->sne_notification)
  <tr>
    <td width="100%" colspan="2">
      <p><b>SNE CEU:</b> {{ $request_notification->sne_ceu }}</p>
    </td>
  </tr>
  @endif
  <tr>
    <td width="100%" colspan="2">
      <p>Notas:</p>
      <ol>
        <li>La forma de entrega estará sujeta a la capacidad técnica de la UAIP.</li>
        <li>En caso de Representante Legal, deberá adjuntar el documento que acredite la representación.</li>
      </ol>
    </td>
  </tr>
  <tr>
    <td width="100%" colspan="2" class="align-center">
      @if(file_exists($temp_path_img.'/signature-'.$request->id_request.'.png'))
        <img src="{{ $temp_path_img.'/signature-'.$request->id_request.'.png' }}">
      @elseif(file_exists($temp_path_img.'/signature-'.$request->id_request.'.jpg'))
        <img src="{{ $temp_path_img.'/signature-'.$request->id_request.'.jpg' }}">
      @endif
      <p>_________________________________________</p>
      <p>Firma del solicitante o representante legal</p>
    </td>
  </tr>
</table>

</body>
</html>

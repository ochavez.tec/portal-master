@extends('layouts.master')

@section('title')
    No encontrado | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
@stop

@section('content')
	<div class="container">
    	<div class="row">
    		<div class="col-md-12">
			    <section class="content-header">
			      <h1>No hemos encontrado ningún resultado</h1>
			    </section>
			    <section class="content">
			      <div class="error-page">
			        <h2 class="headline text-yellow"> 404</h2>
			        <div class="error-content">
			          <h3><i class="fa fa-warning text-yellow"></i> Oops! Ningún elemento ha sido encontrado.</h3>
			          <p>
			            No hemos podido encontrar la página o elemento buscado.
			            Mientras tanto usted podría <a href="/">volver a la página principal</a> o intentar realizar una nueva búsqueda.
			          </p>
			          {!! Form::open(['route' => 'info.search', 'method' => 'POST', 'class' => 'search-form']) !!}
			              <div class="input-group">
			                <input type="text" name="query" class="form-control" placeholder="Buscar...">
			                <div class="input-group-btn">
			                <button type="submit" name="submit" class="btn btn-info btn-flat"><i class="fa fa-search"></i>
			                </button>
			              </div>
			              </div>			              
			        {!! Form::close() !!}
			        </div>
			      </div>
			    </section>
			</div>
    	</div>
   	</div>

@stop


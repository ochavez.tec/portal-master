@extends('layouts.forum')

@section('title')
    Foro de la solicitud | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12">
          @if (Session::has('error'))
             <div class="alert alert-danger"><p class="color-black">{{ Session::get('error') }}</p></div>
          @endif
          @if (Session::has('success'))
             <div class="alert alert-success"><p class="color-black">{{ Session::get('success') }}</p></div>
          @endif
          @if (Session::has('info'))
             <div class="alert alert-info"><p class="color-black">{{ Session::get('info') }}</p></div>
          @endif
          <!-- Box Comment -->
          <div class="box box-default">
          <div class="box-header with-border">
            @if($request->right_type === null)
              <h3 class="box-title">Seguimiento de Solicitud de Información</h3>
            @else 
              <h3 class="box-title">Seguimiento de Solicitud de Datos Personales</h3>
            @endif
          </div>
          <div class="box-body">
          <div class="box box-widget">
            <div class="box-header with-border">
              <div class="user-block">
                @if ($person->gender == "Femenino")
                  <img class="img-circle" src="{{ asset('/assets/media/female.png') }}" alt="User Image">
                @elseif ($person->gender === "Masculino")
                  <img class="img-circle" src="{{ asset('/assets/media/male.png') }}" alt="User Image">
                @endif
                @php
                    $creation_timestamp = strtotime($request->created_at);
                @endphp

                @if((int)$request->correlative != 0)
                  <span class="username"><a href="#">{{ "SIP-".$request->correlative."-".$request->year }}</a></span>
                @else
                  <span class="username"><a href="#">{{ "SIP-".$request->id_request }}</a></span>
                @endif
                
                <span class="description">Fecha de la solicitud: {{ date('d/m/Y H:i:s',$creation_timestamp) }}</span>
              </div>
              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>

            <div class="box-body">
              <p>{{ $request->description }}</p>
            </div>

            <div class="box-footer box-comments" id="comments-box">
              @foreach ($comments as $comment)
                @if ($comment->is_logged == 1)
                  <img class="img-circle img-sm" src="{{ asset('/assets/media/technical.png') }}" alt="User Image">
                @elseif ($comment->is_logged == 0)
                  @if ($person->gender == "Femenino")
                    <img class="img-circle img-sm" src="{{ asset('/assets/media/female.png') }}" alt="User Image">
                  @elseif ($person->gender === "Masculino")
                    <img class="img-circle img-sm" src="{{ asset('/assets/media/male.png') }}" alt="User Image">
                  @endif
                @endif
                <div class="comment-text">
                  @php
                    $timestamp = strtotime($comment->created_at);
                  @endphp
                  @if ($comment->is_logged == 1)
                    <span class="username">Agente de soporte<span class="text-muted pull-right">{{ date('d/m/Y H:i:s',$timestamp) }}</span>
                    </span>
                  @else
                    <span class="username">Persona solicitante<span class="text-muted pull-right">{{ date('d/m/Y H:i:s',$timestamp) }}</span>
                    </span>
                  @endif
                    {{ $comment->message }}
                </div>                
              @endforeach

            </div>

            <div class="">
              <div class="box" style="border-top: 0;">
              {!! Form::open(['route' => 'saveComment', 'method' => 'POST', 'id' => 'request_comment_form']) !!}
                <input type="hidden" name="request_id" id="request_id" value="{{ $request->id_request }}"/>
                <input type="hidden" name="gender" id="gender" value="{{ $person->gender }}"/>
                <div class="table-responsive">
                  <table class="table">
                  <tbody>
                  <tr>
                  <td width="10%" style="text-align:center;">
                    @if ($user_logged == 1)
                      <img class="img-responsive img-circle" src="{{ asset('/assets/media/technical.png') }}" alt="User Image">
                    @elseif ($user_logged == 0)
                      @if ($person->gender == "Femenino")
                        <img class="img-responsive img-circle" src="{{ asset('/assets/media/female.png') }}" alt="User Image">
                      @elseif ($person->gender === "Masculino")
                        <img class="img-responsive img-circle" src="{{ asset('/assets/media/male.png') }}" alt="User Image">
                      @endif
                    @endif
                  </td>
                  <td width="80%">
                    <textarea name="request_comment" style="resize:none;" rows="4" id="request_comment" class="form-control input-sm" placeholder="Escribir nuevo mensaje en el foro" required="true" /></textarea>
                  </td>
                  <td width="10%" style="text-align:center;">
                      <button type="submit" class="btn btn-primary">&nbsp;Enviar</button>
                    </td>
                  </tr>
                  </tbody>
                  </table>
                </div>
              {!! Form::close() !!}
              <div class="overlay hidden" id="div-overlay"><i class="fa fa-refresh fa-spin"></i></div>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>

        <div class="col-md-4 col-xs-12">
          <div class="col-md-12">
            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="fa fa-flag"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Estado de la solicitud</span>
                <span class="info-box-number">{{ $doc_status }}</span>
                
                <span class="status-date">Fecha de actualización: {{ $doc_status_date }}</span>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>
              <div class="info-box-content">
                <span class="info-box-text">Contador de comentarios</span>
                <span class="info-box-number" id="info-box-number">{{ $comments_counter }}</span>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-download"></i>&nbsp;&nbsp;Zona de descargas</h3>
            </div>
            <div class="box-body">
              {!! Form::open(['route' => 'saveDownloadLog', 'method' => 'POST', 'id' => 'save_log']) !!}
              {!! Form::close() !!}
              @if(!empty($attach_files))
                <table class="table table-striped">
                @foreach($attach_files as $attach)                
                  <tr>                    
                                          
                    <?php switch ($attach['file_type']) {

                          case 1: // PDF
                              echo '<td class=""><i class="fa fa-file-pdf-o fa-2x color-red"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">';                              
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/1/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }
                              
                              echo '</td>';
                            break;

                          case 2: // DOCX
                              echo '<td class=""><i class="fa fa-file-word-o fa-2x color-blue"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/2/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';
                              
                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 3: // XLSX
                              echo '<td class=""><i class="fa fa-file-excel-o fa-2x color-green"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/3/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 4: // PPTX
                              echo '<td class=""><i class="fa fa-file-powerpoint-o fa-2x color-orange"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">';
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/4/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';


                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 5: // DOC
                              echo '<td class=""><i class="fa fa-file-word-o fa-2x color-blue"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/5/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 6: // XLS
                              echo '<td class=""><i class="fa fa-file-excel-o fa-2x color-green"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/6/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 7: // PPT
                              echo '<td class=""><i class="fa fa-file-powerpoint-o fa-2x color-orange"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/7/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 8: // RAR
                              echo '<td class=""><i class="fa fa-file-zip-o fa-2x color-brown"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/8/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;
                          
                          case 9: // ZIP
                              echo '<td class=""><i class="fa fa-file-zip-o fa-2x color-brown"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/9/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 10: // MP3
                              echo '<td class=""><i class="fa fa-file-audio-o fa-2x color-gray"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/10/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 11: // WMA
                              echo '<td class=""><i class="fa fa-file-audio-o fa-2x color-gray"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/11/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;
                          
                          case 12: // AVI
                              echo '<td class=""><i class="fa fa-file-movie-o fa-2x color-gray"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/12/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          case 13: // MP4
                              echo '<td class=""><i class="fa fa-file-movie-o fa-2x color-gray"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a class="get-attach" href="/getforumattach/'.$doc_id.'/13/'.$attach['download'].'" rel="'.$attach['download'].'" data-docid="'.$doc_id.'">'.$attach['name'].'</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"><span>'.$attach['comment'].'</span></p>';

                              if (isset($attach['saved_log'])) {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'">Documento descargado por primera vez:&nbsp;'.$attach['saved_log']['created_at'].'</span></p>';  
                              } else {
                                echo '<p style="font-style: italic;font-size: 9px; margin:0;"><span id="span-'.$attach['download'].'"></span></p>';
                              }

                              echo '</td>';
                            break;

                          default:
                              echo '<td class=""><i class="fa fa-file fa-2x color-red"></i></td>';
                              echo '<td class="align-left">';
                              echo '<p style="margin-bottom: 0;">'; 
                              echo '<a href="#">Formato no soportado</a>';
                              echo '</p>';
                              echo '<p style="font-style: italic;font-size: 10px; margin: 0;"></p>';
                              echo '</td>';                      
                            break;
                    } ?>                   
                  </tr>
                @endforeach
                </table>
              @endif
            </div>          
          </div>
          </div>
      </div>
    </div>
@stop
@extends('layouts.form')

@section('title')
    Solicitud | @parent
@stop

@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
@stop

@push('css-stack')
<style>
	.wrapper {
	  position: relative;
	  width:260px;
	  height: 150px;
	  -moz-user-select: none;
	  -webkit-user-select: none;
	  -ms-user-select: none;
	  user-select: none;

	}

	.signature-pad {
	  position: relative;
	  left: 0;
	  top: 0;
	  width:260px;
	  height:150px;
	  border:1px solid #000;
	}

	#signature {
		display: inline-block;
	}

	#signature-msg-empty {
		display: none;
		margin-top: 20px;
		font-size: 18px;
	}

	#signature-msg {
		display: none;
		margin-top: 20px;
		font-size: 18px;
	}

	#sne_ceu_box {
		display: none;
	}
</style>
@endpush

@section('content')
    <div class="container">
    	@if (Session::has('error'))
		   <div class="alert alert-danger"><p class="color-black">{{ Session::get('error') }}</p></div>
		@endif
		@if (Session::has('success'))
		   <div class="alert alert-success"><p class="color-black">{{ Session::get('success') }}</p></div>
		@endif

    	{!! Form::open(['route' => 'request.store', 'method' => 'POST', 'id' => 'requestForm', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) !!}
		<ul class="nav nav-pills">
		    <li class="active"><a href="#first-tab" data-toggle="tab">Paso 1 <span class="glyphicon glyphicon-chevron-right"></span></a></li>
		    <li><a href="#second-tab" data-toggle="tab">Paso 2 <span class="glyphicon glyphicon-chevron-right"></span></a></li>
		</ul>
		<div class="box">
		    <div class="tab-content">
		        <!-- First tab -->
		        <div class="tab-pane active" id="first-tab">
		            <h4>Datos del solicitante</h4>
		            <br>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Nombre(s):</label>
		                <div class="col-sm-7 col-xs-12">
		                    <input type="text" class="form-control" name="name" data-toggle="tooltip" data-placement="auto" title="Ingrese el o los nombres de la persona solicitante" value="{{ old('name') }}" maxlength="50"/>
		                </div>
		            </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Apellido(s):</label>
		                <div class="col-sm-7 col-xs-12">
		                    <input type="text" class="form-control" name="lastname" data-toggle="tooltip" data-placement="auto" title="Ingrese el o los apellidos de la persona solicitante" value="{{ old('lastname') }}" maxlength="50"/>
		                </div>
		            </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Género:</label>
		                <div class="col-sm-7 col-xs-12">
		                    <div class="form-group">
			                  <div class="radio">
			                    <label data-toggle="tooltip" data-placement="right" title="Seleccione su género">
			                      <input type="radio" name="gender" id="" value="Masculino">
			                      Masculino
			                    </label>
			                  </div>
			                  <div class="radio">
			                    <label data-toggle="tooltip" data-placement="right" title="Seleccione su género">
			                      <input type="radio" name="gender" id="" value="Femenino">
			                      Femenino
			                    </label>
			                  </div>
			                </div>
		                </div>
		            </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Tipo de persona:</label>
		                <div class="col-sm-7 col-xs-12">
			               	<select name="person_type" class="form-control" id="people_type" data-toggle="tooltip" data-placement="auto" title="Seleccione que tipo de persona es el solicitante">
				                <option value="1" selected="selected">Natural</option>
				                <option value="2">Jurídica</option>
				                <option value="3">Natural con representante</option>
				            </select>
		                </div>
		            </div>

		            <div id="have_representant" class="panel panel-primary">
		            	<div class="panel-body">
				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Nombre del representante:</label>
				                <div class="col-sm-7 col-xs-12">
				                    <input type="text" class="form-control" name="representative_name" data-toggle="tooltip" data-placement="auto" title="Ingrese el nombre completo del representante legal" value="{{ old('representative_name') }}" maxlength="100"/>
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Número de DUI del representante:</label>
				                <div class="col-sm-7 col-xs-12">
				                    <input type="text" class="form-control" name="representative_dui" data-toggle="tooltip" data-placement="auto" title="Ingrese el número de DUI del representante legal" value="{{ old('representative_dui') }}" maxlength="20"/>
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Dirección del representante:</label>
				                <div class="col-sm-7 col-xs-12">
				                    <input type="text" class="form-control" name="representative_address" data-toggle="tooltip" data-placement="auto" title="Ingrese la dirección del representante legal" value="{{ old('representative_address') }}" maxlength="100"/>
				                </div>
				            </div>
				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label"></label>
				                <div class="col-sm-7 col-xs-12">
				                    <div class="alert alert-warning">
										<p class="color-black">Tamaño máximo de subida para cada archivo: {{ setting("portal::file_size")." KB" }}</p>
									</div>
				                </div>
				            </div>
				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Agregar DUI escaneado (PDF):</label>

				                <div class="col-sm-7 col-xs-12">
					                <input type="file" accept="application/pdf" name="representative_dui_pdf" data-toggle="tooltip" data-placement="auto" title="Adjunte el Documento Único de Identidad escaneado, del representante legal">				                
				                </div>
				            </div>

				            <div class="form-group">
				                <label class="col-sm-3 col-xs-12 control-label">Agregar poder del representante (PDF):</label>
				                <div class="col-sm-7 col-xs-12">
					                <input type="file" accept="application/pdf" name="representative_owner_pdf" data-toggle="tooltip" data-placement="auto" title="Adjunte la carta poder del representante legal">
				                </div>
				            </div>
				        </div>
			        </div>


		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Ocupación u oficio:</label>
		                <div class="col-sm-7 col-xs-12">
		                    <input type="text" class="form-control" name="occupation" data-toggle="tooltip" data-placement="auto" title="Ingrese la ocupación u oficio del solicitante" value="{{ old('occupation') }}" maxlength="100"/>
		                </div>
		            </div>
		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Teléfono celular:</label>
		                <div class="col-sm-3 col-xs-12">
		                    <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-phone"></i>
			                  </div>
			                  <input type="text" class="form-control phone-number" name="cell_phone" data-toggle="tooltip" data-placement="auto" title="Ingrese el teléfono celular del solicitante" value="{{ old('cell_phone') }}" maxlength="20"/>
			                </div>
		                </div>
		            </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Teléfono de casa:</label>
		                <div class="col-sm-3 col-xs-12">
		                    <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-phone"></i>
			                  </div>
			                  <input type="text" class="form-control phone-number" name="home_phone" data-toggle="tooltip" data-placement="auto" title="Opcionalmente puede ingresar el teléfono de casa del solicitante" value="{{ old('home_phone') }}" maxlength="20"/>
			                </div>
		                </div>
		            </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Teléfono de oficina:</label>
		                <div class="col-sm-3 col-xs-12">
		                    <div class="input-group">
			                  <div class="input-group-addon">
			                    <i class="fa fa-phone"></i>
			                  </div>
			                  <input type="text" class="form-control phone-number" name="office_phone" data-toggle="tooltip" data-placement="auto" title="Opcionalmente puede ingresar el teléfono de oficina del solicitante" value="{{ old('office_phone') }}" maxlength="20"/>
			                </div>
		                </div>
		            </div>

		             <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Domicilio del solicitante:</label>
		                <div class="col-sm-7 col-xs-12">
		                    <input type="text" class="form-control" name="person_address" data-toggle="tooltip" data-placement="auto" title="Ingrese la dirección del solicitante" value="{{ old('person_address') }}" maxlength="100" />
		                </div>
		            </div>

			        <div class="form-group">
	                  	<label class="col-sm-3 col-xs-12 control-label">Tipo de documento de identificación del solicitante:</label>
	                  	<div class="col-sm-7 col-xs-12">
			                <select name="document_type" class="form-control" id="document_type" data-toggle="tooltip" data-placement="auto" title="Seleccione el tipo de documento de identidad del solicitante">
			                   <option value="1" selected="selected">DUI</option>
			                   <option value="2">Carnet de residente</option>
			                   <option value="3">Carnet de Minoridad</option>
			                   <option value="4">Pasaporte</option>
			                </select>
		            	</div>
	                </div>

	                <div class="form-group">
				        <label class="col-sm-3 col-xs-12 control-label"></label>
				        <div class="col-sm-7 col-xs-12">
				            <div class="alert alert-warning">
								<p class="color-black">Tamaño máximo de subida para el archivo: {{ setting("portal::file_size")." KB" }}</p>
							</div>
				        </div>
				    </div>
	                <div class="form-group">
				        <label class="col-sm-3 col-xs-12 control-label">Adjuntar documento escaneado (PDF):</label>
				        <div class="col-sm-7 col-xs-12">
					        <input type="file" accept="application/pdf" name="document_pdf" data-toggle="tooltip" data-placement="auto" title="Adjunte el documento de identificación escaneado">
				        </div>
				    </div>

	                <div class="form-group" id="checkbox_is_foreign">
		            	<label class="col-sm-3 col-xs-12 control-label">¿Es extranjero?</label>
		            	<div class="col-sm-7 col-xs-12">
		                  	<div class="checkbox">
		                    	<label>
		                      		<input type="checkbox" value="1" id="is_foreign" name="is_foreign" data-toggle="tooltip" data-placement="right" title="Marque la casilla si el solicitante es extranjero">
		                    	</label>
		                  	</div>
		                </div>
	                </div>

		            <div class="form-group" id="doc_number">
		                <label class="col-sm-3 col-xs-12 control-label">Número del documento:</label>
		                <div class="col-sm-7 col-xs-12">
		                    <input type="text" class="form-control" name="document_number" data-toggle="tooltip" data-placement="auto" title="Ingrese el número del documento seleccionado" value="{{ old('document_number') }}" maxlength="20"/>
		                </div>
		            </div>

		            <div class="form-group" id="doc_student">
		                <label class="col-sm-3 col-xs-12 control-label">Carnet de estudiante, extendido por:</label>
		                <div class="col-sm-7 col-xs-12">
		                    <input type="text" class="form-control" name="student_card_extended_by" data-toggle="tooltip" data-placement="auto" title="Ingrese el nombre de la institución que extendió el carnet de estudiante" value="{{ old('student_card_extended_by') }}" maxlength="50"/>
		                </div>
		            </div>

		            <div class="form-group" id="country_foreign_passport">
		                <label class="col-sm-3 col-xs-12 control-label">Seleccionar país de emisión de pasaporte:</label>
		                <div class="col-sm-7 col-xs-12">
			                <select class="form-control" name="passport_extended_by" data-toggle="tooltip" data-placement="auto" title="Seleccione el país de emisión del pasaporte">
			                	@foreach ($countries as $country)
			                		@if ($country->code === 'SLV')
			                			<option value="{{ $country->code }}" selected="selected">{{ $country->name }}</option>
			                		@else
			                			<option value="{{ $country->code }}">{{ $country->name }}</option>
			                		@endif
			                	@endforeach
							</select>
		            	</div>
		            </div>


		            <div class="form-group">
		            	<div class="col-sm-12">
		            		<p>Nota:</p>
			            	<ol>
			            		<li>La forma de entrega estará sujeta a la capacidad técnica de la UAIP.</li>
			            		<li>En caso de Representante Legal, deberá adjuntar el documento que acredite la representación.</li>
			            	</ol>
		            	</div>
		            </div>
		        </div>

		        <!-- Second tab -->
		        <div class="tab-pane" id="second-tab">
		        	<br>
		            <h4>Información solicitada</h4>
		            <br>
		        	<div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Información solicitada</label>
		                <div class="col-sm-7 col-xs-12">
		                    <textarea class="form-control" name="info_requested" rows="6" data-toggle="tooltip" data-placement="auto" title="Describa la información que se solicita">{{ old('info_requested') }}</textarea>
		                </div>
		            </div>

		            <div class="form-group">
				        <label class="col-sm-3 col-xs-12 control-label"></label>
				        <div class="col-sm-7 col-xs-12">
				            <div class="alert alert-warning">
							<p class="color-black">Tamaño máximo de subida para cada archivo: {{ setting("portal::file_size")." KB" }}</p>
							</div>
				        </div>
				    </div>

		            <div class="form-group">
				        <label class="col-sm-3 col-xs-12 control-label">Documento anexo 1 (PDF):</label>
				        <div class="col-sm-7 col-xs-12">
					    <input type="file" accept="application/pdf" name="annexed_one" data-toggle="tooltip" data-placement="auto" title="(Opcional) Adjunte documentación que considere necesaria">				                
				        </div>
				    </div>

				    <div class="form-group">
				        <label class="col-sm-3 col-xs-12 control-label">Documento anexo 2 (PDF):</label>
				        <div class="col-sm-7 col-xs-12">
					    <input type="file" accept="application/pdf" name="annexed_two" data-toggle="tooltip" data-placement="auto" title="(Opcional) Adjunte documentación que considere necesaria">          
				        </div>
				    </div>

				    <div class="form-group">
				        <label class="col-sm-3 col-xs-12 control-label">Documento anexo 3 (PDF):</label>
				        <div class="col-sm-7 col-xs-12">
					    <input type="file" accept="application/pdf" name="annexed_three" data-toggle="tooltip" data-placement="auto" title="(Opcional) Adjunte documentación que considere necesaria">          
				        </div>
				    </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Lugar para recibir notificaciones:</label>
		                <div class="col-sm-7 col-xs-12">
		                    <input type="text" class="form-control" name="notifications_target" data-toggle="tooltip" data-placement="auto" title="Ingrese el lugar donde desea recibir notificaciones de la solicitud, puede ser una dirección, un número de teléfono o una dirección de correo electrónico alternativa." value="{{ old('notifications_target') }}"  maxlength="100"/>
		                </div>
		            </div>
		            <div class="form-group">
		            	<label class="col-sm-3 col-xs-12 control-label" data-toggle="tooltip" data-placement="auto" title="Marque los medios en los cuales desea recibir la información solicitada. (Máximo 2)">Indique la forma en que desea que se le dé acceso a la información:</label>
		            	<div class="col-sm-7 col-xs-12">
		            		<div class="col-sm-6 col-xs-12">
		                    <div class="form-group">
			                  	<div class="checkbox">
				                    <label data-toggle="tooltip" data-placement="auto" title="LAIP: Art. 63.- El solicitante tendrá derecho a efectuar la consulta directa de información pública dentro de los horarios de atención general del ente obligado correspondiente. Se permitirá la consulta directa de los datos o registros originales en caso que no se hallen almacenados en algún medio magnético, digital, microfichas y que su estado lo permita. Bajo ninguna circunstancia se prestará o permitirá la salida de registros o datos originales de los archivos en que se hallen almacenados. Los entes obligados deberán asesorar al solicitante sobre el servicio de consulta directa de información pública.">
				                	    <input class="check_no" name="access_type_one" type="checkbox" value="Consulta directa">Consulta directa
				                    </label>
				                </div>
			                </div>
		                </div>
		                <div class="col-sm-6 col-xs-12">
		                    <div class="form-group">
		                    	<div class="checkbox">
				                    <label>
				                	    <input class="check_no" name="access_type_two" type="checkbox" value="Vía electrónica">Vía electrónica
				                    </label>
				                </div>
			                </div>
		                </div>
		                <div class="col-sm-6 col-xs-12">
		                    <div class="form-group">
		                    	<div class="checkbox">
				                    <label>
				                	    <input class="check_no" name="access_type_three" type="checkbox" value="Copia simple">Copia simple
				                    </label>
				                </div>
			                </div>
		                </div>
		                <div class="col-sm-6 col-xs-12">
		                    <div class="form-group">
		                    	<div class="checkbox">
				                    <label>
				                	    <input class="check_no" name="access_type_four" type="checkbox" value="Copia certificada">Copia certificada
				                    </label>
				                </div>
			                </div>
		                </div>
		                <div class="col-sm-6 col-xs-12">
		                    <div class="form-group">
		                    	<div class="checkbox">
				                    <label>
				                	    <input class="check_no" name="access_type_five" type="checkbox" value="Fax">Fax
				                    </label>
				                </div>
			                </div>
		                </div>
		                <div class="col-sm-6 col-xs-12">
		                    <div class="form-group">
		                    	<div class="checkbox">
				                    <label>
				                	    <input class="check_no" name="access_type_six" type="checkbox" value="Disco compacto (CD)">Disco compacto (CD)
				                    </label>
				                </div>
			                </div>
		                </div>
		                <div class="col-sm-6 col-xs-12">
		                    <div class="form-group">
		                    	<div class="checkbox">
				                    <label>
				                	    <input class="check_no" name="access_type_seven" type="checkbox" value="Por cartelera">Por cartelera
				                    </label>
				                </div>
			                </div>
		                </div>
		            	</div>		                
		            </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Correo electrónico para recibir notificaciones:</label>
		                <div class="col-sm-7 col-xs-12">
		                    <div class="input-group">
				                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
				                <input type="email" name="email" class="form-control" data-toggle="tooltip" data-placement="auto" title="Ingrese el correo electrónico en el cual desea recibir las notificaciones" maxlength="50" value="{{ old('email') }}"  required="required" />
				            </div>
		                </div>
		            </div>
		            <div class="form-group" id="checkbox_sne_notifications">
		            	<label class="col-sm-3 col-xs-12 control-label" data-toggle="tooltip" data-placement="auto" title="Marque la casilla si desea recibir notificaciones por SNE">Activar notificaciones por SNE:</label>
		            	<div class="col-sm-7 col-xs-12">
		                  	<div class="checkbox">
		                    	<label>
		                      		<input tabindex="0" id="sne_notifications" type="checkbox" name="sne_notifications" value="1" data-toggle="popover" data-placement="top" data-trigger="focus" title="SNE:" data-content="Es una aplicación informática que permite el envío telemático de comunicaciones escritas a los solicitantes que están registrados en dicha aplicación, siguiendo las reglas y protocolos oficiales de la notificación electrónica <a href='http://sne.csj.gob.sv/media/pdfs/brochure.pdf' target='_blank'>Leer más</a>">
		                    	</label>
		                  	</div>
		                </div>
	                </div>

	                <div class="form-group" id="sne_ceu_box">
		                <label class="col-sm-3 col-xs-12 control-label">Ingrese su CEU:</label>
		                <div class="col-sm-7 col-xs-12">
				            <input type="text" name="sne_ceu" class="form-control" placeholder="" data-toggle="tooltip" data-placement="auto" title="Ingrese su CEU" value="{{ old('sne_ceu') }}" maxlength="100" />
		                </div>
		            </div>

		            <div class="form-group">
		                <label class="col-sm-3 col-xs-12 control-label">Puede elegir entre adjuntar su firma o puede dibujarla en el lienzo inferior. Si adjunta su firma y también la dibuja, el dibujo tendrá prioridad:</label>
		                <div class="col-md-7 col-xs-12">
		                	<input type="file" accept="image/x-png,image/jpeg" name="signature_upload" id="signature_upload" data-toggle="tooltip" data-placement="auto" title="Puede adjuntar una imagen pequeña con su firma, procure que la imagen solamente contenga su firma.">
		                </div>
	                </div>

	                <div class="form-group" id="signature">
		                <label class="col-md-6 col-xs-12 control-label">Si lo prefiere, puede dibujar su firma:</label>
		                <div class="col-md-6 col-xs-12">
		                	<div class="wrapper">
								<canvas id="signature-pad" class="signature-pad" width="260" height="150"></canvas>
							</div>
		                </div>
						<input type="hidden" name="signature-img" id="signature-img" value="null">
	                </div>

	                <div class="row">
	                	<div class="col-sm-3 col-xs-12"></div>
	                	<div class="col-sm-7 col-xs-12">
							<a type="button" class="btn btn-default" id="clear"><i class="fa fa-refresh"></i> Limpiar firma</a>
							<a type="button" class="btn btn-info" id="save-signature"><i class="fa fa-save"></i> Guardar firma</a>
	                	</div>
	                </div>
	                <div class="row" id="signature-msg-empty">
	                	<div class="col-sm-3 col-xs-12"></div>
	                	<div class="col-sm-7 col-xs-12">
							<p class="label label-warning">Debe adjuntar una imagen con su firma, o puede dibujar su firma y guardarla</p>	                		
	                	</div>
	                </div>
	                <div class="row" id="signature-msg">
	                	<div class="col-sm-3 col-xs-12"></div>
	                	<div class="col-sm-7 col-xs-12">
							<p class="label label-info">Firma guardada</p>	                		
	                	</div>
	                </div>
	                <br>
	                <br>
	                <div class="form-group">
				        <div class="col-sm-10 col-xs-12">
				        <div class="pull-right">{!! NoCaptcha::display(['data-size' => 'compact']) !!}</div>
				        </div>
				    </div>

		            <div class="form-group">
		            	<div class="col-sm-12">
		            		<p>Nota:</p>
			            	<ol>
			            		<li>La forma de entrega estará sujeta a la capacidad técnica de la UAIP.</li>
			            		<li>En caso de Representante Legal, deberá adjuntar el documento que acredite la representación.</li>
			            	</ol>
		            	</div>
		            </div>
		        </div>

		        <!-- Previous/Next buttons -->
		        <ul class="pager wizard">
		            <li class="previous"><a href="javascript: void(0);">Anterior</a></li>
		            <li class="next"><a id="finish" href="javascript: void(0);"><i></i> Siguiente</a></li>
		        </ul>
		    </div>
		    <div class="overlay hidden" id="div-overlay"><i class="fa fa-refresh fa-spin"></i></div>
		</div>
		{!! Form::close() !!}

    </div>
@stop
{!! NoCaptcha::renderJs() !!}


@push('js-stack')

<script type="text/javascript">
	var signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
		backgroundColor: 'rgba(255, 255, 255, 0)',
		penColor: 'rgb(0, 0, 0)'
	});
	var cancelButton = document.getElementById('clear');

	cancelButton.addEventListener('click', function (event) {
		signaturePad.clear();
		$("#signature-msg").fadeOut("slow");
		$("#signature-msg-empty").fadeOut("slow");
	});

	$("#save-signature").on("click", function(){
		var canvas = document.getElementById('signature-pad');
		var dataURL = canvas.toDataURL();
		$("#signature-img").val(dataURL);
		$("#signature-msg").fadeIn("slow");
		$("#signature-msg-empty").fadeOut("slow");
	});

	$('[data-toggle="popover"]').popover({html:true});

	$(document).ready(function() {
    $('#requestForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            // This option will not ignore invisible fields which belong to inactive panels
            excluded: ':disabled',
            fields: {
                lastname: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Apellido (s)" es requerido'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Nombre (s)" es requerido'
                        }
                    }
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: 'Debe seleccionar su género'
                        }
                    }
                },
                occupation: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Ocupación" es requerido'
                        }
                    }
                },
                cell_phone: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Teléfono celular" es requerido'
                        }
                    }
                },
                person_address: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Domicilio" es requerido'
                        }
                    }
                },
                info_requested: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Correo electrónico" es requerido'
                        }
                    }
                },
                notifications_target: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                document_number: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                document_pdf: {
                    validators: {
                        notEmpty: {
                            message: 'Debe adjuntar el documento de identidad'
                        }
                    }
                },
            }
        })
        .bootstrapWizard({
            tabClass: 'nav nav-pills',
            onTabClick: function(tab, navigation, index) {
                return validateTab(index);
            },
            onNext: function(tab, navigation, index) {
                var numTabs    = $('#requestForm').find('.tab-pane').length,
                    isValidTab = validateTab(index - 1);
                if (!isValidTab) {
                    return false;
                }

                if (index === numTabs) {
                    // We are at the last tab
                    // The hidden input for signature shoudn't be empty
                    if ($('#signature_upload').get(0).files.length === 0 && $("#signature-img").val() === "null") {
                        $("#signature-msg-empty").show("slow");
                        return false;
                    } else {
                    	

                    	$('i', "#finish").addClass("fa-refresh fa-spin");
						$(".next").attr("disabled", "true");

						$("#div-overlay").removeClass("hidden");

						$('#requestForm').formValidation('defaultSubmit');
						                     
                    }
                    //$('#requestForm').formValidation('defaultSubmit');
                }

                return true;
            },
            onPrevious: function(tab, navigation, index) {            
                return validateTab(index + 1);
            },
            onTabShow: function(tab, navigation, index) {
                // Scroll to top
                $('html, body').animate({ scrollTop: $(".form-horizontal").offset().top }, 1000);

                // Update the label of Next button when we are at the last tab
                var numTabs = $('#requestForm').find('.tab-pane').length;
                $('#requestForm')
                    .find('.next')
                        .removeClass('disabled')    // Enable the Next button
                        .find('a')
                        .html(index === numTabs - 1 ? 'Finalizar' : 'Siguiente');
            }
        });

    function validateTab(index) {
        var fv   = $('#requestForm').data('formValidation'),
            // The current tab
            $tab = $('#requestForm').find('.tab-pane').eq(index);

        // Validate the container
        fv.validateContainer($tab);

        var isValidStep = fv.isValidContainer($tab);
        if (isValidStep === false || isValidStep === null) {
            // Do not jump to the target tab
            return false;
        }

        return true;
    }
});

$(function() {
    //Datepicker
    $('.datepicker').datepicker({ autoclose: true, language: 'es' });
    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
    $('.datepicker').datepicker('update', new Date());
    $('.datepicker').datepicker('enableOnReadonly', true);

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false,
      minuteStep: 1,
    });

    $('#is_foreign').click(function(){
        if($(this).is(':checked')){
            $("#country_foreign_passport").fadeIn( "slow");
        } else {
            $("#country_foreign_passport").fadeOut("slow");
        }
    });

    $('#sne_notifications').click(function(){
        if($(this).is(':checked')){
            $("#sne_ceu_box").fadeIn( "slow");
        } else {
            $("#sne_ceu_box").fadeOut("slow");
        }
    });
    
    $('[data-toggle="tooltip"]').tooltip();

    $("#people_type").change(function(){
        var current_selection = $(this).val();
        switch (current_selection) {
            case "1":
                $("#have_representant").fadeOut("slow");
                break;
            case "2":
                $("#have_representant").fadeIn("slow");
                break;
            case "3":
                $("#have_representant").fadeIn("slow");
                break;
        }
    });

    $("#document_type").change(function(){
        $("#document_type option:selected").each(function(){
            var current_selection = $(this).val();
            switch (current_selection) {
                case "1":
                    $("#doc_number").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                case "2":
                    $("#doc_number").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                case "3":
                    $("#doc_number").fadeIn("slow");
                    $("#doc_student").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                case "4":
                    $("#checkbox_is_foreign").fadeIn("slow");
                    $("#doc_number").fadeIn("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                default:
                    $("#doc_number").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;
            }
        });
    });

    $(".phone-number").keypress(function(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    });

    var counter_check = 0;
    $(document).on('click','.check_no',function() {
        if ( counter_check < 2){
            counter_check++;
            $(this).removeClass("check_no").addClass("check_yes");
            if (counter_check == 2){
                $(".check_no").prop('disabled', true);
            }
        }
    });

    $(document).on('click','.check_yes',function() {
        $(this).removeClass("check_yes").addClass("check_no");
        counter_check--;
        $(this).removeClass("check_yes").addClass("check_no");
        $(".check_no").prop('disabled', false);
    });

});
</script>
	
@endpush
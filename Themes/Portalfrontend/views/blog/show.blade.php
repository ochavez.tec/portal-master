@extends('layouts.master')

@section('title')
    {{ $post->title }} | @parent
@stop

@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="linkBack">
                    <a type="button" class="btn btn-primary" href="{{ URL::route($currentLocale . '.blog') }}"><i class="glyphicon glyphicon-chevron-left"></i> Volver al listado</a>
                </div>
                <br>
                <div class="box box-info">
                    <div class="box-header with-border">
                      <h3 class="box-title">{{ $post->title }}</h3>
                    </div>
                    <div class="box-body">
                        {!! $post->content !!}
                    </div>
                </div> 
            </div>
        </div>
        <div class="row">
            <?php if ($previous = $post->present()->previous): ?>
                    <div class="col-md-6">
                        <div class="pull-left">
                            <a type="button" class="btn btn-primary" href="{{ route(locale() . '.blog.slug', [$previous->slug]) }}"><i class="fa fa-arrow-left"></i> Anterior</a>    
                        </div>
                    </div>
            <?php endif; ?>
            <?php if ($next = $post->present()->next): ?>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <a type="button" class="btn btn-primary" href="{{ route(locale() . '.blog.slug', [$next->slug]) }}">Siguiente <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
            <?php endif; ?>
        </div>
</div>
@stop

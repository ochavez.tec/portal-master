@extends('layouts.master')

@section('title')
    Blog | @parent
@stop

@push('css-stack')
<style>
  .preview-content > p {
    width: 250px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
</style>
@endpush

@section('content')
    <div class="container">
          <div class="row">
          <div class="col-md-12">
          <div class="box box-info box-solid">
          <div class="box-header with-border">
          <h3 class="box-title">Noticias:</h3>
          </div>
          <div class="box-body">
            <?php if (isset($posts)): ?>
                <?php foreach ($posts as $post): ?>
                    <?php $post_array = $post->files()->first(); ?>

                    <div class="col-md-6">
                      <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Publicación:</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-4 col-xs-12 align-center border-box">
                              <a href="{{ URL::route($currentLocale . '.blog.slug', [$post->slug]) }}">
                                @if($post_array['path'] != null)
                                  <img src="{{ Imagy::getThumbnail($post_array['path'], 'blogThumb') }}" alt="Post preview image" width="115px" height="115px" />
                                @endif
                              </a>
                              <br/>
                              <br/>
                            </div>
                            
                            <div class="col-md-8 col-xs-12">
                              <div class="callout callout-info post-title-container">
                                <a style="text-decoration: none;" href="{{ URL::route($currentLocale . '.blog.slug', [$post->slug]) }}">
                                  <?php $out = strlen($post->title) > 50 ? substr($post->title,0,50)."..." : $post->title; ?>
                                  <h4>{{ $out }}</h4>
                                </a>
                              </div>
                              <blockquote class="pull-right">
                                <p><a href="{{ URL::route($currentLocale . '.blog.slug', [$post->slug]) }}">Leer artículo</a></p>
                                <span class="date"><small>Creado: {{ $post->created_at->format('d-m-Y') }}</small></span>
                              </blockquote>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        </div>
        </div>
        </div>
    </div>
@stop

@push('js-stack')
<script type="text/javascript">
    $( document ).ready(function() {

    });
</script>
@endpush

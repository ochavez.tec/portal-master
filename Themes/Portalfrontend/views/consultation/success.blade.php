@extends('layouts.master')

@section('title')
    Consulta enviada | @parent
@stop
@section('meta')
    <meta name="title" content="" />
    <meta name="description" content="" />
@stop

@section('content')
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Consulta enviada exitosamente</h3>
              </div>
              <div class="box-body" style="display:block;padding:0;">               
                  <div class="col-md-12">
                    <h2 class="headline color-skyblue align-center"><i class="fa fa-thumbs-o-up fa-5x"></i></h2>
                    <br/>
                    <h4 class="align-center">Su consulta ha sido enviada exitosamente, por favor revise su bandeja de entrada.</h4>
                    <br/>
                  </div>
              </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-success box-solid">
              <div class="box-header with-border">
                  <h3 class="box-title">Instrucciones:</h3>
              </div>
              <div class="box-body" style="display:block;padding:0;">               
                <div class="col-md-12">
                   <ul>
                     <li>Revise su bandeja de entrada, en ella encontrará un correo enviado por el portal de transparencia. No olvide revisar en la bandeja de spam.</li>
                     <li>Lea detenidamente las indicaciones que en él se encuentren.</li>
                     <li>Haga clic en el enlace provisto por medio del correo para continuar con el proceso de su consulta.</li>
                   </ul>
                </div>
              </div>
          </div>

          <div class="box box-primary box-solid">
              <div class="box-body" style="display:block;padding:0;">               
                <div class="col-md-12 align-center">
                  
                  <br/>
                  <a type="button" class="btn btn-lg btn-primary" href="/">Volver a la página principal</a>
                  <br/>
                  <br/>
                </div>
              </div>
          </div>

        </div>
      </div>
    </div>
@stop
@extends('layouts.master')

@section('title')
    {{ $page->title }} | @parent
@stop
@section('meta')
    <meta name="title" content="{{ $page->meta_title}}" />
    <meta name="description" content="{{ $page->meta_description }}" />
@stop

@section('content')
    <div class="container">
        {!! Slider::render('principal-slider-system') !!}
        {!! Block::get('gestion-menu') !!}
        {!! $page->body !!}
    </div>
@stop

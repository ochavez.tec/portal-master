<!DOCTYPE html>
<html>
<head lang="{{ LaravelLocalization::setLocale() }}">
    <meta charset="UTF-8">
    @section('meta')
        <meta name="description" content="@setting('core::site-description')" />
    @show
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        @section('title')@setting('core::site-name')@show
    </title>
    <link rel="shortcut icon" href="{{ Theme::url('favicon.ico') }}">

    {!! Theme::style('css/bootstrap/bootstrap.min.css') !!}
    {!! Theme::style('css/bootstrap/bootstrap-theme.min.css') !!}
    {!! Theme::style('plugins/font-awesome/css/font-awesome.min.css') !!}
    {!! Theme::style('plugins/adminlte/css/AdminLTE.min.css') !!}
    {!! Theme::style('css/general.css') !!}

    <?php if (Setting::has('portal::style_css')): ?>
        <style type="text/css">
            {!! Setting::get('portal::style_css') !!}
        </style>
    <?php endif; ?>
</head>
<body>

@include('partials.forumnavigation')

<div class="main-container">
    <div id="page-body">
        <noscript>
            <div class="container">
            <div class="alert alert-danger">
                <p class="color-black">
                <strong>Alerta: </strong>Para obtener una funcionalidad completa de este sitio es necesario activar JavaScript.
                Aquí podrá encontrar <a href="http://www.enable-javascript.com/" class="color-black" target="_blank" style="color: #000000;"> instrucciones sobre cómo habilitar JavaScript en su navegador web.</a>
                </p>
            </div>
            </div>
        </noscript>
        @yield('content')
    </div>
</div>

@include('partials.footer')

{!! Theme::script('js/jquery/jquery-3.3.1.min.js') !!}
{!! Theme::script('js/bootstrap/bootstrap.min.js') !!}
{!! Theme::script('plugins/adminlte/js/app.min.js') !!}
{!! Theme::script('js/pcforum.js') !!}
@yield('scripts')

<?php if (Setting::has('portal::script_js')): ?>
    <script type="text/javascript">
        {!! Setting::get('portal::script_js') !!}
    </script>
<?php endif; ?>

<?php if (Setting::has('core::analytics-script')): ?>
    {!! Setting::get('core::analytics-script') !!}
<?php endif; ?>
</body>
</html>

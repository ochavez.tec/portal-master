<div class="navbar-wrapper">
  <div class="container">
    <nav class="navbar navbar-primary-custom" id="navbar-primary">
      <div class="container" id="nav-padd-sides">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-first" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
            <img alt="Brand" src="{{ asset('/assets/media/logo-csj.png') }}">
            <span id="brand-title">@setting('core::site-name')</span>
          </a>
        </div>
        <div id="navbar-first" class="navbar-collapse collapse pull-to-right">
          {!! Menu::render('primary', 'navbar') !!}
        </div>
      </div>
    </nav>
    <nav class="navbar navbar-secondary-custom" id="navbar-secondary">
      <div class="">
        <div id="navbar-second" class="navbar-collapse collapse">
          <div class="col-md-7 no-padd-sides" id="secondary">
            {!! Menu::get('secondary') !!}       
          </div>
          <div class="col-md-5 no-padd-sides">
            {!! Form::open(['route' => 'info.search', 'method' => 'POST', 'class' => 'navbar-form navbar-right general-search-form']) !!}
              <div class="input-group general-search-form-group">
                <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
                <input type="text" name="query" class="general-search-form-input form-control" placeholder="¿Qué información está buscando?">
              </div>
              <button type="submit" class="btn btn-info sr-only"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
            {!! Form::close() !!} 
          </div>
        </div>
      </div>
    </nav>
    
    <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      {!! Menu::render('secondary', 'navmenu') !!}
      <div id="search-sidebar">
        {!! Form::open(['route' => 'info.search', 'method' => 'POST', 'class' => 'navbar-form navbar-right general-search-form']) !!}
          <div class="input-group general-search-form-group">
            <span class="input-group-addon" id="basic-addon1"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
            <input type="text" name="query" class="general-search-form-input form-control" placeholder="¿Qué información está buscando?">
          </div>
          <button type="submit" class="btn btn-info sr-only"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> Buscar</button>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
<div class="container" id="navbar-secondary-mobile">
  <div class="row">
    <div class="col-md-12">
      <buttom class="btn btn-primary" onclick="openNav()">&#9776; Buscar</buttom>  
    </div>
    <br>
  </div>
</div>
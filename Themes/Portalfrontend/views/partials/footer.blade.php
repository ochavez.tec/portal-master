<footer class="footer">
      <div class="container">
      	<div class="row">
      		<div class="col-md-12">
      			<div class="col-md-4 footer-block" >
      				{!! Block::get('footer-block-1') !!}
      			</div>
      			<div class="col-md-4 footer-block">
      				{!! Block::get('footer-block-2') !!}                              
      			</div>
      			<div class="col-md-4 footer-block">
      				{!! Block::get('footer-block-3') !!}
      			</div>
      		</div>
      	</div>
            <div class="row">
                  <div class="col-md-12 align-center">@widget('SiteCounterWidget')</div>
            </div>
      </div>
</footer>
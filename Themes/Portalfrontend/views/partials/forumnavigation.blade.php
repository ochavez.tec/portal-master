<div class="navbar-wrapper">
  <div class="container">
    <nav class="navbar navbar-primary-custom" id="navbar-primary">
      <div class="container" id="nav-padd-sides">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-first" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
            <img alt="Brand" src="{{ asset('/assets/media/logo-csj.png') }}">
            <span id="brand-title">Unidad de Acceso a la Información Pública del Órgano Judicial</span>
          </a>
        </div>
      </div>
    </nav>
  </div>
</div>
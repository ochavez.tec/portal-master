<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {	
	/**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__country';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_country';

    protected $fillable = [
    	'code',
    	'name',
    ];
}

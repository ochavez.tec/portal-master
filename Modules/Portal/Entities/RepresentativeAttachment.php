<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class RepresentativeAttachment extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__representative_attachment';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_representative_attachment';

    protected $fillable = [
    	'representative_id',
        'filename',
        'path',
        'extension',
        'mimetype',
        'filesize',
        'document_type',
    ];
}

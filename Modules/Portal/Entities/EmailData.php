<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class EmailData extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__email_data';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_email_data';

    /**
     * Filleable columns
     *
     */
    protected $fillable = [
        'title',
        'subtitle',
        'text_block_1',
        'text_block_2',
        'text_block_3',
        'button_text',
    	'footer',
    ];
}

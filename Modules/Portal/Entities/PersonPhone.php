<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class PersonPhone extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__person_phone';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_person_phone';

    protected $fillable = [
    	'person_id',
        'cell_phone',
        'home_phone',
        'office_phone',
    ];

    /**
     * Person relation (belongsTo)
     */
    public function person(){
        return $this->belongsTo('Modules\Portal\Entities\Person', 'person_id', 'id_person');
    }
}

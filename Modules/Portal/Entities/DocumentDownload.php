<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class DocumentDownload extends Model {	
	/**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__document_download';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_document_download';

    protected $fillable = [
    	'document_id',
    	'name',
        'date',
        'downloads',
    ];
}

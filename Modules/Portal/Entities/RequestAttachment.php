<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class RequestAttachment extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__request_attachment';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_request_attachment';

    protected $fillable = [
    	'request_id',
        'filename',
        'path',
        'extension',
        'mimetype',
        'filesize',
    ];

    /**
     * Request relation (belongsTo)
     */
    public function request(){
        return $this->belongsTo('Modules\Portal\Entities\InformationRequest', 'request_id', 'id_request');
    }
}

<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class Representative extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__representative';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_representative';

    protected $fillable = [
    	'person_id',
        'name',
        'dui',
        'address',
    ];

    /**
     * Person relation (belongsTo)
     */
    public function person(){
        return $this->belongsTo('Modules\Portal\Entities\Person', 'person_id', 'id_person');
    }
}

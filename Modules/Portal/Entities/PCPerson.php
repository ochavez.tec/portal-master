<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class PCPerson extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__pc_person';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_pc_person';

    /**
     * Filleable columns
     *
     */
    protected $fillable = [
    	'name',
    	'email',
    	'phone',
    ];

    /**
     * PCEmailVerification relation (hasMany)
     */
    public function pcEmailVerification(){
        return $this->hasMany('Modules\Portal\Entities\PCEmailVerification', 'pc_person_id', 'id_pc_person');
    }

    /**
     * PCEmailVerification relation (hasMany)
     */
    public function pcRequest(){
        return $this->hasMany('Modules\Portal\Entities\PCRequest', 'pc_person_id', 'id_pc_person');
    }

    /**
     * Returns pcperson consultations counter
     */
    public function getTotalPCRequest(){
        return $this->hasMany('Modules\Portal\Entities\PCRequest', 'pc_person_id', 'id_pc_person')->where('pc_person_id', $this->id_pc_person)->count();    
    }

}

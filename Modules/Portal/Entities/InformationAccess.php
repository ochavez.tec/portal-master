<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class InformationAccess extends Model {
	/**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__information_access';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_information_access';

    protected $fillable = [
    	'request_id',
        'access_one',
    	'access_two',
    ];

    /**
     * Request relation (belongsTo)
     */
    public function request(){
        return $this->belongsTo('Modules\Portal\Entities\InformationRequest', 'request_id', 'id_request');
    }
}

<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class PCEmailVerification extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__pc_email_verification';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_pc_email_verification';

    /**
     * Filleable columns
     *
     * person_id = int, 
     * status = boolean,
     * token = string
     */
    protected $fillable = [
    	'pc_person_id',
    	'status',
    	'token',
    ];

    /**
     * PCPerson relation (belongsTo)
     */
    public function pcPerson(){
        return $this->belongsTo('Modules\Portal\Entities\PCPerson', 'pc_person_id', 'id_pc_person');
    }

}

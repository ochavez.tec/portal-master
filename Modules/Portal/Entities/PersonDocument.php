<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class PersonDocument extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__person_document';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_person_document';

    protected $fillable = [
    	'person_id',
    	'document_type',
    	'is_foreign',
    	'document_number',
    	'extended_by',
    ];

    /**
     * Get the person that owns the document
     */
    public function person(){
        return $this->belongsTo('Modules\Portal\Entities\Person', 'person_id', 'id_person');
    }
}

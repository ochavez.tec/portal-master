<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class DownloadLog extends Model {	
	/**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__download_log';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_download_log';

    protected $fillable = [
        'document_id',
    	'attach_id',
    ];
}

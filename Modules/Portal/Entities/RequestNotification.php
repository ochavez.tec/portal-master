<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class RequestNotification extends Model {
	/**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__request_notification';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_request_notification';

    protected $fillable = [
    	'request_id',
        'notification_site',
        'sne_notification',
        'sne_ceu',
    ];

    /**
     * Request relation (belongsTo)
     */
    public function request(){
        return $this->belongsTo('Modules\Portal\Entities\InformationRequest', 'request_id', 'id_request');
    }
    
}

<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class InformationRequest extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__request';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_request';

    /**
     * Filleable columns
     *
     * correlative = int
     * year = int
     * person_id = int, 
     * status = enum, 
     * request_token = string, 
     * description = text
     */
    protected $fillable = [
        'correlative',
        'year',
    	'person_id',
        'status',
        'request_token',
        'description',
        'right_type'
    ];

    /**
     * Person relation (belongsTo)
     */
    public function person(){
        return $this->belongsTo('Modules\Portal\Entities\Person', 'person_id', 'id_person');
    }

    /**
     * InformationAccess relation (hasMany)
     */
    public function informationAccess(){
        return $this->hasMany('Modules\Portal\Entities\InformationAccess', 'request_id', 'id_request');
    }

    /**
     * RequestNotification relation (hasMany)
     */
    public function requestNotification(){
        return $this->hasMany('Modules\Portal\Entities\RequestNotification', 'request_id', 'id_request');
    }

    /**
     * RequestForum relation (hasMany)
     */
    public function requestForum(){
        return $this->hasMany('Modules\Portal\Entities\RequestForum', 'request_id', 'id_request');
    }

    /*public function delete() {
        
        foreach ($this->informationAccess as $info) {
            $info->delete();
        }

        foreach ($this->requestNotification as $noti) {
            $noti->delete();
        }

        foreach ($this->requestForum as $forum) {
            $forum->delete();
        }
    
        return parent::delete();
    }*/
}

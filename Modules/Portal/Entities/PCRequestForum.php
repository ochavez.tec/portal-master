<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class PCRequestForum extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__pc_request_forum';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_pc_request_forum';

    protected $fillable = [
    	'pc_request_id',
        'message',
        'is_logged',
    ];

    /**
     * PCRequest relation (belongsTo)
     */
    public function pcRequest(){
        return $this->belongsTo('Modules\Portal\Entities\PCRequest', 'pc_request_id', 'id_pc_request');
    }
}

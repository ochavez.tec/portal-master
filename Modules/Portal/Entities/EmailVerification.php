<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class EmailVerification extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__email_verification';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_email_verification';

    /**
     * Filleable columns
     *
     * person_id = int, 
     * status = boolean,
     * token = string
     */
    protected $fillable = [
    	'person_id',
    	'status',
    	'token',
    ];

    /**
     * Person relation (belongsTo)
     */
    public function person(){
        return $this->belongsTo('Modules\Portal\Entities\Person', 'person_id', 'id_person');
    }
}

<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class Person extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__person';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_person';

    /**
     * Filleable columns
     *
     * name = string, 
     * lastname = string, 
     * gender = enum, 
     * person_type = enum, 
     * occupation = string, 
     * address = string, 
     * email = string
     */
    protected $fillable = [
    	'name',
    	'lastname',
    	'gender',
    	'person_type',
    	'occupation',
    	'address',
    	'email',
    ];

    /**
     * PersonPhone relation (hasOne)
     */
    public function personPhone(){
        return $this->hasOne('Modules\Portal\Entities\PersonPhone', 'person_id', 'id_person');
    }

    /**
     * PersonDocument relation (hasOne)
     */
    public function personDocument(){
        return $this->hasOne('Modules\Portal\Entities\PersonDocument', 'person_id', 'id_person');
    }

    /**
     * EmailVerification relation (hasMany)
     */
    public function emailVerification(){
        return $this->hasMany('Modules\Portal\Entities\EmailVerification', 'person_id', 'id_person');
    }

    /**
     * Representative relation (hasMany)
     */
    public function representative(){
        return $this->hasMany('Modules\Portal\Entities\Representative', 'person_id', 'id_person');
    }

    /**
     * Request relation (hasMany)
     */
    public function requests(){
        return $this->hasMany('Modules\Portal\Entities\InformationRequest', 'person_id', 'id_person');
    }

    /**
     * Returns person requests counter
     */
    public function getTotalRequest(){
        return $this->hasMany('Modules\Portal\Entities\InformationRequest', 'person_id', 'id_person')->where('person_id', $this->id_person)->count();    
    }

}

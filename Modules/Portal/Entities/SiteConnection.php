<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class SiteConnection extends Model {	
	/**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__site_connection';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_site_connection';

    protected $fillable = [
    	'counter',
    ];
}

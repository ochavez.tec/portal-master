<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class PCRequest extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__pc_request';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_pc_request';

    /**
     * Filleable columns
     *
     * correlative = int
     * year = int
     * person_id = int, 
     * status = enum, 
     * request_token = string, 
     * description = text
     */
    protected $fillable = [
        'correlative',
        'year',
    	'pc_person_id',
        'status',
        'request_token',
        'subject',
        'description',
    ];

    /**
     * PCPerson relation (belongsTo)
     */
    public function pcPerson(){
        return $this->belongsTo('Modules\Portal\Entities\PCPerson', 'pc_person_id', 'id_pc_person');
    }

    /**
     * PCRequestForum relation (hasMany)
     */
    public function pcRequestForum(){
        return $this->hasMany('Modules\Portal\Entities\PCRequestForum', 'pc_request_id', 'id_pc_request');
    }

}

<?php

namespace Modules\Portal\Entities;

use Illuminate\Database\Eloquent\Model;

class RequestForum extends Model {
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'portal__request_forum';

	/**
     * The custom table id
     *
     * @var int
     */
	protected $primaryKey = 'id_request_forum';

    protected $fillable = [
    	'request_id',
        'message',
        'is_logged',
    ];

    /**
     * Request relation (belongsTo)
     */
    public function request(){
        return $this->belongsTo('Modules\Portal\Entities\InformationRequest', 'request_id', 'id_request');
    }
    
}

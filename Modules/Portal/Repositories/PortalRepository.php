<?php namespace Modules\Portal\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface PortalRepository extends BaseRepository
{
    /**
     * Get all online
     * @return object
     */
    public function allOnline();
}
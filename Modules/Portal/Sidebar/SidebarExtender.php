<?php namespace Modules\Portal\Sidebar;

use Maatwebsite\Sidebar\Badge;
use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item("Portal", function (Item $item) {
                $item->icon('fa fa-home');
                $item->weight(1);
                
                $item->item("Ajustes generales", function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-cog');
                    $item->route('admin.portal.portal.index');
                    $item->authorize(
                        $this->auth->hasAccess('portal.general.index')
                    );
                });

                $item->item("Solicitudes", function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-user');
                    $item->route('admin.portal.portal.request');
                    $item->authorize(
                        $this->auth->hasAccess('portal.request.index')
                    );
                });

                $item->item("Consultas", function (Item $item) {
                    $item->weight(0);
                    $item->icon('fa fa-flag');
                    $item->route('admin.portal.portal.consultation');
                    $item->authorize(
                        $this->auth->hasAccess('portal.consultation.index')
                    );
                });

                $item->authorize(
                    $this->auth->hasAccess('portal.general.index') || $this->auth->hasAccess('portal.request.index') || $this->auth->hasAccess('portal.consultation.index')
                );

            });
        });

        return $menu;
    }
}
<?php

return [
    'portal.general' => [
        'index' => 'portal::portal.general',
    ],
    'portal.request' => [
        'index' => 'portal::portal.requests',
        'delete' => 'portal::portal.request.delete',
    ],
    'portal.consultation' => [
        'index' => 'portal::portal.consultation',
        'delete' => 'portal::portal.consultation.delete',
    ],
];


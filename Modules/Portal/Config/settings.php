<?php

return [
    'dms-sip-url' => [
        'description'  => 'URL del DMS para solicitudes de información:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'dms-sip-user' => [
        'description'  => 'Usuario del DMS para solicitudes de información *(Debe ser un usuario admin):',
        'view'         => 'text',
        'translatable' => false,
    ],
    'dms-sip-pass' => [
        'description'  => 'Contraseña del usuario de solicitudes de información:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'dms-io-url' => [
        'description'  => 'URL del DMS para información oficiosa:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'dms-io-user' => [
        'description'  => 'Usuario del DMS para información oficiosa *(Debe ser un usuario admin):',
        'view'         => 'text',
        'translatable' => false,
    ],
    'dms-io-pass' => [
        'description'  => 'Contraseña del usuario de información oficiosa:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'consultation_folder'  => [
        'description'  => 'Carpeta de consultas ciudadanas:',
        'view'         => 'number',
        'translatable' => false,
    ],
    'request_folder' => [
        'description'  => 'Carpeta de solicitudes de información:',
        'view'         => 'number',
        'translatable' => false,
    ],
    'sender-address' => [
        'description'  => 'Dirección email para envío de correos automáticos:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'notifications-address' => [
        'description'  => 'Dirección email para recibir notificaciones:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'request_workflow' => [
        'description'  => 'Identificador del flujo de trabajo para las solicitudes de información:',
        'view'         => 'number',
        'translatable' => false,
    ],
    'consultation_workflow' => [
        'description'  => 'Identificador del flujo de trabajo para las consultas ciudadanas:',
        'view'         => 'number',
        'translatable' => false,
    ],
    'request_category' => [
        'description'  => 'Categoría para las solicitudes de información:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'consultation_category' => [
        'description'  => 'Categoría para las consultas ciudadanas:',
        'view'         => 'text',
        'translatable' => false,
    ],
    'days-to-verification' => [
        'description'  => 'Número de días para verificar de nuevo el correo de los solicitantes:',
        'view'         => 'number',
        'translatable' => false,
    ],
    'file_size' => [
        'description'  => 'Cantidad de bytes permitidos por archivo de subida:',
        'view'         => 'number',
        'translatable' => false,
    ],
];

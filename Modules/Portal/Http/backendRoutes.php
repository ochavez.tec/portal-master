<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/portal'], function (Router $router) {
	$router->get('index', [
		'as' => 'admin.portal.portal.index', 
		'uses' => 'PortalController@index'
	]);

	$router->get('requests/{year?}', [
		'as' => 'admin.portal.portal.request', 
		'uses' => 'PortalController@request'
	]);

	$router->get('consultation/{year?}', [
		'as' => 'admin.portal.portal.consultation', 
		'uses' => 'PortalController@consultation'
	]);

	$router->post('emails', [
        'as' => 'admin.portal.portal.storeEmails',
        'uses' => 'PortalController@storeEmails',
    ]);

    $router->post('styles', [
        'as' => 'admin.portal.portal.storeStyles',
        'uses' => 'PortalController@storeStyles',
    ]);

    $router->post('delete-persons', [
        'as' => 'admin.portal.portal.deleteSelectedPerson',
        'uses' => 'PortalController@deleteSelectedPerson',
    ]);

    $router->post('delete-pcpersons', [
        'as' => 'admin.portal.portal.deleteSelectedPCPerson',
        'uses' => 'PortalController@deleteSelectedPCPerson',
    ]);

    $router->get('person-requests/{id}', [
        'as' => 'admin.portal.portal.findRequests',
        'uses' => 'PortalController@findRequests',
    ]);

    $router->get('person-consultations/{id}', [
        'as' => 'admin.portal.portal.findConsultations',
        'uses' => 'PortalController@findConsultations',
    ]);

    $router->get('delete-person/{id}', [
        'as' => 'admin.portal.portal.deletePerson',
        'uses' => 'PortalController@deletePerson', 
    ]);

    $router->get('delete-pcperson/{id}', [
        'as' => 'admin.portal.portal.deletePCPerson',
        'uses' => 'PortalController@deletePCPerson', 
    ]);

    $router->get('delete-request/{id}/{key}', [
        'as' => 'admin.portal.portal.deleteRequest',
        'uses' => 'PortalController@deleteRequest', 
    ]);

    $router->get('delete-consultation/{id}/{key}', [
        'as' => 'admin.portal.portal.deleteConsultation',
        'uses' => 'PortalController@deleteConsultation', 
    ]);

});

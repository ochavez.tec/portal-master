<?php

use Illuminate\Routing\Router;

/** @var Router $router */
if (! App::runningInConsole()) {

    $router->get('solicitud-informacion', 'RequestController@index');
    $router->get('solicitud-informacion/exitosa', 'RequestController@success');
    $router->get('solicitud-informacion/no-encontrado', 'RequestController@notFound');

    $router->get('solicitud-datos', 'RequestController@requestData');

    $router->get('consulta/exitosa', 'ConsultationController@success');
    $router->get('informacion/no-encontrado', 'ConsultationController@notFound');

    $router->get('foro/pc/{token}', 'ConsultationController@showForum')->where('token', '(.*)');
    $router->get('foro/{token}', 'RequestController@showForum')->where('token', '(.*)');

    $router->get('activate/pc/{token}', 'ConsultationController@activate')->where('token', '(.*)');
    $router->get('activate/{token}', 'RequestController@activate')->where('token', '(.*)');
    

    $router->get('lectura/{pdf?}', 'InfoController@index')->named('lectura/pdf');

    $router->get('documento/no-encontrado', 'InfoController@notFound');
    $router->get('resoluciones/{id}', 'InfoController@viewDocument')->name('reviewDocument');
    $router->get('resoluciones', 'InfoController@resolutions');

    $router->get('documento/{id}', 'InfoController@viewDocument')->name('reviewDocument');
    $router->get('documentos/{id}', 'InfoController@getDocuments')->name('getDocuments');
    $router->get('documentos/{thepage}/{id}', 'InfoController@getDocumentsByPage')->name('getDocumentsByPage');
    
    $router->get('descargar/{type}/{id}/{name}/{date}', 'InfoController@downloadDocument')->name('downloadDocument')->where('name', '(.*)');
    $router->get('mas-descargados', 'InfoController@mostDownloaded')->name('mostDownloaded');

    $router->get('diamante/{date?}', 'PagesController@diamond')->name('diamond')->where('date', '(.*)');
    $router->post('diamante', ['middleware' => 'verifyCsrf', 'uses' => 'PagesController@updateDiamond'])->name('diamond.update');

    $router->get('test', 'RequestController@test');

    $router->post('solicitud', ['middleware' => 'verifyCsrf', 'uses' => 'RequestController@store'])->name('request.store');
    $router->post('consulta', ['middleware' => 'verifyCsrf', 'uses' => 'ConsultationController@store'])->name('consultation.store');
    $router->post('buscar', ['middleware' => 'verifyCsrf', 'uses' => 'InfoController@search'])->name('info.search');

    $router->post('posts/saveComment', array('as' => 'saveComment', 'uses' => 'RequestController@storeComment'));
    $router->post('posts/savePCComment', array('as' => 'savePCComment', 'uses' => 'ConsultationController@storePCComment'));

    $router->post('posts/saveDownloadLog', array('as' => 'saveDownloadLog', 'uses' => 'RequestController@storeDownloadLog'));
    $router->post('posts/savePCDownloadLog', array('as' => 'savePCDownloadLog', 'uses' => 'ConsultationController@storePCDownloadLog'));
    
    $router->get('retrieveAttachs/{id}', array('as' => 'retrieveAttachs', 'uses' => 'InfoController@retrieveAttachs'));
    $router->get('downloadattach/{docid}/{filetype}/{fileid}', array('as' => 'downloadAttach', 'uses' => 'InfoController@downloadAttach'));
    $router->get('getforumattach/{docid}/{filetype}/{fileid}', array('as' => 'downloadForumAttach', 'uses' => 'InfoController@downloadForumAttach'));

    $router->get('mas-recientes', array('as' => 'mostRecent', 'uses' => 'InfoController@mostRecent'));

    $router->get('blog', 'BlogController@index');
}
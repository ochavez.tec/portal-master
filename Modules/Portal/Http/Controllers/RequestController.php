<?php

namespace Modules\Portal\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Modules\Portal\Http\Controllers\DMSRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Hash;
use Modules\Core\Http\Controllers\BasePublicController;
use Illuminate\Http\Request;
use Modules\Portal\Entities\Country;
use Modules\Portal\Entities\Person;
use Modules\Portal\Entities\PersonPhone;
use Modules\Portal\Entities\PersonDocument;
use Modules\Portal\Entities\InformationRequest;
use Modules\Portal\Entities\EmailVerification;
use Modules\Portal\Entities\Representative;
use Modules\Portal\Entities\RequestForum;
use Modules\Portal\Entities\InformationAccess;
use Modules\Portal\Entities\RequestNotification;
use Modules\Portal\Entities\EmailData;
use Modules\Portal\Entities\DownloadLog;
use Modules\Portal\Mail\SendEmailConfirmation;
use Modules\Portal\Mail\SendNotification;
use Illuminate\Support\Facades\Mail;
use Modules\Portal\Jobs\SendEmailConfirmationJob;
use Modules\Portal\Jobs\SendForumEmailJob;
use Modules\Portal\Jobs\SendNotificationJob;
use Modules\Setting\Contracts\Setting;
use Illuminate\Support\Facades\File;
use Carbon\Carbon;
use PDF;
use Image;

class RequestController extends BasePublicController {

    /**
     * Stores the person email
     */
    public $to;
    
    /**
     * Stores the person name
     */
    public $name;

    /**
     * Data array for email templates
     */
    public $data = array();

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
      $countries = Country::All();

      return view('request/request', compact('countries'));
    }

    public function requestData(){
      $countries = Country::All();

      return view('request/requestdata', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $therequest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $therequest){

        if ($therequest['g-recaptcha-response'] == '') {
          Session::flash('error', 'Error: Para enviar su consulta, primero debe dar clic al botón de reCAPTCHA');
          return Redirect::back()->withInput(Input::all());
        }

        $request = $therequest;

        // Check signatures
        $signature_draw = $therequest['signature-img'];
        if ($request->hasFile('signature_upload')) {
              if ($request->file('signature_upload')->isValid()) {
                $signature_upload_size = $this->isValidFileSize($request->file('signature_upload'));
                if (!$signature_upload_size) {
                  Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con archivos que superan el tamaño máximo permitido: '.setting("portal::file_size")." KB");
                  return Redirect::back()->withInput(Input::all());
                }
                $signature_upload = $request->file('signature_upload');
              }
        } else {
          $signature_upload = "null";
        }

        if ($signature_upload != "null") {
          $signature_mimetype = ".".pathinfo($signature_upload->getClientOriginalName(), PATHINFO_EXTENSION);
          if (strtolower($signature_mimetype) != ".png" && strtolower($signature_mimetype) != ".jpg") {
            Session::flash('error', 'Error en la firma adjuntada: Sólo se permiten imágenes con extensión .png o .jpg');
            return Redirect::back()->withInput(Input::all());
          }
        }

        if ($signature_upload == "null" && $signature_draw == "null") {
          Session::flash('error', 'Error: La solicitud debe contener la firma del solicitante, por favor adjunte su firma como una imagen o dibújela.');
          return Redirect::back()->withInput(Input::all());
        }

        // Person *************************************
        $name = $request['name'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $person_type = $request['person_type'];
        $occupation = $request['occupation'];
        $person_address = $request['person_address'];
        $email = $request['email'];

        // PersonDocument *****************************
        $document_type = $request['document_type'];
        $is_foreign = $request['is_foreign'];
        $document_number = $request['document_number'];
        $student_card_extended_by = $request['student_card_extended_by'];
        $passport_extended_by = $request['passport_extended_by'];

        // PersonPhone ********************************
        $cell_phone = $request['cell_phone'];
        $home_phone = $request['home_phone'];
        $office_phone = $request['office_phone'];
        
        // Representative *****************************
        $representative_name = $request['representative_name'];
        $representative_dui = $request['representative_dui'];
        $representative_address = $request['representative_address'];

        // InformationRequest *************************
        $info_requested = $request['info_requested'];
        
        // InformationAccess **************************
        $access_type = array();
        $access_types = array();
        $access_type[] = $request['access_type_one'];
        $access_type[] = $request['access_type_two'];
        $access_type[] = $request['access_type_three'];
        $access_type[] = $request['access_type_four'];
        $access_type[] = $request['access_type_five'];
        $access_type[] = $request['access_type_six'];
        $access_type[] = $request['access_type_seven'];

        $access_types = array();
        foreach ($access_type as $access_t) {
          if ($access_t != '') {
            $access_types[] = $access_t; 
          }
        }
        $access_types_counter = count($access_types);

        // RequestNotification ************************
        $notifications_target = $request['notifications_target'];

        if ($request['sne_notifications'] == '') {
          $sne_notifications = 0;
        } else {
          $sne_notifications = 1;
        }

        if ($request['sne_ceu'] == '') {
          $sne_ceu = "N/A";
        } else {
          $sne_ceu = $request['sne_ceu'];
        }

        // Validate person identification document
        if ($request->hasFile('document_pdf')) {
          if ($request->file('document_pdf')->isValid()) {
            $size_is_valid = $this->isValidFileSize($request->file('document_pdf'));
            if ($size_is_valid) {
              $temp_file = $request->file('document_pdf');
              $temp_mimetype = ".".pathinfo($temp_file->getClientOriginalName(), PATHINFO_EXTENSION);
              if (strtolower($temp_mimetype) != ".pdf") {
                  Session::flash('error', 'Error: Los archivos anexados en este formulario deben ser estrictamente ficheros PDF.');
                  return Redirect::back()->withInput(Input::all());

              } else {
                $temp_file = "";
              }

            } else {
              Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con archivos que superan el tamaño máximo permitido: '.setting("portal::file_size")." KB");
              return Redirect::back()->withInput(Input::all());
            }                   

          } else {
            Session::flash('error', 'Error: El archivo adjunto de su documento de identidad se encuentra en un estado corrupto. Por favor adjunte un archivo PDF válido.');
            return Redirect::back()->withInput(Input::all());
          }

        } else {
          Session::flash('error', 'Error: No ha adjuntado el archivo PDF del escaneado de su documento de identidad.');
          return Redirect::back()->withInput(Input::all());
        }


        // Empty inputs not are allowed
        if ($request['name'] == '' || $request['lastname'] == '' || $request['gender'] == null || $request['occupation'] == '' || $request['cell_phone'] == '' || $request['person_address'] == '' || $request['document_number'] == '' || $request['info_requested'] == '' || $request['notifications_target'] == '' || $request['email'] == '' || $access_types_counter == 0) {
          Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con uno o más campos obligatorios vacíos');
          return Redirect::back()->withInput(Input::all());
        }

        DB::beginTransaction();
        
        $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();

        try {

            switch ($person_type) {
                case '1':
                    $person_type_text = "Natural";
                    break;
                case '2':
                    $person_type_text = "Jurídica";
                    break;
                case '3':
                    $person_type_text = "Natural con representante";
                    break;
                default:
                    break;
            }
            
            
            $the_person_document = PersonDocument::where('document_number', $document_number)->first(); //null
            if ($the_person_document != null) {
              //$person = Person::where('id_person', $the_person_document->person_id)->first(); //null
$person = null;
            
} else {
              $person = null;
            }

            $verificationIsNeeded = false;

            $person_email = Person::where('email', $email)->first();
            if ($person_email != null) {
              if ($this->personEmailVerification($person_email)){
                $verificationIsNeeded = true;  
              } else {
                $verificationIsNeeded = false;
              }
            } else {
              $verificationIsNeeded = true;
            }

            if ($person == null) {
              // Create a Person
              $person = Person::create([
                'name' => $name,
                'lastname' => $lastname,
                'gender' => $gender,
                'person_type' => $person_type_text,
                'occupation' => $occupation,
                'address' => $person_address,
                'email' => $email,
              ]);
              $lastPersonId = $person->id_person;

              // Create a PersonPhone
              $person_phone = PersonPhone::create([
                'person_id' => $lastPersonId,
                'cell_phone' => $cell_phone,
                'home_phone' => $home_phone,
                'office_phone' => $office_phone,
              ]);

              $extended_by = "";
              $is_foreign_val = 0;

              switch ($document_type) {
                  case '1':
                      $person_document_text = "DUI";
                      break;
                  case '2':
                      $person_document_text = "Carnet de residente";
                      break;
                  case '3':
                      $person_document_text = "Carnet de minoridad";
                      $extended_by = $student_card_extended_by;
                      break;
                  case '4':
                      $person_document_text = "Pasaporte";
                      if ($is_foreign == '1') {
                        $is_foreign_val = 1;
                        $extended_by = $request['passport_extended_by'];    
                      }
                      break;

                  default:
                      break;
              }

              // Create a PersonDocument
              $person_document = PersonDocument::create([
                'person_id' => $lastPersonId,
                'document_type' => $person_document_text,
                'is_foreign' => $is_foreign_val,
                'document_number' => $document_number,
                'extended_by' => $extended_by,
              ]);

              if ($person_type == "2" || $person_type == "3") {
                if ($representative_name == "" || $representative_dui == "" || $representative_address == "") {
                  DB::rollback();
                  Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con uno o más campos obligatorios vacíos');
                  return Redirect::back()->withInput(Input::all());
                }

                // Create a Representative
                $representative = Representative::create([
                    'person_id' => $lastPersonId,
                    'name' => $representative_name,
                    'dui' => $representative_dui,
                    'address' => $representative_address,
                ]);
                $lastRepresentativeId = $representative->id_representative;
              }

            } else {

              // If person exists
              $lastPersonId = $person->id_person;
              
            }

            $right_type = array();

            if (isset($request['right_type_one'])) {
              $right_type[] = $request['right_type_one'];
            }

            if (isset($request['right_type_two'])) {
              $right_type[] = $request['right_type_two'];
            }            

            if (isset($request['right_type_three'])) {
              $right_type[] = $request['right_type_three'];
            }

            if (isset($request['right_type_four'])) {
              $right_type[] = $request['right_type_four'];
            }

            if (!empty($right_type)) {
              $the_right_type = implode(", ", $right_type);

            } else {
              $the_right_type = NULL;

            }

            $correlative_data = $this->calculateCorrelative();

            // Create the Request
            $requestInformation = InformationRequest::create([
                'correlative' => $correlative_data['correlative'],
                'year' => $correlative_data['year'],
                'person_id' => $lastPersonId, 
                'status' => 'En revisión',
                'request_token' => 'temp_code',
                'description' => $info_requested,
                'right_type' => $the_right_type,
            ]);
            $lastRequestId = $requestInformation->id_request;

            // Create a Hash code for the request
            $hash_code = Hash::make($email.$lastRequestId);

            // Update request token
            DB::table('portal__request')->where('id_request', $lastRequestId)->update(['request_token' => $hash_code]);

            // Create folder for the request
            $this->createLocalRequestFolder($requestInformation);

            // Save Document Person PDF
            $document_pdf = $request->file('document_pdf');
            $document_pdf->move($temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year, "documento_de_identidad_del_solicitante.pdf");

            // Annexed documents
            if ($request->hasFile('annexed_one')) {
              if ($request->file('annexed_one')->isValid()) {
                $size_is_valid_one = $this->isValidFileSize($request->file('annexed_one'));
                if (!$size_is_valid_one) {
                  DB::rollback();
                  Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con archivos que superan el tamaño máximo permitido: '.setting("portal::file_size")." KB");
                  return Redirect::back()->withInput(Input::all());
                }
                $annexed_one = $request->file('annexed_one');
                $annexed_one_mimetype = ".".pathinfo($annexed_one->getClientOriginalName(), PATHINFO_EXTENSION);
                if (strtolower($annexed_one_mimetype) != ".pdf") {
                      DB::rollback();
                      Session::flash('error', 'Error: Los archivos anexados en este formulario deben ser estrictamente ficheros PDF.');
                      return Redirect::back()->withInput(Input::all());                     
                }

                $annexed_one->move($temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year, "anexo_uno.pdf");
              }
            }

            if ($request->hasFile('annexed_two')) {
              if ($request->file('annexed_two')->isValid()) {
                $size_is_valid_two = $this->isValidFileSize($request->file('annexed_two'));
                if (!$size_is_valid_two) {
                  DB::rollback();
                  Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con archivos que superan el tamaño máximo permitido: '.setting("portal::file_size")." KB");
                  return Redirect::back()->withInput(Input::all());
                }
                $annexed_two = $request->file('annexed_two');
                $annexed_two_mimetype = ".".pathinfo($annexed_two->getClientOriginalName(), PATHINFO_EXTENSION);
                if (strtolower($annexed_two_mimetype) != ".pdf") {
                      DB::rollback();
                      Session::flash('error', 'Error: Los archivos anexados en este formulario deben ser estrictamente ficheros PDF.');
                      return Redirect::back()->withInput(Input::all());                     
                }

                $annexed_two->move($temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year, "anexo_dos.pdf");
              }
            }

            if ($request->hasFile('annexed_three')) {
              if ($request->file('annexed_three')->isValid()) {
                $size_is_valid_three = $this->isValidFileSize($request->file('annexed_three'));
                if (!$size_is_valid_three) {
                  DB::rollback();
                  Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con archivos que superan el tamaño máximo permitido: '.setting("portal::file_size")." KB");
                  return Redirect::back()->withInput(Input::all());
                }
                $annexed_three = $request->file('annexed_three');
                $annexed_three_mimetype = ".".pathinfo($annexed_three->getClientOriginalName(), PATHINFO_EXTENSION);
                if (strtolower($annexed_three_mimetype) != ".pdf") {
                      DB::rollback();
                      Session::flash('error', 'Error: Los archivos anexados en este formulario deben ser estrictamente ficheros PDF.');
                      return Redirect::back()->withInput(Input::all());                     
                }

                $annexed_three->move($temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year, "anexo_tres.pdf");
              }
            }

            // Register data if person_type is: Juridica o con Representante
            if ($person_type == "2" || $person_type == "3") {
                if (!$request->hasFile('representative_dui_pdf') || !$request->hasFile('representative_owner_pdf')) {
                    DB::rollback();
                    Session::flash('error', 'Error: Ha elegido una de las opciones: Persona Jurídica o Natural con representante, pero no ha adjuntado los archivos PDF necesarios.');
                    return Redirect::back()->withInput(Input::all());

                } else {

                    if($request->file('representative_dui_pdf')->isValid() && $request->file('representative_owner_pdf')->isValid()){
                      $size_is_valid_dui_file = $this->isValidFileSize($request->file('representative_dui_pdf'));
                      $size_is_valid_owner_file = $this->isValidFileSize($request->file('representative_owner_pdf'));

                      if (!$size_is_valid_dui_file || !$size_is_valid_owner_file) {
                        DB::rollback();
                        Session::flash('error', 'Error: Al parecer ud está intentando enviar una solicitud con archivos que superan el tamaño máximo permitido: '.setting("portal::file_size")." KB");
                        return Redirect::back()->withInput(Input::all());
                      }
                      
                      $representative_dui_file = $request->file('representative_dui_pdf');
                      $representative_owner_file = $request->file('representative_owner_pdf');

                      
                      $representative_dui_file_mimetype = ".".pathinfo($representative_dui_file->getClientOriginalName(), PATHINFO_EXTENSION);
                      $representative_owner_file_mimetype = ".".pathinfo($representative_owner_file->getClientOriginalName(), PATHINFO_EXTENSION);
                      if (strtolower($representative_dui_file_mimetype) != ".pdf" || strtolower($representative_owner_file_mimetype) != ".pdf") {
                          DB::rollback();
                          Session::flash('error', 'Error: Los archivos anexados en este formulario deben ser estrictamente ficheros PDF.');
                          return Redirect::back()->withInput(Input::all());
                          
                      }

                      $representative_dui_file->move($temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year, "dui_del_representante_o_apoderado.pdf");
                      $representative_owner_file->move($temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year, "poder_del_representante_o_apoderado.pdf");

                    } else {
                      DB::rollback();
                      Session::flash('error', 'Error: Ha elegido una de las opciones: Persona Jurídica o Natural con representante, pero los archivos que ud ha adjuntado se encuentran en un estado corrupto.');
                      return Redirect::back()->withInput(Input::all());
                    }
                }
            }

            // Create a InformationAccess
            if (isset($access_types[1]) && $access_types[1] != "") {
              $access_types_two = $access_types[1];
            } else {
              $access_types_two = "Ninguna";
            }

            $informationAccess = InformationAccess::create([
                'request_id' => $lastRequestId,
                'access_one' => $access_types[0],
                'access_two' => $access_types_two,
            ]);

            // Create a RequestNotification
            $requestNotification = RequestNotification::create([
                'request_id' => $lastRequestId,
                'notification_site' => $notifications_target,
                'sne_notification' => $sne_notifications,
                'sne_ceu' => $sne_ceu,
            ]);

            if ($verificationIsNeeded) {
              // Create a EmailVerification
              $email_verification = EmailVerification::create([
                  'person_id' => $lastPersonId,
                  'status' => false,
                  'token' => $hash_code,
              ]);

              // Send confirmation email
              $c_details = $this->getConfirmationEmailData();
              $c_details['email'] = $person->email;
              $c_details['token'] = $hash_code;

              $this->data = $c_details;
              $this->to = $person->email;
              $this->name = $person->name." ".$person->lastname;

              Mail::send('mails.confirmation', $this->data, function($message){
                    $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
                    $message->to($this->to, $this->name)->subject("Confirmación de correo electrónico");
              });

            } else {

              // Send forum email
              $fr_details = $this->getForumEmailData();
              $fr_details['email'] = $person->email;
              $fr_details['token'] = $hash_code;

              $this->data = $fr_details;
              $this->to = $person->email;
              $this->name = $person->name." ".$person->lastname;

              Mail::send('mails.forum', $this->data, function($message){
                    $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
                    $message->to($this->to, $this->name)->subject("Enlace al foro");
              });

            }
            

        } catch(\Exception $e) {
             // Get exception
            DB::rollback();

            \Log::error($e->getMessage(), [
              'file' => $e->getFile(),
              'line' => $e->getLine()
            ]);

            Session::flash('error', 'Error: Ha ocurrido un error al enviar su solicitud, lamentamos los inconvenientes. Por favor inténtelo de nuevo más tarde.');
            return Redirect::back()->withInput(Input::all());
        }
        
        DB::commit();

        if ($signature_draw != "null") {
          $temp_path_img = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
          $image = Image::make($signature_draw)->resize(300, 100);
          $image->save($temp_path_img.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year.'/signature-'.$lastRequestId.'.png');

        } else if ($signature_upload != "null") {
          $temp_path_img = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
          $signature_extension_mime = ".".pathinfo($signature_upload->getClientOriginalName(), PATHINFO_EXTENSION);

          $signature_upload_mime = strtolower($signature_extension_mime);

          $image = Image::make($signature_upload)->resize(300, 100);
          $image->save($temp_path_img.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year.'/signature-'.$lastRequestId.$signature_upload_mime);
        }

        // Save request pdf into temporary local folder
        $this->createRequestFile($lastPersonId, $lastRequestId);

        // Send an email notification to portal admin
        $this->sendPortalAdminNotification($hash_code, $person);

        Session::flash('success', "Solicitud enviada exitosamente, se ha enviado un correo a la dirección ingresada, por favor revise su bandeja de entrada.");
        return Redirect::to("solicitud-informacion/exitosa");
    }

    /**
     * Send an email notification to portal admin
     * @return void
     **/
    public function sendPortalAdminNotification($hash_code, $person){
      // SEND NOTIFICATION MESSAGE
        $notification_details = array();
        $notification_details['to'] = setting('portal::notifications-address');
        $notification_details['user'] = $person->name." ".$person->lastname;
        $notification_details['description'] = "Se ha creado una nueva solicitud a través del Portal de Transparencia. Al hacer clic sobre el enlace 'Ver foro', la solicitud se validará y creará en el SGAIP automáticamente, si ésta aún no ha sido validada por el solicitante. Por favor revisar y corroborar su estado en el Portal y SGAIP.";
        $notification_details['date'] = date("d-m-Y H:i:s");
        $notification_details['token'] = $hash_code;

        $this->data = $notification_details;
        $this->to = setting('portal::notifications-address');
        $this->name = "Agente de soporte";

        Mail::send('mails.newrequest', $this->data, function($message){
            $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
            $message->to($this->to, $this->name)->subject("Nueva solicitud creada");
        });
        //  ENDS SEND NOTIFICATION
    }

    /**
     * Calculate the current correlative for request
     * @return array
     **/
    public function calculateCorrelative(){
      $last_request = InformationRequest::orderby('id_request', 'desc')->first();
      $data = array();

      if ($last_request == null || empty($last_request)) {
        
        $data['correlative'] = 1;
        $data['year'] = date("Y");
      
      } else {
        
        $actual_year = date("Y");

        if(((int)$last_request->correlative <= 0) && ((int)$last_request->year <= 0)) { // Row colums equal 0
          
          if ((string)$last_request->created_at->format("Y") == (string)$actual_year) { // Created at has the same year to actual
          
            $data['correlative'] = (int)$last_request->id_request + 1;
            $data['year'] = $actual_year;
          
          } else {
          
            $data['correlative'] = 1;
            $data['year'] = $actual_year;
          
          }

        } else {

          if ((string)$last_request->created_at->format("Y") == (string)$actual_year) { // Created at has the same year to actual
            
            $data['correlative'] = (int)$last_request->correlative + 1;
            $data['year'] = $actual_year;

          } else {

            $data['correlative'] = 1;
            $data['year'] = $actual_year;

          }
        }
        
      }

      return $data;
    }

    /**
     * Verificate the filesize
     * @var Request File
     * @return bool
     */
    public function isValidFileSize($file){
      $kilobyte_size = (round($file->getSize()/1024));
      if ($kilobyte_size <= setting("portal::file_size")) {
        return true;
      } else {
        return false;
      }
    }

    /**
     * Creates Local Request Folder
     * @param $request
     */
    public function createLocalRequestFolder($request){
      Storage::disk('temp')->makeDirectory('sip-'.$request->correlative.'-'.$request->year);
    }

    /**
     * Delete Local Request Folder
     * @param id_request
     */
    public function deleteLocalRequestFolder($request){
      Storage::disk('temp')->deleteDirectory('sip-'.$request->correlative.'-'.$request->year);
    }

    /**
     * Store a newly comment for the forum.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeComment(Request $request){
        $request_id = $request->request_id;
        $comment = $request->comment;

        if ($comment === '') {
          $response = array('success' => false);
          return response()->json($response);
        }

        $informationRequest = InformationRequest::find($request_id);
        $person = Person::where('id_person', $informationRequest->person_id)->first();

        if (Auth::check()) {
            $is_logged = 1;    
        } else {
            $is_logged = 0;
        }
        
        DB::beginTransaction();

        try {

          $request_comment = RequestForum::create([
            'request_id' => $request_id,
            'message' => $comment,
            'is_logged' => $is_logged,
          ]);

        } catch (\Exception $e) {

          DB::rollback();

          \Log::error($e->getMessage(), [
            'file' => $e->getFile(),
            'line' => $e->getLine()
          ]);

          $response = array('success' => false);
          return response()->json($response);

        }

        DB::commit();

        $timestamp = strtotime($request_comment->created_at);
        $comment_created_at = date('d/m/Y H:i:s',$timestamp);

        // Send notification
        $details = array();
        if ($is_logged) {
          $details['to'] = $person->email;
          $details['user'] = "Agente de soporte";
          $details['description'] = $comment;
          $details['date'] = $comment_created_at;
          $details['token'] = $informationRequest->request_token;

          $this->data = $details;
          $this->to = $person->email;
          $this->name = $person->name." ".$person->lastname;

          Mail::send('mails.notification', $this->data, function($message){
            $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
            $message->to($this->to, $this->name)->subject("Nuevo mensaje en el foro");
          });

          //dispatch((new SendNotificationJob($details))->delay(3));

        } else {
          $details['to'] = setting('portal::notifications-address');
          $details['user'] = $person->name." ".$person->lastname;
          $details['description'] = $comment;
          $details['date'] = $comment_created_at;
          $details['token'] = $informationRequest->request_token;

          $this->data = $details;
          $this->to = setting('portal::notifications-address');
          $this->name = "Agente de soporte";

          Mail::send('mails.notification', $this->data, function($message){
            $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
            $message->to($this->to, $this->name)->subject("Nuevo mensaje en el foro");
          });          

          //dispatch((new SendNotificationJob($details))->delay(3));
        }

        $response = array(
          'success' => true,
          'msg' => $request_comment->message,
          'is_logged' => $is_logged,
          'created_at' => $comment_created_at,
        );

        return response()->json($response);
    }

    /**
     * Store an attachment download log.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDownloadLog(Request $request){
        $document_id = $request->document_id;
        $attach_id = $request->attach_id;

        $download_log = DownloadLog::where("attach_id", $attach_id)->first();

        if (!empty($download_log) && !is_bool($download_log)) {
          $response = array('success' => false);
          return response()->json($response);
        }
        
        DB::beginTransaction();

        try {

          $log_saved = DownloadLog::create([
            'document_id' => $document_id,
            'attach_id' => $attach_id,            
          ]);

        } catch (\Exception $e) {

          DB::rollback();

          \Log::error($e->getMessage(), [
            'file' => $e->getFile(),
            'line' => $e->getLine()
          ]);

          $response = array('success' => false);
          return response()->json($response);

        }

        DB::commit();

        $timestamp = strtotime($log_saved->created_at);
        $date_created_at = date('d-m-Y H:i:s',$timestamp);

        $response = array(
          'success' => true,
          'created_at' => $date_created_at,
        );

        return response()->json($response);
    }

    /**
     * Validate email, activate the request, 
     * create folder into DMS, 
     * create document into DMS for SIP and attach files to it
     *
     * @param $token
     * @return view
     *
     */ 
    public function activate($token){
      $email_verification = EmailVerification::whereToken($token)->first();

      if (null == $email_verification) {
        return Redirect::to('solicitud-informacion/no-encontrado');
      }

      if ($email_verification->status) {
        
        Session::flash('info', "Al parecer esta solicitud ya se encuentra activada");
        return Redirect::to('foro/'.$token);

      } else {
        
        // Update data
        DB::table('portal__email_verification')->where('token', $token)->update(['status' => 1]);
        DB::table('portal__request')->where('request_token', $token)->update(['status' => 'En proceso']);
        
        // Get data
        $request = InformationRequest::where('request_token', $token)->first();
        $person = Person::where('id_person', $request->person_id)->first();

        $this->processRequest($request, $person);

        // Send email
        $fr_details = $this->getForumEmailData();
        $fr_details['email'] = $person->email;
        $fr_details['token'] = $request->request_token;

        $this->data = $fr_details;
        $this->to = $person->email;
        $this->name = $person->name." ".$person->lastname;

        Mail::send('mails.forum', $this->data, function($message){
          $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
          $message->to($this->to, $this->name)->subject("Enlace al foro");
        });        

        //dispatch((new SendForumEmailJob($fr_details))->delay(3));

        
        // Return to forum
        Session::flash('success', "Hola! Su solicitud se encuentra en proceso, analizaremos la información que nos ha enviado para proveerle una respuesta a la brevedad posible. Se le ha enviado a la dirección de correo electrónico ingresada el enlace para que pueda mantenerse pendiente y verificar el estado de su solicitud, y por medio de esta conversación privada se le hará llegar la respuesta a su solicitud.");

        return Redirect::to('foro/'.$token);
      }
    }

    /**
     * Process documents and attachments for the request
     * @var Modules/Portal/Entities/Person $person
     * @var Modules/Portal/Entities/InformationRequest $request
     * @return void
     *
     */
    public function processRequest($request, $person){
        
        $path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
        $request_file = $path.'sip-'.$request->correlative.'-'.$request->year.'/sip-'.$request->correlative.'-'.$request->year.'.pdf';

        if (!file_exists($request_file)) {
          return Redirect::to('solicitud-informacion/no-encontrado');
        }

        // Create folder into DMS
        $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));
        $response = $dms_request->createRequestFolder(setting('portal::request_folder'), "SIP-".$request->correlative."-".$request->year, "Solicitud de información");
        $content = json_decode($response, true);

        // Create document into DMS for SIP
        if($content['success']){
          $document = $this->makeRequestDocument($content['data']['id'], $request, $person, setting('portal::request_workflow'), setting('portal::request_category'));

          if (is_bool($document) && !$document) {
            return Redirect::to('solicitud-informacion/no-encontrado');
          }

          if (isset($document['success']) && $document['success']) {
            $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();

            // Delete temporal request pdf in the local folder
            $bool = \File::delete($temp_path."sip-".$request->correlative."-".$request->year."/sip-".$request->correlative."-".$request->year.".pdf");

            // Delete temporal signature image in the local folder
            if(file_exists($temp_path."sip-".$request->correlative."-".$request->year."/signature-".$request->id_request.".png")){
              $bool = \File::delete($temp_path."sip-".$request->correlative."-".$request->year."/signature-".$request->id_request.".png");
            } else if(file_exists($temp_path."sip-".$request->correlative."-".$request->year."/signature-".$request->id_request.".jpg")) {
              $bool = \File::delete($temp_path."sip-".$request->correlative."-".$request->year."/signature-".$request->id_request.".jpg");
            }
            
            // Get all files in the request temp folder
            $files = \File::allFiles($temp_path."sip-".$request->correlative."-".$request->year);
            // Scan any file and send to DMS document like an attach
            foreach ($files as $file) {
              $file_name = $file->getFileName();
              $thefile = $file->getPathName();

              if ($file_name == "dui_del_representante_o_apoderado.pdf") {
                $attach_name = "DUI del representante o apoderado";
                $attach_comment = "DUI del representante o apoderado";

              } else if ($file_name == "documento_de_identidad_del_solicitante.pdf") {
                $attach_name = "Documento de identidad del solicitante";
                $attach_comment = "Documento de identidad del solicitante";

              } else if ($file_name == "poder_del_representante_o_apoderado.pdf") {
                $attach_name = "Poder del representante o apoderado";
                $attach_comment = "Poder del representante o apoderado";

              } else if ($file_name == "anexo_uno.pdf") {
                $attach_name = "Archivo anexo 1 de la solicitud";
                $attach_comment = "Archivo anexo 1 de la solicitud";

              } else if ($file_name == "anexo_dos.pdf") {
                $attach_name = "Archivo anexo 2 de la solicitud";
                $attach_comment = "Archivo anexo 2 de la solicitud";

              } else if ($file_name == "anexo_tres.pdf") {
                $attach_name = "Archivo anexo 3 de la solicitud";
                $attach_comment = "Archivo anexo 3 de la solicitud";

              } else {
                $attach_name = "Archivo adjunto de la solicitud";
                $attach_comment = "Archivo adjunto de la solicitud";
              }
              
              $this->uploadAttach($document['data']['id'], $attach_name, $attach_comment, $thefile);
              
              // Delete folder and files 
            }

            $bool = \File::deleteDirectory($temp_path."/sip-".$request->correlative."-".$request->year); //
          }
        }
    }

    /**
     * Show Forum
     *
     */
    public function showForum($token) {

        $request = InformationRequest::where("request_token", $token)->first();
        if ($request == null) {
          return Redirect::to('solicitud-informacion/no-encontrado');
        }
        
        $person = Person::where("id_person", $request->person_id)->first();

        $email_verification = EmailVerification::whereToken($token)->first();

        if (null != $email_verification && !$email_verification->status) {

          return Redirect::to('activate/'.$token);

        } else if (null != $email_verification && $email_verification->status) {

            $comments = RequestForum::All()->where('request_id', $request->id_request);

            if (Auth::check()) {
              $user_logged = 1;    
            } else {
              $user_logged = 0;
            }

            $comments_counter = 0;
            foreach ($comments as $comment) {
              $comments_counter++;
            }

            $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));
            $doc_name = "DOCSIP-".$request->correlative."-".$request->year;
            $response = $dms_request->getSIPDocumentByName($doc_name, $request->correlative, $request->year, $request->id_request);
            $content = json_decode($response, true);

            if (!$content['success']) {
              return Redirect::to('informacion/no-encontrado');
            }

            $attachs = $dms_request->getDocumentAttachs($content['data']['id']);
            $attachments = json_decode($attachs, true);
              
            $attach_files = array();
            if (isset($attachments['success']) && $attachments['success']) {
              if(!empty($attachments['data'])){
                $attach_files = $attachments['data'];
              }
            }

            $doc_id = $content['data']['id'];
            /*$doc_status = $content['data']['status_name'];
            $doc_status_date = $content['data']['workflow_status_date'];*/

            $doc_status = isset($content['data']['status_name']) ? $content['data']['status_name'] : "N/A";
            $doc_status_date = isset($content['data']['workflow_status_date']) ? $content['data']['workflow_status_date'] : "N/A";

            $temp_attachs = array();
              if (!empty($attach_files)) {
                foreach ($attach_files as $attach) {
                  $download_saved = DownloadLog::where("attach_id", $attach['download'])->first();

                  if ($download_saved) {
                    $timestamp = strtotime($download_saved->created_at);
                    $date_created_at = date('d-m-Y H:i:s',$timestamp);
                    
                    $data_saved = array('attach_id' => $download_saved->attach_id, 'created_at' => $date_created_at);
                    $attach['saved_log'] = $data_saved;
                  }

                  $temp_attachs[] = $attach;  
                }
            }

            return view('request/forum', ['person' => $person, 'request' => $request, 'comments' => $comments, 'user_logged' => $user_logged, 'comments_counter' => $comments_counter, 'doc_status' => $doc_status, 'doc_status_date' => $doc_status_date, 'attach_files' => $temp_attachs, 'doc_id' => $doc_id]);
        }

        if (null == $email_verification) {

          if (null != $request) {

            if ($request->status == "En revisión") {
              // Update request status
              DB::table('portal__request')->where('request_token', $token)->update(['status' => "En proceso"]);

              $this->processRequest($request, $person);

              // Return to forum
              Session::flash('success', "Hola! Su solicitud se encuentra en proceso, analizaremos la información que nos ha enviado para proveerle una respuesta a la brevedad posible. Se le ha enviado a la dirección de correo electrónico ingresada el enlace para que pueda mantenerse pendiente y verificar el estado de su solicitud, y por medio de esta conversación privada se le hará llegar la respuesta a su solicitud.");

              return Redirect::to('foro/'.$token);

            } else {

              $comments = RequestForum::All()->where('request_id', $request->id_request);

              if (Auth::check()) {
                  $user_logged = 1;    
              } else {
                  $user_logged = 0;
              }

              $comments_counter = 0;
              foreach ($comments as $comment) {
                $comments_counter++;
              }

              $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));
              $doc_name = "DOCSIP-".$request->correlative."-".$request->year;
              $response = $dms_request->getSIPDocumentByName($doc_name, $request->correlative, $request->year, $request->id_request);

              $content = json_decode($response, true);

              if (!$content['success']) {
                return Redirect::to('informacion/no-encontrado');
              }

              $attachs = $dms_request->getDocumentAttachs($content['data']['id']);
              $attachments = json_decode($attachs, true);
              
              $attach_files = array();
              if (isset($attachments['success']) && $attachments['success']) {
                if(!empty($attachments['data'])){
                  $attach_files = $attachments['data'];
                }
              }

              $doc_id = $content['data']['id'];
              $doc_status = isset($content['data']['status_name']) ? $content['data']['status_name'] : "N/A";
              $doc_status_date = isset($content['data']['workflow_status_date']) ? $content['data']['workflow_status_date'] : "N/A";

              $temp_attachs = array();
              if (!empty($attach_files)) {
                foreach ($attach_files as $attach) {
                  $download_saved = DownloadLog::where("attach_id", $attach['download'])->first();

                  if ($download_saved) {

                    $timestamp = strtotime($download_saved->created_at);
                    $date_created_at = date('d-m-Y H:i:s',$timestamp);

                    $data_saved = array('attach_id' => $download_saved->attach_id, 'created_at' => $date_created_at);
                    $attach['saved_log'] = $data_saved;
                  }

                  $temp_attachs[] = $attach;  
                }
              }


              return view('request/forum', ['person' => $person, 'request' => $request, 'comments' => $comments, 'user_logged' => $user_logged, 'comments_counter' => $comments_counter, 'doc_status' => $doc_status, 'doc_status_date' => $doc_status_date, 'attach_files' => $temp_attachs, 'doc_id' => $doc_id]);

            }

          } else {

            // If email_verification and request doesn't exists
            return Redirect::to('solicitud-informacion/no-encontrado');

          }
          
        }
    }   

    /**
     * Email Sender
     *
     */
    public function sendMail($objMail){
        $details['email'] = $objMail['to'];
        $details['token'] = $objMail['token'];
        dispatch((new SendEmailConfirmationJob($details))->delay(3));
    }

    /**
     * Upload attachments to DMS document
     *
     */
    public function uploadAttach($document_id, $attach_name, $attach_comment, $file){
      //$temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
      $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));

      // $thefile = 'request-78/poder_del_representante_o_apoderado.pdf'
      //$file = $temp_path.$thefile;

      $response = $dms_request->uploadDocumentAttach($document_id, $attach_name, $attach_comment, $file);
      $content = json_decode($response, true);

      if ($content['success']) {
        return true;
      } else {
        return false;
      }
    }

    /**
     * Fill array data for a document creation
     * @param Modules/Portal/Entities/InformationRequest $request
     * @param Modules/Portal/Entities/InformationRequest $person
     * @return array
     */
    public function fillDocumentData($request, $person){
      $informationAccess = InformationAccess::where('request_id', $request->id_request)->first();
      $requestNotification = RequestNotification::where('request_id', $request->id_request)->first();
      $personPhone = PersonPhone::where('person_id', $person->id_person)->first();
      $representative = Representative::where('person_id', $person->id_person)->first();
      $personDocument = PersonDocument::where('person_id', $person->id_person)->first();

      //$timestamp = strtotime($request->created_at); // This is when the form was submited with the request
      //$request_date = date("d/m/Y", $timestamp);
      //$request_hour = date("H:i:s", $timestamp);
      //$correlative = $request->id_request;

      if ($personDocument->is_foreign) {
        if ($personDocument->extended_by != "") {
            $country = Country::where('code', $personDocument->extended_by)->first();
            $document_extended_by = $country->name;
        }
      } else {
          $document_extended_by = $personDocument->extended_by;
      }

      if ($representative != null) {
        $representative_name = $representative->name;
        $representative_dui = $representative->dui;
        $representative_address = $representative->address;
      } else {
        $representative_name = "";
        $representative_dui = "";
        $representative_address = "";
      }

      if ($requestNotification->sne_notification) {
        $sne_notification = "Si";
      } else {
        $sne_notification = "No";
      }

      $document_data = [
            1 => $person->lastname,
            2 => $person->name,
            3 => $person->gender,
            4 => $person->occupation,
            5 => $personPhone->cell_phone,
            6 => $person->address,
            7 => $person->person_type,
            8 => $representative_name,
            9 => $representative_address,
            10 => $representative_dui,
            11 => $personDocument->document_type,
            12 => $personDocument->document_number,
            13 => $document_extended_by,
            14 => $request->description,
            15 => $requestNotification->notification_site,
            16 => $informationAccess->access_one,
            17 => $informationAccess->access_two,
            18 => $person->email,
            19 => $requestNotification->sne_ceu
      ];

      if($request->right_type != null){
        array_push($document_data, $request->right_type);
      }

      return $document_data;
    }

    public function makeRequestDocument($folderId, $request, $person, $workflow, $category){
      $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
      $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));

      $document_data = $this->fillDocumentData($request, $person);

      $file = $temp_path.'sip-'.$request->correlative.'-'.$request->year.'/sip-'.$request->correlative.'-'.$request->year.'.pdf';

      if (file_exists($file)) {

        $response = $dms_request->createRequestDocument($folderId, "DOCSIP-".$request->correlative.'-'.$request->year, "Solicitud de información", $document_data, $file, $workflow, $category, "sip");
        $content = json_decode($response, true);

        return $content;        
      
      } else {

        return false;

      }

    }

    public function createRequestFile($personId, $requestId){
        $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();

        $person = Person::where('id_person', $personId)->first();
        $requestInformation = InformationRequest::where('id_request', $requestId)->first();
        $informationAccess = InformationAccess::where('request_id', $requestInformation->id_request)->first();
        $requestNotification = RequestNotification::where('request_id', $requestInformation->id_request)->first();
        $personPhone = PersonPhone::where('person_id', $person->id_person)->first();
        $representative = Representative::where('person_id', $person->id_person)->first();
        $personDocument = PersonDocument::where('person_id', $person->id_person)->first();

        $timestamp = strtotime($requestInformation->created_at); // This is when the form was submited with the request
        $request_date = date("d/m/Y", $timestamp);
        $request_hour = date("H:i:s", $timestamp);
        $correlative = $requestInformation->id_request;

        if ($personDocument->is_foreign) {
          if ($personDocument->extended_by != "") {
            $country = Country::where('code', $personDocument->extended_by)->first();
            $document_extended_by = $country->name;
          }
        } else {
          $document_extended_by = $personDocument->extended_by;
        }

        // Save pdf into temporary local folder
        $pdf = PDF::loadView('pdf/request', [
          'request_date' => $request_date,
          'request_hour' => $request_hour,
          'correlative' => $correlative,
          'request' => $requestInformation, 
          'person' => $person, 
          'information_access' => $informationAccess,
          'request_notification' => $requestNotification,
          'person_document' => $personDocument,
          'document_extended_by' => $document_extended_by,
          'representative' => $representative,
          'person_phone' => $personPhone,
          'temp_path_img' => $temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year,
        ]);

        return $pdf->save($temp_path.'sip-'.$requestInformation->correlative.'-'.$requestInformation->year.'/sip-'.$requestInformation->correlative.'-'.$requestInformation->year.'.pdf');
    }

    /**
     * Verificate if it's necessary create a new row for email verification
     * @param Modules/Portal/Entities/Person $person
     * @return bool
     */
    public function personEmailVerification($person){
        // Last occurence
        $email_verification = DB::table('portal__email_verification')->where("person_id", $person->id_person)->orderBy('updated_at', 'desc')->first();

        if ($email_verification) {
          $actual_date = (int)date(time());
          $email_verification_date = (int)strtotime($email_verification->updated_at);

          $numDays = abs($email_verification_date - $actual_date)/60/60/24;

          if (round($numDays) >= (int)setting('portal::days-to-verification')) {
            return true;
          } else {
            return false;
          }

        } else {
          return true;
        }
        
        
    }


    /**
     * Get Forum Email Data
     * @return array
     **/
    public function getForumEmailData(){
      $fr_email = EmailData::find(2);
      $details = array();
        
      $details['title'] = $fr_email->title;
      $details['subtitle'] = $fr_email->subtitle;
      $details['text_01'] = $fr_email->text_block_1;
      $details['text_02'] = $fr_email->text_block_2;
      $details['text_03'] = $fr_email->text_block_3;
      $details['button'] = $fr_email->button_text;
      $details['footer'] = $fr_email->footer;

      return $details;
    }

    /**
     * Get Confirmation Email Data
     * @return array
     **/
    public function getConfirmationEmailData(){
      $c_email = EmailData::find(1);
      $details = array();
        
      $details['title'] = $c_email->title;
      $details['subtitle'] = $c_email->subtitle;
      $details['text_01'] = $c_email->text_block_1;
      $details['text_02'] = $c_email->text_block_2;
      $details['text_03'] = $c_email->text_block_3;
      $details['button'] = $c_email->button_text;
      $details['footer'] = $c_email->footer;

      return $details;
    }

    /**
     * Request successfully
     */
    public function success(){
        return view('request/success');
    }

    /**
     * Request not found
     */
    public function notFound(){
      return view('request/notfound');
    }

    /**
     * Create PDF
     *
     */
    public function createPDF($request_id){
      $request = InformationRequest::find($request_id);

      $pdf = PDF::loadView('pdf/request', ['request'=>$request]);
      return $pdf->download('Request.pdf');
    }

}

<?php

namespace Modules\Portal\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\DB;
use Modules\Core\Http\Controllers\BasePublicController;
use Illuminate\Http\Request;
use Modules\Portal\Http\Controllers\DMSRequest;
use Illuminate\Support\Facades\Redirect;
use Modules\Setting\Contracts\Setting;
use Modules\Portal\Entities\DocumentDownload;
use Modules\Portal\Entities\DownloadLog;

class InfoController extends BasePublicController
{

    /**
     * @var Setting
     */
    private $setting;

    public $most_recent = array();
    public $most_recent_counter = 0;

    public function __construct(Setting $setting) {
        $this->setting = $setting;
    }

    /**
     * Display indexinfo view.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pdf = 0){
        if ($pdf === 0) {
            return view('info/notfound');
        }

        $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));
        $res = $dms_request->getDocumentReview($pdf);
        $content = json_decode($res, true);

        if (isset($content['success']) && $content['success'] == false) {
            return view('info/notfound');
        } else {
            return view('info/reader', ['pdf'=>$pdf]);
        }   
    }

    /**
     * Makes a search.
     *
     * @return void
     */
    public function search(Request $request) {
        $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));

        $response = $dms_request->makeASearch($request['query']);
        $content = json_decode($response, true);

        $server = \Request::getSchemeAndHttpHost();

        $documents_order = array();
        if($content['success']){
            $documents_order = $this->array_sort_by_key($content['data'], "name", SORT_ASC); // Order by document name  
        }

        return view('info/result', ['result' => $documents_order, 'server' => $server]);
    }

    
    
    /**
     * Simple function to sort an array by a specific key. Maintains index association.
     *
     * @param      array   $array  The array
     * @param      <type>  $on     { key }
     * @param      <type>  $order  The order
     *
     * @return     array   ( array sorted by key )
     */
    public function array_sort_by_key($array, $on, $order=SORT_ASC) {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public function resolutions(){
        $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));

        $response = $dms_request->getFolderChildren($this->setting->get('portal::id-resolutions-folder'));
        $content = json_decode($response, true);

        $subfolders = $content['data']['root']['subfolders'];
        $server = \Request::getSchemeAndHttpHost();

        return view('info/resolutions', ['subfolders' => $subfolders, 'server' => $server]);
    }

    public function viewDocument($id = 0){
        if ($id == 0) {
            echo "<h3>Error 404: Oops! Ningún elemento ha sido encontrado.</h3>";
            echo "<p>No hemos podido encontrar la página o elemento buscado. Mientras tanto usted podría volver a la página principal o intentar realizar una nueva búsqueda.</p>";
        } else {
            $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));
            $res = $dms_request->getDocumentReview($id);

            $content = json_decode($res, true);

            if (isset($content['success']) && $content['success'] == false) {
                echo "<h3>Error 404: Oops! Ningún elemento ha sido encontrado.</h3>";
                echo "<p>No hemos podido encontrar la página o elemento buscado. Mientras tanto usted podría volver a la página principal o intentar realizar una nueva búsqueda.</p>";
            } else {
                header('Content-type: application/pdf');
                echo $res;
            }
        }
    }

    /**
     * Get all data from a folder id
     * @var int id
     * @return view 
     */
    public function getDocuments($id = 0){
        if ($id == 0 || $id == '') {
            return view('info/notfound');
        } else {
            $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));

            $res = $dms_request->getFolderChildren($id);
            $content = json_decode($res, true);

            if (!$content['success']) {
               return view('info/notfound');

            } else {

                //$server = \Request::getSchemeAndHttpHost();
                $output = $this->formatDocumentsView($content['data']['root'], 0, $content['data']['root']['id'], 0);

                return view('info/documents', ['id' => $id, 'output' => $output]);
            }
        }
    }

     /**
     * Get all data from a folder id
     * @var int id
     * @return view 
     */
    public function getDocumentsByPage($thepage, $id = 0){
        if ($id == 0 || $id == '') {
            return view('info/notfound');
        } else {
            $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));

            $res = $dms_request->getFolderChildren($id);
            $content = json_decode($res, true);

            

            if (!$content['success']) {
               return view('info/notfound');

            } else {

                //$server = \Request::getSchemeAndHttpHost();

                $output = $this->formatDocumentsView($content['data']['root'], 0, $content['data']['root']['id'], 0);

                switch ($thepage) {
                    case 'ge':
                        return view('info/estrategica', ['output' => $output]);                        
                        break;
                    case 'gf':
                        return view('info/financiera', ['output' => $output]);                        
                        break;
                    case 'gn':
                        return view('info/normativa', ['output' => $output]);                        
                        break;
                    case 'ga':
                        return view('info/administrativa', ['output' => $output]);
                        break;
                    case 'gj':
                        return view('info/judicial', ['output' => $output]);
                        break;
                    default:
                        return view('info/documents', ['output' => $output]);
                        break;
                }
            }
        }
    }

    public function closeFolderBox(){
        $output = '
            </div>
            </div>';

        return $output;
    }

    public function openFolderBox($folder_name, $counter, $folder_id, $tabulator){
        /* The first folder must be open */
        if ($counter == 0) {
            $output = '
            <div class="box box-primary box-solid" id="parent-div-'.$folder_id.'" style="margin-bottom:0;border-radius:0;">
            <div class="box-header with-border">
            <i class="fa fa-folder-o" id="parent-icon-'.$folder_id.'"></i>&nbsp;<h3 class="box-title '.$tabulator.' parent-title-'.$folder_id.'">';
        $output .= $folder_name;
        $output .= '</h3>
            <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool button-folder" rel="'.$folder_id.'" data-widget=""><i id="i-'.$folder_id.'" class="fa fa-minus"></i></button>
            </div>
            </div>
            <div class="box-body no-padding open" id="div-folder-'.$folder_id.'">';

        } else {
            $output = '
            <div class="box box-primary collapsed-box box-solid" id="parent-div-'.$folder_id.'" style="margin-bottom:0;border-radius:0;">
            <div class="box-header with-border">
            <i class="fa fa-folder-o" id="parent-icon-'.$folder_id.'"></i>&nbsp;<h3 class="box-title '.$tabulator.' parent-title-'.$folder_id.'">';
        $output .= $folder_name;
        $output .= '</h3>
            <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool button-folder" rel="'.$folder_id.'" data-widget=""><i id="i-'.$folder_id.'" class="fa fa-plus"></i></button>
            </div>
            </div>
            <div class="box-body no-padding close" id="div-folder-'.$folder_id.'">';   
        }

        return $output;
    }

    public function openDocumentTable($data){
        $server = \Request::getSchemeAndHttpHost();
        $output = '
                <div class="box-body table-responsive">
                <table class="documents-table table table-bordered table-striped">
                <thead>
                <tr>
                <th class="align-center">Tipo de archivo</th>
                <th class="align-center">Nombre del documento</th>
                <th class="align-center">Fecha de actualización</th>
                <th class="align-center">Vigente hasta</th>
                <th class="align-center">Unidad productora</th>
                <th class="align-center">Acciones</th>
                </tr>
                </thead>
                <tbody>';

        foreach ($data as $document) {
            if ($document['actual_version_status'] == '2') {
                $output .= '<tr>
                            <td class="align-center">';
                            
                if ($document['mimetype'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                    $output .= '<a href="/descargar/1/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'].'" download="'.$document['name'].'"><i class="fa fa-file-word-o fa-2x color-blue"></i></a>';
                } elseif ($document['mimetype'] == 'application/msword') {
                    $output .= '<a href="/descargar/2/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'].'" download="'.$document['name'].'"><i class="fa fa-file-word-o fa-2x color-blue"></i></a>';
                } elseif ($document['mimetype'] == 'application/pdf') {
                    $output .= '<a href="/descargar/3/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'].'" download="'.$document['name'].'"><i class="fa fa-file-pdf-o fa-2x color-red"></i></a>';
                } else {
                    $output .= '<p></p>';
                }

                $output .= '</td>';

                if ($document['name'] != '' || $document['name'] != null) {
                    $output .= '<td><p>'.$document['name'].'</p></td>';    
                } else {
                    $output .= '<td><p></p></td>';    
                }

                $output .= '<td class="align-center"><p>'.$document['actual_version_workflow_date_updated'].'</p></td>';

                $output .= '<td class="align-center"><p>'.$document['expires'].'</p></td>';

                $output .= '<td class="align-center"><p>'.$document['publisher_group'].'</p></td>';
                            
                $output .= '<td>';


                $output .= '<a tabindex="0" role="button" class="btn btn-info" data-toggle="popover" data-placement="left" data-trigger="focus" title="Checksum: Código de integridad del documento. Permite verificar que el contenido del documento no ha sido modificado durante la descarga." data-content="'.$document['checksum'].'"><i class="fa fa-lock"></i></a>';

                if ($document['mimetype'] == 'application/pdf') {
                    $output .= '<a type="button" title="Ver documento" data-id="'.$document['id'].'" class="btn btn-warning view-doc"><i class="fa fa-eye"></i></a>';
                }

                $output .= '<div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="fa fa-bars"></span>
                    <span class="sr-only">Opciones</span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" role="menu">';

                if ($document['attachments']) {
                    $output .= '<li><a href="#" data-id="'.$document['id'].'" class="see-attachments text-light-blue"><i class="fa fa-search"></i> Ver adjuntos</a></li>';
                }


                if ($document['mimetype'] == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                    $output .= '<li><a title="Descargar documento" href="'. '/descargar/2/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] .'" download="'. $document['name'] .'" class="text-green"><i class="fa fa-download"></i> Descargar documento</a></li>';

                } elseif ($document['mimetype'] == 'application/msword') {
                    $output .= '<li><a title="Descargar documento" href="'. '/descargar/1/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] .'" download="'. $document['name'] .'" class="text-green"><i class="fa fa-download"></i> Descargar documento</a></li>';
                } elseif ($document['mimetype'] == 'application/pdf') {
                    $output .= '<li><a title="Descargar documento" href="'. '/descargar/3/'.$document['id'].'/'.$document['name'].'/'.$document['actual_version_workflow_date_updated'] .'" download="'. $document['name'] .'" class="text-green"><i class="fa fa-download"></i> Descargar documento</a></li>';
                }
                        
                $output .= '<li><a title="Compartir en Facebook" href="'. 'https://www.facebook.com/sharer/sharer.php?u='.$server.'/lectura/'.$document['id'] .'" target="_blank" class="text-light-blue"><i class="fa fa-facebook-square"></i> Compartir en Facebook</a></li>';

                $output .= '<li><a title="Compartir en Twitter" href="'. 'https://twitter.com/home?status='.$server.'/lectura/'.$document['id'] .'" target="_blank" class="text-aqua"><i class="fa fa-twitter"></i> Compartir en Twitter</a></li>';

                $output .= '</ul></div>';

                $output .= '</td></tr>';
            }
        }

        $output .= '</tbody></table></div>';

        return $output;
    }

    public function formatDocumentsView($data, $counter = 0, $folder_id, $tabulator){
        $output = "";
        
        if ($data['type'] == 'folder' ) {
            $output .= $this->openFolderBox($data['name'], $counter, $folder_id, $tabulator);

            if (isset($data['documents'])) {
                if (isset($data['documents']['one_published'])) {
                    if ($data['documents']['one_published']) {
                        $output .= $this->openDocumentTable($data['documents']);    
                    }                
                }
            }

            $counter++;
            
            if (isset($data['subfolders'])) {
                
                foreach ($data['subfolders'] as $folder) {
                    $tabulator++;
                    $output .= $this->formatDocumentsView($folder, $counter, $folder['id'], $tabulator);
                    
                }
            } 
            
            $output .= $this->closeFolderBox();    
            
        }

        return $output;
    }

    public function downloadDocument($type = 0, $id = 0, $name, $date){
        if ($id == 0 || $id == "" || $type == "" || $name == "" || $date == "") {
            return Redirect::to("documento/no-encontrado");
        } else {
            $doc_id = $id;
            $doc_name = $name;
            $doc_date = $date;
            $file_type = $type;

            $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));

            $res = $dms_request->getDocumentDownload($doc_id);

            if ($res == null || $res == "") {
               return Redirect::to("documento/no-encontrado");
            } else {

                // Plus the document download counter
                $document_download = DocumentDownload::where('document_id', $doc_id)->first();

                if ($document_download == null) {
                    $doc_download_created = DocumentDownload::create([
                        'document_id' => $doc_id,
                        'name' => $doc_name,
                        'date' => $doc_date,
                        'downloads' => 1,
                    ]);
                } else {
                    $counter = $document_download->downloads + 1;
                    
                    $doc_download_updated = DocumentDownload::where('document_id', $doc_id)->update([
                        'downloads' => $counter,
                    ]);
                }                

                switch ($file_type) {
                    case 0:
                        return Redirect::to("documento/no-encontrado");
                        break;

                    case 1:
                        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                        header('Content-Disposition: attachment;filename="'.$doc_name.'.docx"');     
                        echo($res);
                        break;

                    case 2:
                        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                        header('Content-Disposition: attachment;filename="'.$doc_name.'.doc"');          
                        echo($res);
                        break;

                    case 3:
                        header('Content-type: application/pdf');
                        header('Content-disposition: attachment;filename="'.$doc_name.'.pdf"');           
                        echo($res);
                        break;

                    default:
                        return Redirect::to("documento/no-encontrado");
                        break;
                }
            }
        }
    }

    /**
     * Document not found
     */
    public function notFound(){
      return view('info/notfound');
    }

    public function mostDownloaded(){
        $server = \Request::getSchemeAndHttpHost();
        $document_downloads = DB::table('portal__document_download')->orderBy('downloads', 'desc')->limit(10)->get();
        
        $data = array();
        $i = 0;
        if (null != $document_downloads) {
            foreach ($document_downloads as $document) {
                $data[] = $document;    
            }
            
        } else {
            $data[] = null;
        }

        return view('info/mostdownloaded', ['data' => $data, 'server' => $server]);
    }

    public function retrieveAttachs($docId){
      $dms_request = new DMSRequest(setting('portal::dms-io-url'), setting('portal::dms-io-user'), setting('portal::dms-io-pass'));
      $response = $dms_request->getDocumentAttachs($docId);

      return response($response);

    }

    public function downloadAttach($doc_id, $type, $file_id){
       
        if ($doc_id == 0 || $doc_id == "" || $type == "" || $file_id == "") {
            return Redirect::to("documento/no-encontrado");
        } else {

            $file_type = $type;

            $dms_request = new DMSRequest(setting('portal::dms-io-url'), setting('portal::dms-io-user'), setting('portal::dms-io-pass'));
            $response = $dms_request->downloadFileAttach($doc_id, $file_id);

            switch ($file_type) {
                    case 1: // PDF
                        header('Content-type: application/pdf');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.pdf"');
                        echo $response;
                        break;

                    case 2: // DOCX
                        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                        header('Content-Disposition: attachment;filename="csj_portal_file_download.docx"');          
                        echo $response;
                        break;

                    case 3: // XLSX
                        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.xlsx"');
                        echo $response;
                        break;

                    case 4: // PPTX
                        header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.pptx"');
                        echo $response;
                        break;

                    case 5: // DOC
                        header('Content-type: application/msword');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.doc"');
                        echo $response;
                        break;

                    case 6: // XLS
                        header('Content-type: application/vnd.ms-excel');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.xls"');
                        echo $response;
                        break;

                    case 7: // PPT
                        header('Content-type: application/vnd.ms-powerpoint');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.ppt"');
                        echo $response;
                        break;

                    case 8: // RAR
                        header('Content-type: application/x-rar');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.rar"');
                        echo $response;
                        break;

                    case 9: // ZIP
                        header('Content-type: application/zip');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.zip"');
                        echo $response;
                        break;

                    case 10: // MP3
                        header('Content-type: audio/mp3');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.mp3"');
                        echo $response;
                        break;

                    case 11: // WMA
                        header('Content-type: audio/x-ms-wma');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.wma"');
                        echo $response;
                        break;

                    case 12: // AVI
                        header('Content-type: video/x-msvideo');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.avi"');
                        echo $response;
                        break;

                    case 13: // MP4
                        header('Content-type: video/mp4');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.mp4"');
                        echo $response;
                        break;

                    default:
                        break;                    

            }
        }
    }

    public function downloadForumAttach($doc_id, $type, $file_id){

        /**
         * Check if attach was already register
         */
        $download_log = DownloadLog::where("attach_id", $file_id)->first();

        if (empty($download_log) || $download_log == null) {
            DB::beginTransaction();

            try {

              $log_saved = DownloadLog::create([
                'document_id' => $doc_id,
                'attach_id' => $file_id,            
              ]);

            } catch (\Exception $e) {

              DB::rollback();

              \Log::error($e->getMessage(), [
                'file' => $e->getFile(),
                'line' => $e->getLine()
              ]);
            }

            DB::commit(); 
        }        


        if ($doc_id == 0 || $doc_id == "" || $type == "" || $file_id == "") {
            return Redirect::to("documento/no-encontrado");
        } else {

            $file_type = $type;

            $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));
            $response = $dms_request->downloadFileAttach($doc_id, $file_id);

            switch ($file_type) {
                    case 1: // PDF
                        header('Content-type: application/pdf');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.pdf"');
                        echo $response;
                        break;

                    case 2: // DOCX
                        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                        header('Content-Disposition: attachment;filename="csj_portal_file_download.docx"');          
                        echo $response;
                        break;

                    case 3: // XLSX
                        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.xlsx"');
                        echo $response;
                        break;

                    case 4: // PPTX
                        header('Content-type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.pptx"');
                        echo $response;
                        break;

                    case 5: // DOC
                        header('Content-type: application/msword');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.doc"');
                        echo $response;
                        break;

                    case 6: // XLS
                        header('Content-type: application/vnd.ms-excel');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.xls"');
                        echo $response;
                        break;

                    case 7: // PPT
                        header('Content-type: application/vnd.ms-powerpoint');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.ppt"');
                        echo $response;
                        break;

                    case 8: // RAR
                        header('Content-type: application/x-rar');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.rar"');
                        echo $response;
                        break;

                    case 9: // ZIP
                        header('Content-type: application/zip');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.zip"');
                        echo $response;
                        break;

                    case 10: // MP3
                        header('Content-type: audio/mp3');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.mp3"');
                        echo $response;
                        break;

                    case 11: // WMA
                        header('Content-type: audio/x-ms-wma');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.wma"');
                        echo $response;
                        break;

                    case 12: // AVI
                        header('Content-type: video/x-msvideo');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.avi"');
                        echo $response;
                        break;

                    case 13: // MP4
                        header('Content-type: video/mp4');
                        header('Content-disposition: attachment;filename="csj_portal_file_download.mp4"');
                        echo $response;
                        break;

                    default:
                        break;                    

            }
        }
    }

    public function mostRecent(){
        $server = \Request::getSchemeAndHttpHost();
        $dms_request = new DMSRequest($this->setting->get('portal::dms-io-url'), $this->setting->get('portal::dms-io-user'), $this->setting->get('portal::dms-io-pass'));

        $res = $dms_request->getMostRecentDocs();
        $content = json_decode($res, true);

        if (!$content['success']) {
            return view('info/notfound');
        } else {
            $this->recursive_order_array($content['data']);
            return view('info/mostrecent', ['data' => $this->most_recent, 'server' => $server]);
        }
    }

    public function recursive_order_array($arr) {
        
        foreach ($arr as $index => $value){
            if (is_array($value) && !isset($value['type']) ){
                $this->recursive_order_array($value);
            } else if ($this->most_recent_counter < 10) {
                $this->most_recent_counter++;
                array_push($this->most_recent, $value);
            }
        }
    }
}

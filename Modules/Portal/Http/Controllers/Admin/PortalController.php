<?php namespace Modules\Portal\Http\Controllers\Admin;

use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Modules\Portal\Entities\EmailData;
use Modules\Portal\Entities\Person;
use Modules\Portal\Entities\PCPerson;
use Modules\Portal\Entities\PCRequest;
use Modules\Portal\Entities\InformationRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

use Modules\Setting\Entities\Setting;
use Modules\User\Contracts\Authentication;


class PortalController extends AdminBaseController {

    /**
     * Loads general configuration for portal module on backend
     */
	public function index(){

        $auth = app(Authentication::class);
        $user_allowed = $auth->hasAccess('portal.general.index');
        if(!$user_allowed){
            return Redirect::to('/backend');
        }

        $style_data = $this->getStyleData();
        $the_data = $this->getEmailData();

        return view('portal::admin.index', ['data' => $the_data, 'style_data' => $style_data]);
    }

    /**
     * Process the request by one/any year and returns the result for consultations
     *
     * @param      \Illuminate\Http\Request  $year   The year
     *
     * @return     Laravel view
     */
    public function consultation(Request $year){

        $auth = app(Authentication::class);
        $user_allowed = $auth->hasAccess('portal.consultation.index');
        $delete_allowed = $auth->hasAccess('portal.consultation.delete');

        if(!$user_allowed){
            return Redirect::to('/backend');
        }

        if($year == null){
            
            $persons = PCPerson::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->orderBy('id_pc_person', 'DESC')->get();
            $current_year =  date('Y');

            return view('portal::admin.consultation.consultation', ['persons' => $persons, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed ]);

        } else if(isset($year['persons_year'])) {

            $theyear = $year['persons_year'];

            if (is_numeric($theyear)) {
                $persons = PCPerson::where( DB::raw('YEAR(created_at)'), '=', (int)$theyear )->orderBy('id_pc_person', 'DESC')->get();
                $current_year =  (int)$theyear;                
            } else {
                $persons = PCPerson::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->orderBy('id_pc_person', 'DESC')->get();
                $current_year =  date('Y');               
            }

            return view('portal::admin.consultation.consultation', ['persons' => $persons, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed ]);

        } else if(isset($year['requests_year'])) {

            $theyear = $year['requests_year'];

            if (is_numeric($theyear)) {
                $requests = PCRequest::where( DB::raw('YEAR(created_at)'), '=', (int)$theyear )->orderBy('id_pc_request', 'DESC')->get();
                $current_year =  (int)$theyear;                
            } else {
                $requests = PCRequest::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->orderBy('id_pc_request', 'DESC')->get();
                $current_year =  date('Y');               
            }

            return view('portal::admin.consultation.consultation', ['requests' => $requests, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed ]);

        } else {

            $persons = PCPerson::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->get();
            $current_year =  date('Y');

        }        
        
        return view('portal::admin.consultation.consultation', ['persons' => $persons, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed ]);
    }

     /**
     * Process the request by one/any year and returns the result for requests
     *
     * @param      \Illuminate\Http\Request  $year   The year
     *
     * @return     Laravel view
     */
    public function request(Request $year){
       
        $auth = app(Authentication::class);
        $user_allowed = $auth->hasAccess('portal.request.index');
        $delete_allowed = $auth->hasAccess('portal.request.delete');

        if(!$user_allowed){
            return Redirect::to('/backend');
        }

        if($year == null){
            
            $persons = Person::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->orderBy('id_person', 'DESC')->get();
            $current_year =  date('Y');

            return view('portal::admin.request.request', ['persons' => $persons, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed]);

        } else if(isset($year['persons_year'])) {

            $theyear = $year['persons_year'];

            if (is_numeric($theyear)) {
                $persons = Person::where( DB::raw('YEAR(created_at)'), '=', (int)$theyear )->orderBy('id_person', 'DESC')->get();
                $current_year =  (int)$theyear;                
            } else {
                $persons = Person::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->orderBy('id_person', 'DESC')->get();
                $current_year =  date('Y');               
            }

            return view('portal::admin.request.request', ['persons' => $persons, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed]);

        } else if(isset($year['requests_year'])) {

            $theyear = $year['requests_year'];

            if (is_numeric($theyear)) {
                $requests = InformationRequest::where( DB::raw('YEAR(created_at)'), '=', (int)$theyear )->orderBy('id_request', 'DESC')->get();
                $current_year =  (int)$theyear;                
            } else {
                $requests = InformationRequest::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->orderBy('id_request', 'DESC')->get();
                $current_year =  date('Y');               
            }

            return view('portal::admin.request.request', ['requests' => $requests, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed]);

        } else {

            $persons = Person::where( DB::raw('YEAR(created_at)'), '=', date('Y') )->get();
            $current_year =  date('Y');

        }        
        
        return view('portal::admin.request.request', ['persons' => $persons, 'current_year' => $current_year, 'delete_allowed' => $delete_allowed]);
    }

    /**
     * Save data for automatic emails
     *
     * @param      \Illuminate\Http\Request  $request
     *
     * @return     Laravel view
     */
    public function storeEmails(Request $request){
        $cn_title = (null != $request['cn_title'] && $request['cn_title'] != "") ? $request['cn_title'] : "";
        $cn_subtitle = (null != $request['cn_subtitle'] && $request['cn_subtitle'] != "") ? $request['cn_subtitle'] : "";
        $cn_text_01 = (null != $request['cn_text_01'] && $request['cn_text_01'] != "") ? $request['cn_text_01'] : "";
        $cn_text_02 = (null != $request['cn_text_02'] && $request['cn_text_02'] != "") ? $request['cn_text_02'] : "";
        $cn_text_03 = (null != $request['cn_text_03'] && $request['cn_text_03'] != "") ? $request['cn_text_03'] : "";
        $cn_button = (null != $request['cn_button'] && $request['cn_button'] != "") ? $request['cn_button'] : "";
        $cn_footer = (null != $request['cn_footer'] && $request['cn_footer'] != "") ? $request['cn_footer'] : "";

        $fr_title = (null != $request['fr_title'] && $request['fr_title'] != "") ? $request['fr_title'] : "";
        $fr_subtitle = (null != $request['fr_subtitle'] && $request['fr_subtitle'] != "") ? $request['fr_subtitle'] : "";
        $fr_text_01 = (null != $request['fr_text_01'] && $request['fr_text_01'] != "") ? $request['fr_text_01'] : "";
        $fr_text_02 = (null != $request['fr_text_02'] && $request['fr_text_02'] != "") ? $request['fr_text_02'] : "";
        $fr_text_03 = (null != $request['fr_text_03'] && $request['fr_text_03'] != "") ? $request['fr_text_03'] : "";
        $fr_button = (null != $request['fr_button'] && $request['fr_button'] != "") ? $request['fr_button'] : "";
        $fr_footer = (null != $request['fr_footer'] && $request['fr_footer'] != "") ? $request['fr_footer'] : "";

        $cn_email = EmailData::find(1); //null
        if ($cn_email == null) {
            $cn_email = EmailData::create([
                'title' => $cn_title,
                'subtitle' => $cn_subtitle,
                'text_block_1' => $cn_text_01,
                'text_block_2' => $cn_text_02,
                'text_block_3' => $cn_text_03,
                'button_text' => $cn_button,
                'footer' => $cn_footer,
            ]);
        } else {
            $cn_email = EmailData::where('id_email_data', 1)->update([
                'title' => $cn_title,
                'subtitle' => $cn_subtitle,
                'text_block_1' => $cn_text_01,
                'text_block_2' => $cn_text_02,
                'text_block_3' => $cn_text_03,
                'button_text' => $cn_button,
                'footer' => $cn_footer,
            ]);
        }

        $fr_email = EmailData::find(2); //null
        if ($fr_email == null) {
            $fr_email = EmailData::create([
                'title' => $fr_title,
                'subtitle' => $fr_subtitle,
                'text_block_1' => $fr_text_01,
                'text_block_2' => $fr_text_02,
                'text_block_3' => $fr_text_03,
                'button_text' => $fr_button,
                'footer' => $fr_footer,
            ]);
        } else {
            $fr_email = EmailData::where('id_email_data', 2)->update([
                'title' => $fr_title,
                'subtitle' => $fr_subtitle,
                'text_block_1' => $fr_text_01,
                'text_block_2' => $fr_text_02,
                'text_block_3' => $fr_text_03,
                'button_text' => $fr_button,
                'footer' => $fr_footer,
            ]);
        }

        $style_data = $this->getStyleData();
        $the_data = $this->getEmailData();

        return view('portal::admin.index', ['data' => $the_data, 'style_data' => $style_data]);
    }

    /**
     * Save data for general styles and javascript on the backend
     *
     * @param      \Illuminate\Http\Request  $request
     *
     * @return     Laravel view
     */
    public function storeStyles(Request $request){
        $style_css = (null != $request['style-css'] && $request['style-css'] != "") ? $request['style-css'] : "";
        $script_js = (null != $request['script-js'] && $request['script-js'] != "") ? $request['script-js'] : "";

        //$fillable = ['name', 'value', 'description', 'isTranslatable', 'plainValue'];

        $style = DB::table('setting__settings')->where('name', 'portal::style_css')->first();
        
        if ($style == null) {
            $style = Setting::create([
                'name' => "portal::style_css",
                'plainValue' => $style_css,
                'isTranslatable' => 0,
            ]);
        } else {
            $style = Setting::where('name', "portal::style_css")->update([
                'plainValue' => $style_css,
            ]);
        }

        $script = DB::table('setting__settings')->where('name', 'portal::script_js')->first();
        if ($script == null) {
            $script = Setting::create([
                'name' => "portal::script_js",
                'plainValue' => $script_js,
                'isTranslatable' => 0,
            ]);
        } else {
            $script = Setting::where('name', "portal::script_js")->update([
                'plainValue' => $script_js,
            ]);
        }

        $style_data = $this->getStyleData();
        $the_data = $this->getEmailData();

        return view('portal::admin.index', ['data' => $the_data, 'style_data' => $style_data]);

    }

    /**
     * Get data for automatic emails
     *
     * @return     array    Automatic email data
     */
    public function getEmailData(){
        $emails = EmailData::all();

        $i = 0;

        if (null != $emails) {
            foreach ($emails as $email) {
                $data[$i]['title'] = $email->title;
                $data[$i]['subtitle'] = $email->subtitle;
                $data[$i]['text_01'] = $email->text_block_1;
                $data[$i]['text_02'] = $email->text_block_2;
                $data[$i]['text_03'] = $email->text_block_3;
                $data[$i]['button'] = $email->button_text;
                $data[$i]['footer'] = $email->footer;

                $i++;
            } 
        } else {
            $data[0] = null;
        }
        
        
        return $data;
    }

    /**
     * Get data for general styles
     *
     * @return     array    Styles css/javascript data
     */
    public function getStyleData(){
        $style_css = DB::table('setting__settings')->where('name', 'portal::style_css')->first();
        $script_js = DB::table('setting__settings')->where('name', 'portal::script_js')->first();
        $data = array();

        $data[0] = (null != $style_css && $style_css != "") ? $style_css->plainValue : "";
        $data[1] = (null != $script_js && $script_js != "") ? $script_js->plainValue : "";
        
        return $data;
    }

    /**
     * Get all consultations from a person
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     json     Consultations founded
     */
    public function findConsultations(Request $request){
        $consultations = DB::table('portal__pc_request')->where('pc_person_id', (int)$request->id)->orderBy('created_at', 'DESC')->get();

        $auth = app(Authentication::class);
        $user_allowed = $auth->hasAccess('portal.consultation.delete');

        $data = array();
        $response = array();
        if ($consultations != null && !empty($consultations)) {
            $i = 0;

            $data[$i]['success'] = true;
            
            foreach ($consultations as $consultation) {
                $date = strtotime($consultation->created_at);
                $the_date = date('d-m-Y', $date);

                $data[$i] = array(
                    'id' => $consultation->id_pc_request,
                    'correlative' => $consultation->correlative,
                    'year' => $consultation->year,
                    'status' => $consultation->status,
                    'request_token' => $consultation->request_token,
                    'created_at' => $the_date,
                );
                $i++;
            }

            $response['data'] = $data; 
            $response['permission'] = $user_allowed;

        } else {
            $response['data'] = false;
        }      

        return response()->json($response);
    }

    /**
     * Get all request from a person
     *
     * @param      \Illuminate\Http\Request  $request  The request
     *
     * @return     json     Requests founded
     */
    public function findRequests(Request $request){
        $requests = DB::table('portal__request')->where('person_id', (int)$request->id)->orderBy('created_at', 'DESC')->get();

        $auth = app(Authentication::class);
        $user_allowed = $auth->hasAccess('portal.request.delete');

        $data = array();
        $response = array();
        if ($requests != null) {
            $i = 0;
            $data[$i]['success'] = true;

            foreach ($requests as $request) {
                $date = strtotime($request->created_at);
                $the_date = date('d-m-Y', $date);

                $data[$i] = array(
                    'id' => $request->id_request,
                    'correlative' => $request->correlative,
                    'year' => $request->year,
                    'type' => $request->right_type,
                    'status' => $request->status,
                    'request_token' => $request->request_token,
                    'created_at' => $the_date,
                );
                $i++;
            }

            $response['data'] = $data; 
            $response['permission'] = $user_allowed;

        } else {
            $response['data'] = false;
        }      

        return response()->json($response);
    }

    public function deleteSelectedPerson(Request $request){
        $person = Person::where('id_person', $request['person_id'])->first();
        
        DB::beginTransaction();

        if ($person) {
            try {

                $person->delete();

            } catch (\Exception $e) {
                DB::rollback();

                Session::flash('error', 'Error: Ha ocurrido un error al eliminar el registro. Por favor contacte al administrador.');
                return Redirect::to('backend/portal/requests');

            }

            DB::commit();

            Session::flash('success', "Registro eliminado exitosamente.");
            return Redirect::to('backend/portal/requests');    

        } else {

            Session::flash('warning', 'Advertencia: Al parecer el registro indicado no existe.');
            return Redirect::to('backend/portal/requests');
        }

    }

    public function deleteSelectedPCPerson(Request $request){
        $person = PCPerson::where('id_pc_person', $request['person_id'])->first();
        
        DB::beginTransaction();

        if ($person) {
            try {

                $person->delete();

            } catch (\Exception $e) {
                DB::rollback();

                Session::flash('error', 'Error: Ha ocurrido un error al eliminar el registro. Por favor contacte al administrador.');
                return Redirect::to('backend/portal/consultation');

            }

            DB::commit();

            Session::flash('success', "Registro eliminado exitosamente.");
            return Redirect::to('backend/portal/consultation');    

        } else {

            Session::flash('warning', 'Advertencia: Al parecer el registro indicado no existe.');
            return Redirect::to('backend/portal/consultation');
        }

    }

    public function deletePerson($id){

        $auth = app(Authentication::class);
        $user_allowed = $auth->hasAccess('portal.request.delete');

        if(!$user_allowed){
            return Redirect::to('/backend');
        } 

        $person = Person::where('id_person', $id)->first();

        if(empty($person) || false === $person || $person == null) {
            $message = "La persona solicitada no existe.<br><br>";
            $message_data = "";
        } else if((int)$person->getTotalRequest()) {
            $message = "Al parecer la persona solicitada aún posee registros de solicitudes que deben ser eliminados manualmente.<br><br>";
            $message_data = "";
        } else {
            $message = "Se encuentra a punto de eliminar los registros de la persona: ";
            $message_data = "<b>Nombre:</b> ".$person->name." ".$person->lastname."<br>";
            $message_data .= "<b>Tipo de documento:</b> ".$person->personDocument->document_type."<br>";
            $message_data .= "<b>Número de documento:</b> ".$person->personDocument->document_number."<br>";
            $message_data .= "<b>Correo:</b> ".$person->email."<br><br>";
            $message_data .= "Las acciones de borrado son permanentes y no permiten recuperación futura de los datos.<br><br>";    
        }

        return view('portal::admin.request.delete', ['id' => $id, 'message' => $message, 'message_data' => $message_data]);
    }

    public function deletePCPerson($id){

        $auth = app(Authentication::class);
        $user_allowed = $auth->hasAccess('portal.consultation.delete');

        if(!$user_allowed){
            return Redirect::to('/backend');
        }        

        $person = PCPerson::where('id_pc_person', $id)->first();

        if(empty($person) || false === $person || $person == null) {
            $message = "La persona solicitada no existe.<br><br>";
            $message_data = "";
        } else if((int)$person->getTotalPCRequest()) {
            $message = "Al parecer la persona solicitada aún posee registros de consultas que deben ser eliminados manualmente.<br><br>";
            $message_data = "";
        } else {
            $message = "Se encuentra a punto de eliminar los registros de la persona: ";
            $message_data = "<b>Nombre:</b> ".$person->name."<br>";
            $message_data .= "<b>Correo:</b> ".$person->email."<br>";
            $message_data .= "<b>Número de teléfono:</b> ".$person->phone."<br><br>";
            $message_data .= "Las acciones de borrado son permanentes y no permiten recuperación futura de los datos.<br><br>";    
        }

        return view('portal::admin.consultation.delete', ['id' => $id, 'message' => $message, 'message_data' => $message_data]);
    }

    public function deleteRequest($id, $key){

        $request = InformationRequest::where('id_request', $id)->first();

        if ($key == "deleterequest" && $request != null) {
            
            $correlative = $request->correlative;
            $year = $request->year;
            
            DB::beginTransaction();

            try {
                                
                $request->delete();

            } catch (\Exception $e) {
                DB::rollback();

                $response = array('success' => false,'msg' => "Ha ocurrido un error");
                return response()->json($response);
            }

            DB::commit();

            $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
            if(File::isDirectory($temp_path.'sip-'.$correlative.'-'.$year)){
                Storage::disk('temp')->deleteDirectory('sip-'.$correlative.'-'.$year);
            }

            $response = array('success' => true,'msg' => "Borrado exitosamente");
            return response()->json($response);

        } else {
            $response = array('success' => false,'msg' => "Ha ocurrido un error");    
            return response()->json($response);
        }


    }

    public function deleteConsultation($id, $key){

        $consultation = PCRequest::where('id_pc_request', $id)->first();

        if ($key == "deleteconsultation" && $consultation != null) {            

            $correlative = $consultation->correlative;
            $year = $consultation->year;

            DB::beginTransaction();

            try {
                    
                $consultation->delete();

            } catch (\Exception $e) {
                DB::rollback();

                $response = array('success' => false,'msg' => "Ha ocurrido un error");
                return response()->json($response);
            }

            DB::commit();

            $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
            if(File::isDirectory($temp_path.'cc-'.$correlative.'-'.$year)){
                Storage::disk('temp')->deleteDirectory('cc-'.$correlative.'-'.$year);
            }

            $response = array('success' => true,'msg' => "Borrado exitosamente");
            return response()->json($response);

        } else {
            $response = array('success' => false,'msg' => "Ha ocurrido un error");    
            return response()->json($response);
        }

    }

}
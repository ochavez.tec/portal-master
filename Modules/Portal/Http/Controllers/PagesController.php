<?php

namespace Modules\Portal\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Illuminate\Http\Request;

class PagesController extends BasePublicController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function diamond($date = null){

        /*if ($date == null) {
            $date = date("Y-m-d");
        }*/

        if ($date == null) {
            $month = date('m');
            $year = date('Y');
        } else {
            list($month, $year) = explode("/", $date);
        }

        $resp = $this->makeDiamondChart($year, $month);

        $varTables = $this->makeVarTables($resp['data']->all_variables);

        return view('pages/diamond', ['chartjs' => $resp['chartjs'], 'data' => $resp['data']->axes, 'all_variables' => $varTables]);
    }

    public function makeVarTables($allVariables){

        $prev_axe = "";
        $k = 0;
        $axe = 1;
        $header = true;

        // Create tabs

        $output = '<div class="nav-tabs-custom">';
        $output .= '<ul class="nav nav-tabs">';

        foreach ($allVariables as $var) {
                if ($k == 0) {
                    $prev_inst = $var->instrument;
                    $prev_axe = $var->axe;
                    $is_active = 'active';
                } else {
                    $is_active = '';
                }

                if ($header) {
                    $output .= '<li class="'.$is_active.'"><a href="#tab-'.$axe.'" data-toggle="tab" aria-expanded="true">'.$var->axe.'</a></li>';
                    $header = false;
                }

                if ($var->axe != $prev_axe) {
                    $prev_axe = $var->axe;
                    $header = true;
                    $axe++;  
                }

                $k++;
                       
        }

        $output .= '</ul>';

        // Create tabs content

        $output .= '<div class="tab-content">';
        $prev_axe = "";
        $prev_inst = "";

        $l = 1;
        $m = 0;
        $orderAxes = array();
        $the_axe = array();
        foreach ($allVariables as $index => $var) {
            if ($l == 1) {
                $prev_axe = $var->axe;
                $prev_inst = $var->instrument;  
            }

            if ($var->axe == $prev_axe) {
                array_push($the_axe,$var);    
            }
            
            if ($var->axe != $prev_axe) {
                $orderAxes[$m] = $the_axe;
                unset($the_axe);
                $the_axe = array();
                $prev_axe = $var->axe;

                array_push($the_axe,$var);

                $m++;
            }

            // Last var
            if (count($allVariables) == $l) {
                $orderAxes[$m] = $the_axe;
            }

            $l++;
        }

        //var_dump($orderAxes);

        $k = 1;
        $n = 1;
        $header_three = true;
        $prev_inst = "";
        foreach ($orderAxes as $axe) {
            if ($k == 1) {
                $is_active = 'active';
            } else {
                $is_active = '';
            }
            
            $output .= '<div class="tab-pane '.$is_active.'" id="tab-'.$k.'">';

            foreach ($axe as $instrument) {
                if ($n == 1) {
                    $prev_inst = $instrument->instrument;
                    $output .= $this->getStartInstrumentBox($instrument->instrument);
                }

                if ($instrument->instrument != $prev_inst) {
                    $prev_inst = $instrument->instrument;
                    $output .= '</tbody>';
                    $output .= '</table>';
                    $output .= '</div>';
                    $output .= '</div>';
                    $output .= $this->getStartInstrumentBox($instrument->instrument);
                }

                

                if ($instrument->instrument === $prev_inst) {
                    if (isset($instrument->var) && isset($instrument->var_score)) {                    
                        if ($instrument->var !== null && $instrument->var_score !== null) {
                       
                        $output .= "<tr>";

                        $output .= "<td>";
                        $output .= $instrument->varCode." - ".$instrument->var;
                        $output .= "</td>";
                        
                        $output .= "<td class='align-center'>";
                        $output .= $instrument->var_weight;
                        $output .= "</td>";

                        $output .= "<td class='align-center'>";
                        if ($instrument->var_accomplished == 0) {
                            $output .= "<i class='fa fa-times color-red'></i>";
                        } else if ($instrument->var_accomplished > 0 && $instrument->var_accomplished < 1) {
                            $output .= "<span><strong>".(string)($instrument->var_accomplished*100)."%</strong></span>";
                        } else if ($instrument->var_accomplished == 1){
                            $output .= "<i class='fa fa-check color-green'></i>";
                        }
                        $output .= "</td>";
                        
                        $output .= "</tr>";

                        }
                    }
                }

                if (count($axe) === $n) {
                    $output .= '</tbody>';
                    $output .= '</table>';
                    $output .= '</div>';
                    $output .= '</div>';
                }

                $n++;
            }

            $output .= '</div>';

            $n = 1;
            $k++;
        }

        $output .= '</div>';
        $output .= '</div>';

        return $output;

    }

    public function getStartInstrumentBox($name){

        $output = '<div class="box box-info box-solid">';
        $output .= '<div class="box-header with-border">';
        $output .= '<h3 class="box-title">'.$name.'</h3>';
        $output .= '<div class="box-tools pull-right">';
        $output .= '<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>';
        $output .= '</button>';
        $output .= '</div>';         
        $output .= '</div>';        
        $output .= '<div class="box-body table-responsive">';
        $output .= '<table class="table table-condensed table-bordered">';
        $output .= '<thead>';
        $output .= '<row>';
        $output .= '<th class="align-center" width="60%">Variable</th>';
        $output .= '<th class="align-center" width="20%">Puntaje</th>';
        $output .= '<th class="align-center" width="20%">Cumplimiento</th>';
        $output .= '</row>';        
        $output .= '</thead>';
        $output .= '</tbody>';

        return $output;
    }

    public function makeDiamondChart($year, $month){ // This need your attention

        $dms_request = new DMSRequest(setting('portal::dms-io-url'), setting('portal::dms-io-user'), setting('portal::dms-io-pass'));
        $response = $dms_request->getDiamond($year, $month);
        $content = json_decode($response, true);
        $data = json_decode($content['data']);

        $labels = array();
        $datasets = array();
        foreach ($data->axes as $dt) {
            array_push($labels, $dt->name);
            array_push($datasets, $dt->score);            
        }

        if ($data != null) {
            $chartjs = app()->chartjs;
            
            $chartjs->name('radar');
            $chartjs->type('radar');
            $chartjs->size(['width' => 400, 'height' => 200]);        
            
            $chartjs->labels($labels);
            
            $chartjs->datasets([
                [
                    "label" => "Mes: ".$month." - Año: ".$year,
                    "fill" => true,
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => $datasets,
                ]
            ]);

            $chartjs->options([
                "title" => [
                    "display" => true,
                    "text" => "Estado del Diamante de Transparencia para la fecha:",
                ],
                "scales" => [
                    "yAxes" => [
                        "ticks" => [
                            "beginAtZero" => true,
                            "max" => 100,
                        ]
                    ]
                ],
            ]);

            $resp['chartjs'] = $chartjs;
            $resp['data'] = $data;

            return $resp;

        } else {

            $chartjs = app()->chartjs
            ->name('radar')
            ->type('radar')
            ->size(['width' => 400, 'height' => 200])
            ->labels(['Eficiencia', 'Institucionalidad', 'Rendición de Cuentas', 'Visibilidad'])
            ->datasets([
                [
                    "label" => $thedate,
                    "fill" => true,
                    'backgroundColor' => "rgba(38, 185, 154, 0.31)",
                    'borderColor' => "rgba(38, 185, 154, 0.7)",
                    "pointBorderColor" => "rgba(38, 185, 154, 0.7)",
                    "pointBackgroundColor" => "rgba(38, 185, 154, 0.7)",
                    "pointHoverBackgroundColor" => "#fff",
                    "pointHoverBorderColor" => "rgba(220,220,220,1)",
                    'data' => [0,0,0,0],
                ]
            ])
            ->options([
                "title" => [
                    "display" => true,
                    "text" => "No se encontraron datos para la fecha seleccionada",
                ],
            ]);

            return $chartjs;
        }

    }

    public function updateDiamond(Request $request){
        if ($request['date'] == '') {
            $month = date('m');
            $year = date('Y');
        } else { 
            list($month, $year) = explode("/", $request['date']);
        }

        $themonth = $month;
        $theyear = $year;

        $resp = $this->makeDiamondChart($theyear, $themonth);

        //var_dump($resp['data']->all_variables);

        $varTables = $this->makeVarTables($resp['data']->all_variables);

        return view('pages/diamond', ['chartjs' => $resp['chartjs'], 'data' => $resp['data']->axes, 'all_variables' => $varTables]);
    }
}

<?php

namespace Modules\Portal\Http\Controllers;

use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Modules\Portal\Http\Controllers\DMSRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Hash;
use Modules\Core\Http\Controllers\BasePublicController;
use Illuminate\Http\Request;
use Modules\Portal\Entities\PCPerson;
use Modules\Portal\Entities\PCRequest;
use Modules\Portal\Entities\PCEmailVerification;
use Modules\Portal\Entities\PCRequestForum;
use Modules\Portal\Entities\EmailData;
use Modules\Portal\Entities\DownloadLog;
use Modules\Portal\Jobs\SendEmailConfirmationJob;
use Modules\Portal\Jobs\SendForumEmailJob;
use Modules\Portal\Jobs\SendNotificationJob;
use Modules\Setting\Contracts\Setting;
use Illuminate\Support\Facades\Input;
use PDF;

class ConsultationController extends BasePublicController {
    
    /**
     * Stores the person email
     */
    public $to;
    
    /**
     * Stores the person name
     */
    public $name;

    /**
     * Data array for email templates
     */
    public $data = array();

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $therequest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $therequest){

        if ($therequest['g-recaptcha-response'] == '') {
          Session::flash('error', 'Error: Para enviar su consulta, primero debe dar clic al botón de reCAPTCHA');
          return Redirect::back()->withInput(Input::all());
        }

        $request = $therequest;

        // Empty inputs not are allowed
        if ($request['name'] == '' || $request['email'] == '' || $request['phone'] == '' || $request['subject'] == '' || $request['description'] == '') {
          Session::flash('error', 'Error: Al parecer ud está intentando enviar una consulta con uno o más campos obligatorios vacíos');
          return Redirect::back()->withInput(Input::all());
        }

        $name = (null != $request['name'] && $request['name'] != "") ? $request['name'] : "";
        $email = (null != $request['email'] && $request['email'] != "") ? $request['email'] : "";
        $phone = (null != $request['phone'] && $request['phone'] != "") ? $request['phone'] : "";
        $subject = (null != $request['subject'] && $request['subject'] != "") ? $request['subject'] : "";
        $description = (null != $request['description'] && $request['description'] != "") ? $request['description'] : "";

        DB::beginTransaction();
        
        $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();

        try {
                  
            $person_email = PCPerson::where('email', $email)->first();
            if ($person_email != null) {
              if ($this->personEmailVerification($person_email)){
                $verificationIsNeeded = true;  
              } else {
                $verificationIsNeeded = false;
              }
            } else {
              $verificationIsNeeded = true;
            }
            
            $person = PCPerson::create([
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
            ]);
            $lastPersonId = $person->id_pc_person;

            $correlative_data = $this->calculateCorrelative();             

            // Create the Request
            $requestInformation = PCRequest::create([
                'correlative' => $correlative_data['correlative'],
                'year' => $correlative_data['year'],
                'pc_person_id' => $lastPersonId, 
                'status' => 'En revisión',
                'request_token' => 'temp_code',
                'subject' => $subject,
                'description' => $description,
            ]);
            $lastRequestId = $requestInformation->id_pc_request;

            // Create a Hash code for the request
            $hash_code = Hash::make($email.$lastRequestId);

            // Update request token
            DB::table('portal__pc_request')->where('id_pc_request', $lastRequestId)->update(['request_token' => $hash_code]);

            if ($verificationIsNeeded) {
	            // Create a EmailVerification
	            $email_verification = PCEmailVerification::create([
	                  'pc_person_id' => $lastPersonId,
	                  'status' => false,
	                  'token' => $hash_code,
	            ]);

              // Send confirmation email
              $c_details = $this->getConfirmationEmailData();
              $c_details['email'] = $person->email;
              $c_details['token'] = "pc/".$hash_code;           

              $this->data = $c_details;
              $this->to = $person->email;
              $this->name = $person->name;

              Mail::send('mails.confirmation', $this->data, function($message){
                    $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
                    $message->to($this->to, $this->name)->subject("Confirmación de correo electrónico");
              });

            } else {

              // Send forum email
              $fr_details = $this->getForumEmailData();
              $fr_details['email'] = $person->email;
              $fr_details['token'] = "pc/".$hash_code;

              $this->data = $fr_details;
              $this->to = $person->email;
              $this->name = $person->name;

              Mail::send('mails.forum', $this->data, function($message){
                    $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
                    $message->to($this->to, $this->name)->subject("Enlace al foro");
              });
             
            }
            
            

        } catch(\Exception $e) {
             // Get exception
            DB::rollback();

            \Log::error($e->getMessage(), [
              'file' => $e->getFile(),
              'line' => $e->getLine()
            ]);

            Session::flash('error', 'Error: Ha ocurrido un error al enviar su consulta ciudadana, lamentamos los inconvenientes. Por favor inténtelo de nuevo más tarde.');
            return Redirect::back()->withInput(Input::all());
        }
        
        DB::commit();

        // Create folder for the request
        $this->createLocalConsultationFolder($requestInformation);

        // Save request pdf into temporary local folder
        $this->createRequestFile($lastPersonId, $lastRequestId);

        // Send an email notification to portal admin
        $this->sendPortalAdminNotification($hash_code, $person);

        Session::flash('success', "Consulta enviada exitosamente, se ha enviado un correo a la dirección ingresada, por favor revise su bandeja de entrada.");
        return Redirect::to("consulta/exitosa");
    }

    /**
     * Send an email notification to portal admin
     * @return void
     **/
    public function sendPortalAdminNotification($hash_code, $person){
      // SEND NOTIFICATION MESSAGE
        $notification_details = array();
        $notification_details['to'] = setting('portal::notifications-address');
        $notification_details['user'] = $person->name;
        $notification_details['description'] = "Se ha creado una nueva consulta a través del Portal de Transparencia. Al hacer clic sobre el enlace 'Ver foro', la consulta se validará y creará en el SGAIP automáticamente, si ésta aún no ha sido validada por el consultante. Por favor revisar y corroborar su estado en el Portal y SGAIP.";
        $notification_details['date'] = date("d-m-Y H:i:s");
        $notification_details['token'] = "pc/".$hash_code;

        $this->data = $notification_details;
        $this->to = setting('portal::notifications-address');
        $this->name = "Agente de soporte";

        Mail::send('mails.newconsultation', $this->data, function($message){
          $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
          $message->to($this->to, $this->name)->subject("Nueva consulta creada");
        });
        //  ENDS SEND NOTIFICATION
    }

    /**
     * Calculate the current correlative for request
     * @return array
     **/
    public function calculateCorrelative(){
      $last_pc_request = PCRequest::orderby('id_pc_request', 'desc')->first();
      $data = array();

      if ($last_pc_request === null || empty($last_pc_request)) {
        
        $data['correlative'] = 1;
        $data['year'] = date("Y");
      
      } else {
        
        $actual_year = date("Y");

        if(((int)$last_pc_request->correlative <= 0) && ((int)$last_pc_request->year <= 0)) { // Row colums equal 0
          
          if ((string)$last_pc_request->created_at->format("Y") == (string)$actual_year) { // Created at has the same year to actual
          
            $data['correlative'] = (int)$last_pc_request->id_pc_request + 1;
            $data['year'] = $actual_year;
          
          } else {
          
            $data['correlative'] = 1;
            $data['year'] = $actual_year;
          
          }

        } else {

          if ((string)$last_pc_request->created_at->format("Y") == (string)$actual_year) { // Created at has the same year to actual
            
            $data['correlative'] = (int)$last_pc_request->correlative + 1;
            $data['year'] = $actual_year;

          } else {            
            
            $data['correlative'] = 1;
            $data['year'] = $actual_year;

          }
        }
        
      }

      return $data;
    }

    /**
     * Get Forum Email Data
     * @return array
     **/
    public function getForumEmailData(){
      $fr_email = EmailData::find(2);
      $details = array();
        
      $details['title'] = $fr_email->title;
      $details['subtitle'] = $fr_email->subtitle;
      $details['text_01'] = $fr_email->text_block_1;
      $details['text_02'] = $fr_email->text_block_2;
      $details['text_03'] = $fr_email->text_block_3;
      $details['button'] = $fr_email->button_text;
      $details['footer'] = $fr_email->footer;

      return $details;
    }

    /**
     * Get Confirmation Email Data
     * @return array
     **/
    public function getConfirmationEmailData(){
      $c_email = EmailData::find(1);
      $details = array();
        
      $details['title'] = $c_email->title;
      $details['subtitle'] = $c_email->subtitle;
      $details['text_01'] = $c_email->text_block_1;
      $details['text_02'] = $c_email->text_block_2;
      $details['text_03'] = $c_email->text_block_3;
      $details['button'] = $c_email->button_text;
      $details['footer'] = $c_email->footer;

      return $details;
    }


    /**
     * Creates Local Request Folder
     * @param consultation
     */
    public function createLocalConsultationFolder($consultation){
      Storage::disk('temp')->makeDirectory('cc-'.$consultation->correlative."-".$consultation->year);
    }

    /**
     * Store a newly comment for the forum.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePCComment(Request $request){
        $request_id = $request->request_id;
        $comment = $request->comment;

        if ($comment === '') {
          $response = array('success' => false);
          return response()->json($response);
        }

        $pcrequest = PCRequest::find($request_id);
        $pcperson = PCPerson::where('id_pc_person', $pcrequest->pc_person_id)->first();

        if (Auth::check()) {
            $is_logged = 1;    
        } else {
            $is_logged = 0;
        }

        DB::beginTransaction();

        try {

          $request_comment = PCRequestForum::create([
            'pc_request_id' => $request_id,
            'message' => $comment,
            'is_logged' => $is_logged,
          ]);
          
        } catch (\Exception $e) {
          
          DB::rollback();

          \Log::error($e->getMessage(), [
            'file' => $e->getFile(),
            'line' => $e->getLine()
          ]);

          $response = array('success' => false);
          return response()->json($response);

        }

        DB::commit();

        $timestamp = strtotime($request_comment->created_at);
        $comment_created_at = date('d/m/Y H:i:s',$timestamp);

        // Send notification
        $details = array();
        if ($is_logged) {
          $details['to'] = $pcperson->email;
          $details['user'] = "Agente de soporte";
          $details['description'] = $comment;
          $details['date'] = $comment_created_at;
          $details['token'] = "pc/".$pcrequest->request_token;

          $this->data = $details;
          $this->to = $pcperson->email;
          $this->name = $pcperson->name;

          Mail::send('mails.notification', $this->data, function($message){
            $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
            $message->to($this->to, $this->name)->subject("Nuevo mensaje en el foro");
          });

          //dispatch((new SendNotificationJob($details))->delay(3));//////////

        } else {

          $details['to'] = setting('portal::notifications-address');
          $details['user'] = $pcperson->name;
          $details['description'] = $comment;
          $details['date'] = $comment_created_at;
          $details['token'] = "pc/".$pcrequest->request_token;

          $this->data = $details;
          $this->to = setting('portal::notifications-address');
          $this->name = "Agente de soporte";

          Mail::send('mails.notification', $this->data, function($message){
            $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
            $message->to($this->to, $this->name)->subject("Nuevo mensaje en el foro");
          });

          //dispatch((new SendNotificationJob($details))->delay(3)); ////

        }

        $response = array(
          'success' => true,
          'msg' => $request_comment->message,
          'is_logged' => $is_logged,
          'created_at' => $comment_created_at,
        );

        return response()->json($response);
    }

    /**
     * Store an attachment download log.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePCDownloadLog(Request $request){
        $document_id = $request->document_id;
        $attach_id = $request->attach_id;

        $download_log = DownloadLog::where("attach_id", $attach_id)->first();

        if (!empty($download_log) && !is_bool($download_log)) {
          $response = array('success' => false);
          return response()->json($response);
        }
        
        DB::beginTransaction();

        try {

          $log_saved = DownloadLog::create([
            'document_id' => $document_id,
            'attach_id' => $attach_id,            
          ]);

        } catch (\Exception $e) {

          DB::rollback();

          \Log::error($e->getMessage(), [
            'file' => $e->getFile(),
            'line' => $e->getLine()
          ]);

          $response = array('success' => false);
          return response()->json($response);

        }

        DB::commit();

        $timestamp = strtotime($log_saved->created_at);
        $date_created_at = date('d-m-Y H:i:s',$timestamp);

        $response = array(
          'success' => true,
          'created_at' => $date_created_at,
        );

        return response()->json($response);
    }

    /**
     * Validate email, activate the request, 
     * create folder into DMS, 
     * create document into DMS for SIP and attach files to it
     *
     * @param $token
     * @return view
     *
     */ 
    public function activate($token){
      $email_verification = PCEmailVerification::whereToken($token)->first();

      if (null == $email_verification) {
        return Redirect::to('informacion/no-encontrado');
      }

      if ($email_verification->status) {
        
        Session::flash('info', "Al parecer esta consulta ciudadana ya se encuentra activada");
        return Redirect::to('foro/pc/'.$token);

      } else {
        
        // Update data
        DB::table('portal__pc_email_verification')->where('token', $token)->update(['status' => 1]);
        DB::table('portal__pc_request')->where('request_token', $token)->update(['status' => 'En proceso']);
        
        // Get data
        $request = PCRequest::where('request_token', $token)->first();
        $person = PCPerson::where('id_pc_person', $request->pc_person_id)->first();

        $this->processPCRequest($request, $person);

        // Send forum email
        $fr_details = $this->getForumEmailData();
        $fr_details['email'] = $person->email;
        $fr_details['token'] = "pc/".$token;

        $this->data = $fr_details;
        $this->to = $person->email;
        $this->name = $person->name;

        Mail::send('mails.forum', $this->data, function($message){
          $message->from(setting('portal::sender-address'), 'Portal de Transparencia');
          $message->to($this->to, $this->name)->subject("Enlace al foro");
        });

        // Return to forum
        Session::flash('success', "Hola! Su consulta se encuentra en proceso, analizaremos la información que nos ha enviado para proveerle una respuesta a la brevedad posible. Se le ha enviado a la dirección de correo electrónico ingresada el enlace para que pueda mantenerse pendiente y verificar el estado de su consulta, y por medio de esta conversación privada se le hará llegar la respuesta correspondiente.");

        return Redirect::to('foro/pc/'.$token);
      }
    }

    /**
     * Process documents and attachments for the request
     * @var Modules/Portal/Entities/Person $person
     * @var Modules/Portal/Entities/PCRequest $request
     * @return void
     *
     */
    public function processPCRequest($request, $person){
      // Create folder into DMS

      $thetemp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
      $file = $thetemp_path.'cc-'.$request->correlative.'-'.$request->year.'/cc-'.$request->correlative.'-'.$request->year.'.pdf';

      if (file_exists($file)) {
        $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));
        $response = $dms_request->createRequestFolder(setting('portal::consultation_folder'), "CC-".$request->correlative."-".$request->year, "Consulta Ciudadana");
        $content = json_decode($response, true);

        // Create document into DMS for SIP
        if($content['success']){
          $document = $this->makePCRequestDocument($content['data']['id'], $request, $person, setting('portal::consultation_workflow'), setting('portal::consultation_category'));

          if (is_bool($document) && !$document) {
            return Redirect::to('solicitud-informacion/no-encontrado');
          }
          
          if (isset($document['success']) && $document['success']) {
            $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
            $bool = \File::delete($temp_path."cc-".$request->correlative."-".$request->year."/cc-".$request->correlative."-".$request->year.".pdf");
            $bool = \File::deleteDirectory($temp_path."/cc-".$request->correlative."-".$request->year);
          }
        }
      } else {
        return Redirect::to('informacion/no-encontrado');
      }
    }

    /**
     * Show Forum
     *
     */
    public function showForum($token) {

        $request = PCRequest::where("request_token", $token)->first();
        if ($request == null) {
          return Redirect::to('informacion/no-encontrado');
        }

        $person = PCPerson::where("id_pc_person", $request->pc_person_id)->first();
        $email_verification = PCEmailVerification::whereToken($token)->first();

        if (null != $email_verification && !$email_verification->status) {
          return Redirect::to('activate/pc/'.$token);
        } else if (null != $email_verification && $email_verification->status) {

            $comments = PCRequestForum::All()->where('pc_request_id', $request->id_pc_request);

            if (Auth::check()) {
              $user_logged = 1;
            } else {
              $user_logged = 0;
            }

            $comments_counter = 0;
            foreach ($comments as $comment) {
              $comments_counter++;
            }

            $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));
            //$doc_name = "doccc-".$request->id_pc_request;
            $doc_name = "doccc-".$request->correlative."-".$request->year;
            $response = $dms_request->getCCDocumentByName($doc_name, $request->correlative, $request->year, $request->id_pc_request);
            $content = json_decode($response, true);

            if (!$content['success']) {
              return Redirect::to('informacion/no-encontrado');
            }

            $attachs = $dms_request->getDocumentAttachs($content['data']['id']);
            $attachments = json_decode($attachs, true);
              
            $attach_files = array();
            if (isset($attachments['success']) && $attachments['success']) {
              if(!empty($attachments['data'])){
                $attach_files = $attachments['data'];
              }
            }

            $doc_id = $content['data']['id'];
            //$doc_status = $content['data']['status_name'];
            //$doc_status_date = $content['data']['workflow_status_date'];
            $doc_status = isset($content['data']['status_name']) ? $content['data']['status_name'] : "N/A";
            $doc_status_date = isset($content['data']['workflow_status_date']) ? $content['data']['workflow_status_date'] : "N/A";

            //$attach_files = array();
            //var_dump($content['data']);
            $attributes = $content['data']['attributes'];
            $cc_response = "";
            foreach ($attributes as $attr) {
              if ($attr['id'] == 85 && $content['data']['is_published']) {
                $cc_response = $attr['value'];
              }
            }

            $temp_attachs = array();
              if (!empty($attach_files)) {
                foreach ($attach_files as $attach) {
                  $download_saved = DownloadLog::where("attach_id", $attach['download'])->first();

                  if ($download_saved) {
                    $timestamp = strtotime($download_saved->created_at);
                    $date_created_at = date('d-m-Y H:i:s',$timestamp);
                    
                    $data_saved = array('attach_id' => $download_saved->attach_id, 'created_at' => $date_created_at);
                    $attach['saved_log'] = $data_saved;
                  }

                  $temp_attachs[] = $attach;  
                }
            }


            return view('consultation/forum', ['person' => $person, 'request' => $request, 'comments' => $comments, 'user_logged' => $user_logged, 'comments_counter' => $comments_counter, 'doc_status' => $doc_status, 'doc_status_date' => $doc_status_date, 'attach_files' => $temp_attachs, 'doc_id' => $doc_id, 'cc_response' => $cc_response]);
        }

        if (null == $email_verification) {

          if (null != $request) {

            if ($request->status == "En revisión") {
              // Update request status
              DB::table('portal__pc_request')->where('request_token', $token)->update(['status' => "En proceso"]);

              $this->processPCRequest($request, $person);

              // Return to forum
              Session::flash('success', "Hola! Su consulta se encuentra en proceso, analizaremos la información que nos ha enviado para proveerle una respuesta a la brevedad posible. Se le ha enviado a la dirección de correo electrónico ingresada el enlace para que pueda mantenerse pendiente y verificar el estado de su consulta, y por medio de esta conversación privada se le hará llegar la respuesta correspondiente.");

              return Redirect::to('foro/pc/'.$token);

            } else {

              $comments = PCRequestForum::All()->where('pc_request_id', $request->id_pc_request);

              if (Auth::check()) {
                  $user_logged = 1;    
              } else {
                  $user_logged = 0;
              }

              $comments_counter = 0;
              foreach ($comments as $comment) {
                $comments_counter++;
              }

              $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));
              $doc_name = "doccc-".$request->correlative."-".$request->year;
              $response = $dms_request->getCCDocumentByName($doc_name, $request->correlative, $request->year, $request->id_pc_request);
              $content = json_decode($response, true);

              if (!$content['success']) {
                return Redirect::to('informacion/no-encontrado');
              }

              $attachs = $dms_request->getDocumentAttachs($content['data']['id']);
              $attachments = json_decode($attachs, true);
              
              $attach_files = array();
              if (isset($attachments['success']) && $attachments['success']) {
                if(!empty($attachments['data'])){
                  $attach_files = $attachments['data'];
                }
              }

              $doc_id = $content['data']['id'];
              //var_dump($content['data']);
              $attributes = $content['data']['attributes'];
              $cc_response = "";
              foreach ($attributes as $attr) {
                if ($attr['id'] == 85 && $content['data']['is_published']) {
                  $cc_response = $attr['value'];
                }
              }

              //$doc_status = $content['data']['status_name'];
              //$doc_status_date = $content['data']['workflow_status_date'];

              $doc_status = isset($content['data']['status_name']) ? $content['data']['status_name'] : "N/A";
              $doc_status_date = isset($content['data']['workflow_status_date']) ? $content['data']['workflow_status_date'] : "N/A";

              $temp_attachs = array();
              if (!empty($attach_files)) {
                foreach ($attach_files as $attach) {
                  $download_saved = DownloadLog::where("attach_id", $attach['download'])->first();

                  if ($download_saved) {
                    $timestamp = strtotime($download_saved->created_at);
                    $date_created_at = date('d-m-Y H:i:s',$timestamp);
                    
                    $data_saved = array('attach_id' => $download_saved->attach_id, 'created_at' => $date_created_at);
                    $attach['saved_log'] = $data_saved;
                  }

                  $temp_attachs[] = $attach;  
                }
              }

              return view('consultation/forum', ['person' => $person, 'request' => $request, 'comments' => $comments, 'user_logged' => $user_logged, 'comments_counter' => $comments_counter, 'doc_status' => $doc_status, 'doc_status_date' => $doc_status_date, 'attach_files' => $temp_attachs, 'doc_id' => $doc_id, 'cc_response' => $cc_response]);

            }

          } else {

            // If email_verification and request doesn't exists
            return Redirect::to('informacion/no-encontrado');

          }
          
        }
    }   

    /**
     * Upload attachments to DMS document
     *
     */
    public function uploadAttach($document_id, $attach_name, $attach_comment, $file){
      //$temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
      $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));

      // $thefile = 'request-78/poder_del_representante_o_apoderado.pdf'
      //$file = $temp_path.$thefile;

      $response = $dms_request->uploadDocumentAttach($document_id, $attach_name, $attach_comment, $file);
      $content = json_decode($response, true);

      if ($content['success']) {
        return true;
      } else {
        return false;
      }
    }

    /**
     * Fill array data for a document creation
     * @param Modules/Portal/Entities/PCRequest $request
     * @param Modules/Portal/Entities/PCPerson $person
     * @return array
     */
    public function fillDocumentData($request, $person){
      $document_data = [
            65 => $person->name,
            66 => $person->email,
            67 => $person->phone,
            68 => $request->subject,
            69 => $request->description,
      ];

      return $document_data;
    }

    public function makePCRequestDocument($folderId, $request, $person, $workflow, $category){
      $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();
      $dms_request = new DMSRequest(setting('portal::dms-sip-url'), setting('portal::dms-sip-user'), setting('portal::dms-sip-pass'));

      $document_data = $this->fillDocumentData($request, $person);

      $file = $temp_path.'cc-'.$request->correlative.'-'.$request->year.'/cc-'.$request->correlative.'-'.$request->year.'.pdf';

      if (file_exists($file)) {

        $response = $dms_request->createRequestDocument($folderId, "DOCCC-".$request->correlative."-".$request->year, "Consulta ciudadana", $document_data, $file, $workflow, $category, "cc");
        $content = json_decode($response, true);

        return $content;
        
      } else {
        return false;
      }
    }

    public function createRequestFile($personId, $requestId){
        $temp_path = Storage::disk('temp')->getDriver()->getAdapter()->getPathPrefix();

        $person = PCPerson::where('id_pc_person', $personId)->first();
        $request = PCRequest::where('id_pc_request', $requestId)->first();

        $timestamp = strtotime($request->created_at); // This is when the form was submited with the request
        $request_date = date("d/m/Y", $timestamp);
        $request_hour = date("H:i:s", $timestamp);
        $correlative = $request->id_pc_request;

        // Save pdf into temporary local folder
        $pdf = PDF::loadView('pdf/consultation', [
          'request_date' => $request_date,
          'request_hour' => $request_hour,
          'correlative' => $correlative,
          'request' => $request, 
          'person' => $person, 
        ]);

        return $pdf->save($temp_path.'cc-'.$request->correlative.'-'.$request->year.'/cc-'.$request->correlative.'-'.$request->year.'.pdf');
    }

    /**
     * Verificate if it's necessary create a new row for email verification
     * @var Modules/Portal/Entities/PCPerson $person
     * @return bool
     */
    public function personEmailVerification($person){
        // Last occurence
        $email_verification = DB::table('portal__pc_email_verification')->where("pc_person_id", $person->id_pc_person)->orderBy('updated_at', 'desc')->first();

        if ($email_verification) {
          $actual_date = (int)date(time());
          $email_verification_date = (int)strtotime($email_verification->updated_at);

          $numDays = abs($email_verification_date - $actual_date)/60/60/24;

          if (round($numDays) >= (int)setting('portal::days-to-verification')) {
            return true;
          } else {
            return false;
          }

        } else {
          return true;
        }
        
    }

    /**
     * Request successfully
     */
    public function success(){
        return view('consultation/success');
    }

    /**
     * Request not found
     */
    public function notFound(){
      return view('request/notfound');
    }

}

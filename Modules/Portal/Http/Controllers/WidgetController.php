<?php

namespace Modules\Portal\Http\Controllers;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Modules\Portal\Http\Controllers\DMSRequest;
use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Modules\Portal\Entities\SiteConnection;
use Illuminate\Support\Facades\Cookie;


class WidgetController extends BasePublicController {

	/**
    * You may call this widget in any blade files like this :
    *
    * @widget('TestWidget', ['a' => 'someVal', 'b' => 'foo'])
    */
	public function data() {

		$counter = SiteConnection::find(1);

		if ($counter == null) {
			Cookie::queue('wasConnected', "yes", 60);
			$site_connection = SiteConnection::create([
            	'counter' => "1",
            ]);
		} else {
			if (null !== Cookie::get('wasConnected')) {
				$site_connection = SiteConnection::find(1);
			} else {
				Cookie::queue('wasConnected', "yes", 60);

				$counter_num = (int)$counter->counter + 1;
				DB::table('portal__site_connection')->where('id_site_connection', 1)->update(['counter' => (string)$counter_num]);

				$site_connection = SiteConnection::find(1);
			}
		}

		return ['data' => $site_connection->counter];
	}

}
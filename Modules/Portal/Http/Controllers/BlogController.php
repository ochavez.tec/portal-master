<?php

namespace Modules\Portal\Http\Controllers;

use Auth;
use Modules\Core\Http\Controllers\BasePublicController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class BlogController extends BasePublicController {

	public function index(){

		return view('blog/index');
	}

}
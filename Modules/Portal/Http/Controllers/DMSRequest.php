<?php

namespace Modules\Portal\Http\Controllers;

class DMSRequest {

    /**
     * @var This set the DMS url
     * @access private
     */
    private $server;

    /**
     * @var This set the DMS username
     * @access private
     */
    private $username;

    /**
     * @var This set the DMS user password
     * @access private
     */
    private $password;

    public function __construct($server, $user, $pass){
        $this->server = $server;
        $this->username = $user;
        $this->password = $pass;
    }

    public function login(){
        $username = $this->my_encrypt('encrypt', $this->username);
        $password = $this->my_encrypt('encrypt', $this->password);
        
        $curl_request = curl_init($this->server."/restapi/index.php/login/".$username."/".$password);
        $this->setGet($curl_request);
        $response = curl_exec($curl_request);
        curl_close($curl_request);
        $decoded = json_decode($response, true);
        if(!$decoded['success']) {
            return false;
        } else {
            return $decoded['dms_session'];
        }
    }

    /**
     * simple method to encrypt or decrypt a plain text string
     * initialization vector(IV) has to be the same when encrypting and decrypting
     * 
     * @param string $action: can be 'encrypt' or 'decrypt'
     * @param string $string: string to encrypt or decrypt
     *
     * @return string
     */
    function my_encrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'rT%$fRGT58FFTYUBvfdWs#45?loPCFDRTE12jkss#@?pvDE';
        $secret_iv = 'FRTEsdsdKI48/($%JuMk%s48njD43DfiTrre432';
        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    public function getDMSServer(){
        return $this->server;
    }

    public function setSessionCookie($curl_request, $dms_session){
        $expiration = time()+3600;
        curl_setopt($curl_request, CURLOPT_COOKIEJAR, 'tmp/cookies.txt');
        curl_setopt($curl_request, CURLOPT_COOKIEFILE, 'tmp/cookies.txt');
        curl_setopt($curl_request, CURLOPT_COOKIE, 'mydms_session='.$dms_session.";expiration=".$expiration);
    }

    public function setGet($curl_request){
        curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_request, CURLOPT_CUSTOMREQUEST, "GET");
    }

    public function logout(){
        $curl_request = curl_init($this->server."/restapi/index.php/logout");
        $this->setGet($curl_request);
        $response = curl_exec($curl_request);
        curl_close($curl_request);
    }

    public function getFolder($id){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/folder/".$id);
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getDocumentReview($id, $width = null){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/document/download/".$id."/view");
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getDocumentAttachs($doc_id){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/document/".$doc_id."/files");
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function downloadFileAttach($doc_id, $file_id) {
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/document/".$doc_id."/file/".$file_id);
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getDocumentDownload($id, $width = null){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/document/download/".$id."/doc");
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getFolderChildren($id){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/folder/".$id."/children");
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function createRequestFolder($id, $name, $comment){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/folder/".$id."/createrequestfolder");
              
            curl_setopt($curl_request,CURLOPT_POST, 1); // 1 for a post request
            curl_setopt($curl_request,CURLOPT_POSTFIELDS,"id=$id&name=$name&comment=$comment");
            curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, true);

            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function createRequestDocument($id, $name, $comment, $document_data, $file, $workflow, $category, $type){
        $dms_session = $this->login();

        $doc = file_get_contents($file);

        $fields = array(
            'id' => $id,
            'name' => $name,
            'comment' => $comment,
            'workflow' => $workflow,
            'request-category' => $category,
        );

        if (false !== $dms_session) {

            // files to upload
            $filenames = array($file);
            $files = array();
            foreach ($filenames as $f){
               $files[$f] = file_get_contents($f);
            }

            $url = $this->server."/restapi/index.php/folder/".$id."/createrequestdocument/".$type;
            $curl = curl_init();

            $boundary = uniqid();
            $delimiter = '-------------' . $boundary;
            $post_data = $this->build_data_document_files($boundary, $fields, $files, $document_data);
            
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => 1,
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POST => 1,
              CURLOPT_POSTFIELDS => $post_data,
              CURLOPT_HTTPHEADER => array(
                //"Authorization: Bearer $TOKEN",
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)
              ),  
            ));

            $this->setSessionCookie($curl, $dms_session);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);
            $err = curl_error($curl);
            //var_dump($err);

            curl_close($curl);
            return($response);

        } else {
            $this->logout();
            return false;
        }

    }

    public function uploadDocumentAttach($id, $name, $comment, $file){
        $dms_session = $this->login();

        $doc = file_get_contents($file);

        $fields = array(
            'id' => $id,
            'name' => $name,
            'comment' => $comment
        );

        if (false !== $dms_session) {

            // files to upload
            $filenames = array($file);
            $files = array();
            foreach ($filenames as $f){
               $files[$f] = file_get_contents($f);
            }

            $url = $this->server."/restapi/index.php/document/".$id."/uploadattachment";
            $curl = curl_init();

            $boundary = uniqid();
            $delimiter = '-------------' . $boundary;
            $post_data = $this->build_data_files($boundary, $fields, $files);

            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => 1,
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POST => 1,
              CURLOPT_POSTFIELDS => $post_data,
              CURLOPT_HTTPHEADER => array(
                //"Authorization: Bearer $TOKEN",
                "Content-Type: multipart/form-data; boundary=" . $delimiter,
                "Content-Length: " . strlen($post_data)
              ),  
            ));

            $this->setSessionCookie($curl, $dms_session);

            $response = curl_exec($curl);
            $info = curl_getinfo($curl);

            //var_dump($response);
            $err = curl_error($curl);
            //var_dump($err);

            curl_close($curl);
            return($response);

        } else {
            $this->logout();
            return false;
        }

    }

    public function build_data_files($boundary, $fields, $files){
        $data = '';
        $eol = "\r\n";

        $delimiter = '-------------' . $boundary;

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
        }

        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary'.$eol
                ;

            $data .= $eol;
            $data .= $content . $eol;
        }
        $data .= "--" . $delimiter . "--".$eol;
        return $data;
    }

    public function build_data_document_files($boundary, $fields, $files, $document_data){
        $data = '';
        $eol = "\r\n";

        $delimiter = '-------------' . $boundary;

        if (!empty($document_data)) {
            foreach ($document_data as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
            }    
        }

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
                . $content . $eol;
        }

        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary'.$eol
                ;

            $data .= $eol;
            $data .= $content . $eol;
        }
        $data .= "--" . $delimiter . "--".$eol;
        return $data;
    }

    public function makeASearch($query){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/makesearch");
              
            curl_setopt($curl_request,CURLOPT_POST, 1); // 1 for a post request
            curl_setopt($curl_request,CURLOPT_POSTFIELDS,"query=$query&mode=1");
            curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, true);

            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getDiamond($year, $month){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/diamondbyym/".$year."/".$month);
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getDocumentByName($doc_name) {
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/docbyname/".$doc_name);
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getSIPDocumentByName($doc_name, $correlative, $year, $request_id) {
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/sipdocbyname/".$doc_name."/".$correlative."/".$year."/".$request_id);
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getCCDocumentByName($doc_name, $correlative, $year, $request_id) {
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/ccdocbyname/".$doc_name."/".$correlative."/".$year."/".$request_id);
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }

    public function getMostRecentDocs(){
        $dms_session = $this->login();
        
        if (false !== $dms_session) {
            $curl_request = curl_init($this->server."/restapi/index.php/mostrecentdocs");
            
            $this->setGet($curl_request);
            $this->setSessionCookie($curl_request, $dms_session);

            $response = curl_exec($curl_request);
            curl_close($curl_request);

            $this->logout();

            return $response;
        } else {
            $this->logout();
            return false;
        }
    }
}

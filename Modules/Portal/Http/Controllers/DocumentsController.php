<?php

namespace Modules\Portal\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Modules\Core\Http\Controllers\BasePublicController;
use Illuminate\Http\Request;

class DocumentsController extends BasePublicController
{

    /**
     * Display lastdocuments view.
     *
     * @return \Illuminate\Http\Response
     */
    public function lastdocuments()
    {
        return view('documents/lastdocuments');
    }

    /**
     * Display a downloaded view.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloaded()
    {
        return view('documents/downloaded');
    }
}

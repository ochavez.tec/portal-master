<?php

namespace Modules\Portal\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Modules\Portal\Mail\SendForumEmail;

class SendForumEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, SerializesModels, Queueable;

    public $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details){
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $email = new SendForumEmail($this->details['token'], $this->details['title'], $this->details['subtitle'], $this->details['text_01'], $this->details['text_02'], $this->details['text_03'], $this->details['button'], $this->details['footer']);

        Mail::to($this->details['email'])->send($email);
    }
}

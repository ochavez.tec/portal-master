<?php

namespace Modules\Portal\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Modules\Portal\Mail\SendNotification;
use Modules\Setting\Contracts\Setting;

use Modules\Portal\Http\Controllers\PortalSettings;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, SerializesModels, Queueable;

    protected $details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($details){
        $this->details = $details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(){
        $email = new SendNotification($this->details['token'], $this->details['user'], $this->details['description'], $this->details['date']);

        Mail::to($this->details['to'])->send($email);
    }
}

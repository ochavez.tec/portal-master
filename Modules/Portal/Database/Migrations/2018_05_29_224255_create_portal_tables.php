<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortalTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create table portal__country
        Schema::create('portal__country', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_country');
            $table->char('code', 3);
            $table->string('name', 50);
            $table->timestamps();
        });

        // Create table portal__download_log
        Schema::create('portal__download_log', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_download_log');
            $table->integer('document_id')->unsigned();
            $table->integer('attach_id')->unsigned();
            $table->timestamps();
        });        

        // Create table portal__site_connection
        Schema::create('portal__site_connection', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_site_connection');
            $table->bigInteger('counter');
            $table->timestamps();
        });

        // Create table portal__email_data
        Schema::create('portal__email_data', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_email_data');
            $table->string('title');
            $table->string('subtitle');
            $table->text('text_block_1');
            $table->text('text_block_2');
            $table->text('text_block_3');
            $table->string('button_text');
            $table->text('footer');
            $table->timestamps();
        });

        // Create table portal__person
        Schema::create('portal__person', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_person');
            $table->string('name');
            $table->string('lastname');
            $table->enum('gender', ['Masculino', 'Femenino']);
            $table->enum('person_type', ['Natural', 'Jurídica', 'Natural con representante']);
            $table->string('occupation');
            $table->string('address');
            $table->string('email');
            $table->timestamps();
        });

        // Create table portal__representative
        Schema::create('portal__representative', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_representative');
            $table->integer('person_id')->unsigned();
            $table->string('name');
            $table->string('dui');
            $table->string('address');

            $table->foreign('person_id')->references('id_person')->on('portal__person')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__email_verification
        Schema::create('portal__email_verification', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_email_verification');
            $table->integer('person_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->string('token');

            $table->foreign('person_id')->references('id_person')->on('portal__person')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__request
        Schema::create('portal__request', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_request');
            $table->integer('person_id')->unsigned();
            $table->enum('status', ['En revisión', 'En proceso', 'Completado']);
            $table->string('request_token');
            $table->text('description');

            $table->foreign('person_id')->references('id_person')->on('portal__person')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__request_notification
        Schema::create('portal__request_notification', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_request_notification');
            $table->integer('request_id')->unsigned();            
            $table->string('notification_site')->nullable();
            $table->boolean('sne_notification')->default(0);
            $table->string('sne_ceu')->nullable();

            $table->foreign('request_id')->references('id_request')->on('portal__request')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__person_document
        Schema::create('portal__person_document', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_person_document');
            $table->integer('person_id')->unsigned();
            $table->enum('document_type', ['DUI', 'Carnet de residente', 'Carnet de minoridad', 'Pasaporte']);
            $table->boolean('is_foreign')->default(0);
            $table->string('document_number')->nullable();
            $table->string('extended_by')->nullable();

            $table->foreign('person_id')->references('id_person')->on('portal__person')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__person_phone
        Schema::create('portal__person_phone', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_person_phone');
            $table->integer('person_id')->unsigned();
            $table->string('cell_phone');
            $table->string('home_phone');
            $table->string('office_phone');

            $table->foreign('person_id')->references('id_person')->on('portal__person')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__information_access
        Schema::create('portal__information_access', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_information_access');
            $table->integer('request_id')->unsigned();
            $table->string('access_one');
            $table->string('access_two');

            $table->foreign('request_id')->references('id_request')->on('portal__request')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__request_forum
        Schema::create('portal__request_forum', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_request_forum');
            $table->integer('request_id')->unsigned();
            $table->text('message');
            $table->boolean('is_logged')->default(0);

            $table->foreign('request_id')->references('id_request')->on('portal__request')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__document_download
        Schema::create('portal__document_download', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_document_download');
            $table->integer('document_id')->unsigned();
            $table->string('name');
            $table->string('date');
            $table->bigInteger('downloads');            
            $table->timestamps();
        });


        // Create table portal__pc_person
        Schema::create('portal__pc_person', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_pc_person');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->timestamps();
        });

        // Create table portal__pc_request
        Schema::create('portal__pc_request', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_pc_request');
            $table->integer('pc_person_id')->unsigned();
            $table->enum('status', ['En revisión', 'En proceso', 'Completado']);
            $table->string('request_token');
            $table->string('subject');
            $table->text('description');

            $table->foreign('pc_person_id')->references('id_pc_person')->on('portal__pc_person')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__pc_email_verification
        Schema::create('portal__pc_email_verification', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_pc_email_verification');
            $table->integer('pc_person_id')->unsigned();
            $table->boolean('status')->default(0);
            $table->string('token');

            $table->foreign('pc_person_id')->references('id_pc_person')->on('portal__pc_person')->onDelete('cascade');
            $table->timestamps();
        });

        // Create table portal__pc_request_forum
        Schema::create('portal__pc_request_forum', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id_pc_request_forum');
            $table->integer('pc_request_id')->unsigned();
            $table->text('message');
            $table->boolean('is_logged')->default(0);

            $table->foreign('pc_request_id')->references('id_pc_request')->on('portal__pc_request')->onDelete('cascade');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('portal__country');
        Schema::dropIfExists('portal__request_forum');
        Schema::dropIfExists('portal__information_access');
        Schema::dropIfExists('portal__request_attachment');
        Schema::dropIfExists('portal__information_access');
        Schema::dropIfExists('portal__person_phone');
        Schema::dropIfExists('portal__person_document');
        Schema::dropIfExists('portal__request_notification');
        Schema::dropIfExists('portal__request');
        Schema::dropIfExists('portal__email_verification');
        Schema::dropIfExists('portal__representative');
        Schema::dropIfExists('portal__person');
    }
}

<?php

namespace Modules\Portal\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Setting\Contracts\Setting;
use Modules\Portal\Entities\EmailData;

class SendForumEmail extends Mailable {
    use Queueable, SerializesModels;

    public $token;
    public $title;
    public $subtitle;
    public $text_01;
    public $text_02;
    public $text_03;
    public $button;
    public $footer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $title = "", $subtitle = "", $text_01 = "", $text_02 = "", $text_03 = "", $button = "", $footer = "") {
        $this->token = $token;
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->text_01 = $text_01;
        $this->text_02 = $text_02;
        $this->text_03 = $text_03;
        $this->button = $button;
        $this->footer = $footer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        
        return $this->from(setting('portal::sender-address'))
            ->view('mails.forum')
            ->subject("Enlace al foro");
            
    }
}

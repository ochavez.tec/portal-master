<?php

namespace Modules\Portal\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Setting\Contracts\Setting;

class SendNotification extends Mailable {
    use Queueable, SerializesModels;

    public $token;
    public $user;
    public $description;
    public $date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($token, $user = "", $description = "", $date = "") {
        $this->token = $token;
        $this->user = $user;
        $this->description = $description;
        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        
        return $this->from(setting('portal::sender-address'))
            ->view('mails.notification')
            ->subject("Nuevo mensaje en el foro");
            
    }
}

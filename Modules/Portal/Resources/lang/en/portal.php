<?php

return [
	'general' => 'General configuration',
    'requests' => 'See requests',
    'consultation' => 'See consultations',
    'request.delete' => 'Delete requests data',
    'consultation.delete' => 'Delete consultations data',
];
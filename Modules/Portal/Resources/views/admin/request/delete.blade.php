@extends('layouts.master')

@section('content-header')
    <h1>
        
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    </ol>
@stop

@push('css-stack')
    <style>
        .align-center {
            text-align: center;
        }

        .content-wrapper, .right-side {
            background-color: #31393c;
        }

    </style>
@endpush

@section('content')

    <div class="row">
    <div class="col-md-3 col-xs-12"></div>

    <div class="col-md-6 col-xs-12">
    <div class="box box-danger box-solid">
    <div class="box-header with-border">
    <h3 class="box-title">Borrado de datos</h3>
    </div>
    <div class="box-body">
        
        @if($message_data === "")
        <div class="callout callout-warning lead align-center">
            <h4>Advertencia</h4>
        </div>
        @else
        <div class="callout callout-danger lead align-center">
            <h4>Advertencia</h4>
        </div>
        @endif

        <p><?php printf($message); ?></p>
        <div><?php printf($message_data); ?></div>

        @if($message_data === "")
        <div class="col-md-12">
            <a href="/backend/portal/requests" type="button" class="btn btn-info"><i class="fa fa-chevron-left"></i> Volver</a>
        </div>
        @else
        <div class="col-md-12">
            <a href="/backend/portal/requests" type="button" class="btn btn-info pull-left"><i class="fa fa-chevron-left"></i> Volver</a>

            {!! Form::open(['route' => ['admin.portal.portal.deleteSelectedPerson'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
            <input type="hidden" name="person_id" value="{{$id}}">
            <button type="submit" class="btn btn-danger pull-right"><i class="fa fa-times"></i> Eliminar definitivamente</button>
            {!! Form::close() !!}
        </div>
        @endif

        
    </div>
    </div>
    </div>

    <div class="col-md-3 col-xs-12"></div>
    </div>
    
    
@stop

@push('js-stack')

@endpush
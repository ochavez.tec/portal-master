@extends('layouts.master')

@section('content-header')
    <h1>
        Consultas ciudadanas
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    </ol>
    <br/>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => 'admin.portal.portal.consultation', 'method' => 'GET', 'class' => 'form-inline', 'id' => 'request_filter_form']) !!}
            <div class="form-group">
            <table class="table table-striped">
                <tr>
                    <td width="40%">
                        <label class="control-label">Filtrar personas por año de creación:</label>
                    </td>
                    <td width="40%">
                        <div class="input-group date">
                            <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="persons_year" id="person-year" class="form-control" data-inputmask="'alias': 'yyyy'" required="true" readonly="true" value="{{ $current_year }}" />
                        </div>            
                    </td>
                    <td width="20%">
                        <button type="submit" class="btn btn-info" title="Filtrar solicitantes"><i class="fa fa-filter"></i> Filtrar</button>    
                    </td>
                </tr>
            </table>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => 'admin.portal.portal.consultation', 'method' => 'GET', 'class' => 'form-inline', 'id' => 'request_filter_form']) !!}
            <div class="form-group">
                <table class="table table-striped">
                    <tr>
                        <td width="40%">
                            <label class="control-label">Filtrar consultas por año de creación:</label>
                        </td>
                        <td width="40%">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="requests_year" id="request-year" class="form-control" data-inputmask="'alias': 'yyyy'" required="true" readonly="true" value="{{ $current_year }}" />
                            </div>           
                        </td>
                        <td width="20%">
                            <button type="submit" class="btn btn-success" title="Filtrar solicitudes"><i class="fa fa-filter"></i> Filtrar</button>   
                        </td>
                    </tr>
                </table>            
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@push('css-stack')
    <style>
        .align-center {
            text-align: center;
        }
    </style>
@endpush

@section('content')
    <div class="modal fade modal-default" id="requestModal">
    <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">×</span></button>
    <h4 class="modal-title">Consultas ciudadanas:</h4>
    </div>
    <div class="modal-body">
    <div class="table-responsive">
    <table class="table table-bordered table-striped">
    <thead>
    <tr>
    <th class="align-center">Id</th>
    <th class="align-center">Correlativo</th>
    <th class="align-center">Año</th>
    <th class="align-center">Enlace</th>
    <th class="align-center">Fecha de creación</th>
    <th class="align-center">Acción</th>
    </tr>
    </thead>
    <tbody id="tbody-request">
    </tbody>
    </table>    
    </div>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
    </div>    
    </div>
    </div> 
    <!-- End Modal -->

    <div class="modal fade modal-danger" id="deleteModal">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Borrar consulta</h4>
        </div>
        <div class="modal-body">
        <p class="align-center">¿Realmente desea eliminar la consulta seleccionada? Esta acción no se puede deshacer.</p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-outline delete-definitely">Eliminar definitivamente</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
          <!-- /.modal-dialog -->
    </div>
    <!-- End Modal -->

    @if(isset($persons))
    <div class="row">
    <div class="col-md-12">
    <div class="box box-info box-solid">
    <div class="box-header with-border">
    <h3 class="box-title">Personas registradas en el año {{ $current_year }}:</h3>
    </div>
    <div class="box-body table-responsive">
    <table class="table-datatable table table-bordered table-striped">
    <thead>
    <tr>
    <th class="align-center">Id</th>
    <th class="align-center">Nombre</th>
    <th class="align-center">Correo</th>
    <th class="align-center">Teléfono</th>
    <th class="align-center">Acciones</th>
    </tr>
    </thead>
    <tbody>
        @if(null !== $persons)
            @foreach($persons as $person)
            <?php $total_request = $person->getTotalPCRequest(); ?>
            <tr>
                <td class="align-center">
                    <p>{{ $person->id_pc_person }}</p>
                </td>
                
                <td class="align-center">
                    <p>{{ $person->name }}</p>
                </td>
                
                <td class="align-center">
                    <p>{{ $person->email }}</p>
                </td>

                <td class="align-center">
                    <p class="align-center">{{ $person->phone }}</p>
                </td>
                
                <td class="align-center">                    
                    @if(!(int)$total_request)
                        @if($delete_allowed)
                            <a title="Eliminar" href="/backend/portal/delete-pcperson/{{ $person->id_pc_person }}" data-id="{{ $person->id_pc_person }}" class="btn btn-danger delete-person"><i class="fa fa-times"></i></a>
                        @endif
                    @else
                        <a title="Ver consultas" data-id="{{ $person->id_pc_person }}" class="btn btn-info view-registers"><i class="fa fa-eye"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        @endif
    </tbody>
    </table>

    </div>
    </div>
    </div>
    </div>
    @endif

    @if(isset($requests))
    <div class="box box-success box-solid">
    <div class="box-header with-border">
    <h3 class="box-title">Consultas creadas en el año {{ $current_year }}:</h3>
    </div>
    <div class="box-body table-responsive">
    <table class="table-datatable table table-bordered table-striped">
    <thead>
    <tr>
    <th class="align-center">Id</th>
    <th class="align-center">Correlativo</th>
    <th class="align-center">Persona</th>
    <th class="align-center">Correo</th>
    <th class="align-center">Enlace</th>
    <th class="align-center">Fecha de creación</th>
    <th class="align-center">Eliminar consulta</th>
    </tr>
    </thead>
    <tbody>
        @if(null !== $requests)
            @foreach($requests as $request)
            <tr>
                <td class="align-center" id="{{ 'td-'.$request->id_pc_request }}">
                    <p>{{ $request->id_pc_request }}</p>
                </td>

                <td class="align-center" id="">
                    <p>{{ $request->correlative }}</p>
                </td>
                
                <td>
                    <p>{{ $request->pcPerson->name }}</p>
                </td>

                <td>
                    <p>{{ $request->pcPerson->email }}</p>
                </td>
                
                <td class="align-center">
                    <a type="button" class="btn btn-info" href="{{ '/foro/pc/'.$request->request_token }}" target="_blank">Ver foro</a>
                </td>

                <td class="align-center">
                    <p>{{ $request->created_at }}</p>
                </td>
                
                <td class="align-center">
                    @if($delete_allowed)
                        <a type="button" data-id="{{ $request->id_pc_request }}" class="delete-request btn btn-danger"><i class="fa fa-times"></i></a>
                    @endif
                </td>
            </tr>
            @endforeach
        @endif
    </tbody>
    </table>
    </div>
    </div>
    @endif


    @include('core::partials.delete-modal')
@stop

@push('js-stack')
    <script>
        /* Ajax Call for Person Consultation */
            $('.view-registers').on('click', function(e){

                var person_id = $(this).attr('data-id');

                $.ajax({
                    type: 'GET',
                    url: "/backend/portal/person-consultations/"+person_id,
                    dataType: 'JSON',
                })
                .done(function(data) {
                    var consultations = "";
                    if (data.data) {

                        var user_allowed = data.permission;

                        $.each( data.data, function( request, value ) {
                            consultations += "<tr>";
                            var consultation_id = 0;

                            $.each( value, function( attr, val ) {
                                //console.log(attr + " => " + val);
                                if (attr == 'id') {
                                    consultation_id = val;
                                    consultations += "<td id='td-"+consultation_id+"' class='align-center'>" + consultation_id + "</td>";

                                } else if (attr == 'correlative') {
                                    var correlative = val;
                                    consultations += "<td class='align-center'>" + correlative + "</td>";

                                } else if (attr == 'year') {
                                    var year = val;
                                    consultations += "<td class='align-center'>" + year + "</td>";

                                } else if (attr == "request_token") {
                                    var consultation_token = val;
                                    consultations += "<td class='align-center'><a type='button' class='btn btn-info' href='/foro/pc/"+ consultation_token +"' target='_blank'>Ver conversación</a></td>";

                                } else if (attr == "created_at") {
                                    var consultation_created = val;
                                    consultations += "<td class='align-center'>" + consultation_created + "</td>";

                                }
                                
                            });

                            if (user_allowed) {
                                consultations += "<td class='align-center'><a type='button' data-id='"+consultation_id+"' class='delete-request btn btn-danger'><i class='fa fa-times'></i></a></td>";
                            } else {
                                consultations += "<td class='align-center'></td>";
                            }
                            

                            consultations += "</tr>";
                        });

                    }

                    $("#tbody-request").empty();

                    $("#tbody-request").append(consultations);

                    $('#requestModal').modal('show');
                })
                .fail(function(data) {
                    console.log( "Error ocurred" );
                });
            });

        $( document ).ready(function() {
            $(".table-datatable").DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": true,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "Ningún registro ha sido encontrado",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "Ningún registro disponible",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "search": "Buscar",
                    "paginate": {
                        "first":"Primero",
                        "last":"Último",
                        "next":"Siguiente",
                        "previous":"Anterior"
                    },
                }
            });

            $('#request-year').datepicker({
                  autoclose: true,
                  format: 'yyyy',
                  language: 'es',
                  minViewMode: 2,                  
            });

            $('#person-year').datepicker({
                  autoclose: true,
                  format: 'yyyy',
                  language: 'es',
                  minViewMode: 2,                  
            });

        });

        var global_id = 0;

        $(document).on('click', '.delete-request', function(event){
            event.preventDefault();
            global_id = $(this).attr('data-id');

            $('#deleteModal').modal('toggle');

        });

        $(document).on('click', '.delete-definitely', function(event){
            event.preventDefault();
            $('#deleteModal').modal('hide');
            var key = "deleteconsultation";

            $.ajax({
                type: 'GET',
                url: "/backend/portal/delete-consultation/"+global_id+"/"+key,
                dataType: 'JSON',
            })
            .done(function(data) {
                
                if (data.success) {
                    // Hide current deleted row
                    $("#td-"+global_id).closest('tr').fadeOut('slow');    
                } else {
                    console.log( "Error ocurred" );    
                }
                
            })
            .fail(function() {
                console.log( "Error ocurred" );
            });

        });
    </script>
@endpush
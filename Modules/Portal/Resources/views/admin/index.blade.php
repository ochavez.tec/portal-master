@extends('layouts.master')

@section('content-header')
    <h1>
        Portal
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
    </ol>
@stop

@push('css-stack')
    <style>
        textarea.code {
            width: 100%;
            float: top;
            min-height: 250px;
            overflow: scroll;
            margin: auto;
            display: inline-block;
            background: #F4F4F9;
            outline: none;
            font-family: Courier, sans-serif;
            font-size: 14px;
          }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab"><i class="fa fa-pencil"></i> Estilos</a></li>
              <li class=""><a href="#tab_2" data-toggle="tab"><i class="fa fa-envelope"></i> Emails</a></li>
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                {!! Form::open(['route' => ['admin.portal.portal.storeStyles'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    <div class="box box-success">
                        <div class="box-header with-border">
                          <i class="fa fa-pencil"></i>
                          <h3 class="box-title">CSS:</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Estilos CSS:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($style_data[0])): ?>
                                        <?php $value = $style_data[0]; ?>
                                        {!! Form::textarea("style-css", $value, ['class' => 'code form-control', 'id' => 'css-textarea']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("style-css", "", ['size' => '60x12', 'class' => 'code form-control', 'id' => 'css-textarea']) !!}
                                    <?php endif; ?>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-warning">
                        <div class="box-header with-border">
                          <i class="fa fa-gear"></i>
                          <h3 class="box-title">JavaScript:</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Script JS:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($style_data[1])): ?>
                                        <?php $value = $style_data[1]; ?>
                                        {!! Form::textarea("script-js", $value, ['class' => 'code form-control', 'id' => 'js-textarea']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("script-js", "", ['size' => '60x12', 'class' => 'code form-control', 'id' => 'js-textarea']) !!}
                                    <?php endif; ?> 
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Guardar</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.page.page.index')}}"><i class="fa fa-times"></i> Cancelar</a>
                    </div>

                {!! Form::close() !!}
              </div>
              <div class="tab-pane" id="tab_2">
                {!! Form::open(['route' => ['admin.portal.portal.storeEmails'], 'method' => 'post', 'class' => 'form-horizontal']) !!}
                    <div class="box box-default">
                        <div class="box-header with-border">
                          <i class="fa fa-envelope"></i>
                          <h3 class="box-title">Correo de confirmación:</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Titulo:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[0]["title"])): ?>
                                        <?php $value = $data[0]["title"]; ?>
                                        {!! Form::text("cn_title", $value, ['class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::text("cn_title", "", ['class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Subtitulo:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[0]["subtitle"])): ?>
                                        <?php $value = $data[0]["subtitle"]; ?>
                                        {!! Form::text("cn_subtitle", $value, ['class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::text("cn_subtitle", "", ['class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                        

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Bloque de texto 1:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[0]["text_01"])): ?>
                                        <?php $value = $data[0]["text_01"]; ?>
                                        {!! Form::textarea("cn_text_01", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("cn_text_01", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Bloque de texto 2:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[0]["text_02"])): ?>
                                        <?php $value = $data[0]["text_02"]; ?>
                                        {!! Form::textarea("cn_text_02", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("cn_text_02", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Bloque de texto 3:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[0]["text_03"])): ?>
                                        <?php $value = $data[0]["text_03"]; ?>
                                        {!! Form::textarea("cn_text_03", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("cn_text_03", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Texto del botón:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[0]["button"])): ?>
                                        <?php $value = $data[0]["button"]; ?>
                                        {!! Form::text("cn_button", $value, ['class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::text("cn_button", "", ['class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Texto del pie de página:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[0]["footer"])): ?>
                                        <?php $value = $data[0]["footer"]; ?>
                                        {!! Form::textarea("cn_footer", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("cn_footer", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box box-default">
                        <div class="box-header with-border">
                          <i class="fa fa-envelope-o"></i>
                          <h3 class="box-title">Correo de enlace al foro:</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Titulo:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[1]["title"])): ?>
                                        <?php $value = $data[1]["title"]; ?>
                                        {!! Form::text("fr_title", $value, ['class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::text("fr_title", "", ['class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Subtitulo:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[1]["subtitle"])): ?>
                                        <?php $value = $data[1]["subtitle"]; ?>
                                        {!! Form::text("fr_subtitle", $value, ['class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::text("fr_subtitle", "", ['class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                        

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Bloque de texto 1:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[1]["text_01"])): ?>
                                        <?php $value = $data[1]["text_01"]; ?>
                                        {!! Form::textarea("fr_text_01", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("fr_text_01", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Bloque de texto 2:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[1]["text_02"])): ?>
                                        <?php $value = $data[1]["text_02"]; ?>
                                        {!! Form::textarea("fr_text_02", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("fr_text_02", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Bloque de texto 3:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[1]["text_03"])): ?>
                                        <?php $value = $data[1]["text_03"]; ?>
                                        {!! Form::textarea("fr_text_03", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("fr_text_03", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Texto del botón:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[1]["button"])): ?>
                                        <?php $value = $data[1]["button"]; ?>
                                        {!! Form::text("fr_button", $value, ['class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::text("fr_button", "", ['class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-xs-12 control-label">Texto del pie de página:</label>
                                <div class="col-sm-7 col-xs-12">
                                    <?php if (isset($data[1]["footer"])): ?>
                                        <?php $value = $data[1]["footer"]; ?>
                                        {!! Form::textarea("fr_footer", $value, ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php else: ?>
                                        {!! Form::textarea("fr_footer", "", ['size' => '30x6', 'class' => 'form-control']) !!}
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Guardar</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ URL::route('admin.page.page.index')}}"><i class="fa fa-times"></i> Cancelar</a>
                    </div>

                {!! Form::close() !!}
              </div>
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@push('js-stack')
    
@endpush


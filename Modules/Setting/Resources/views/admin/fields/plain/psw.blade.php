<div class='form-group'>
    {!! Form::label($settingName, trans($moduleInfo['description'])) !!}
    <?php if ($dbSettings[$settingName]->plainValue !== null): ?>
        <input type="password" name="{{ $settingName }}" class="form-control" value="{{ $dbSettings[$settingName]->plainValue }}" id="{{ $settingName }}" />
    <?php else: ?>
        <input type="password" name="{{ $settingName }}" class="form-control" id="{{ $settingName }}" />
    <?php endif; ?>
</div>

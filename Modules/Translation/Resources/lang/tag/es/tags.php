<?php

return [
    'list resource' => 'Listado de etiquetas',
    'create resource' => 'Create etiquetas',
    'edit resource' => 'Edit etiquetas',
    'destroy resource' => 'Destroy etiquetas',
    'tags' => 'Etiquetas',
    'tag' => 'Etiqueta',
    'create tag' => 'Crear una etiqueta',
    'edit tag' => 'Editar una etiqueta',
    'name' => 'Nombre',
    'slug' => 'URL amigable',
    'namespace' => 'Espacio de nombres',
];

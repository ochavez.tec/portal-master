$(document).ready(function(){
	//Date picker
	$("#diamond-date").inputmask("mm/yyyy", {"placeholder": "mm/yyyy"});

	$('#diamond-date').datepicker({
		language: 'es',
		format: 'mm-yyyy',
	    autoclose: true,
	    minViewMode: 1,
	});
});
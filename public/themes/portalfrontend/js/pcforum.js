$(document).ready(function(){
	var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	$('#request_comment_form').on('submit', function(event){		
		event.preventDefault();
		$("#request_comment").attr('readonly', true);
		$("#request_comment").attr('disabled', true);
		$("#div-overlay").removeClass('hidden');

		var url = $(this).attr('action');
		var gender = $("#gender").val();
		var request_id = $("#request_id").val();
		var comment = $("#request_comment").val();

		$.ajax({
		    method: 'POST',
		    url: url,
		    data: {
		    	_token: CSRF_TOKEN, 
		    	request_id: request_id,
		    	comment: comment,
		    },
		    dataType: 'JSON',
		    success: function (data) {
			    if (data.success) {
			    	if (data.is_logged) {
			    		var user_image = '<img class="img-circle img-sm" src="/assets/media/technical.png" alt="User Image">';
			    		var message = '<div class="comment-text">'+
				    		'<span class="username">Agente de soporte<span class="text-muted pull-right">'+
				    		data.created_at+
				    		'</span></span>'+data.msg+
				    		'</div>';
			    	} else {
				    	if (gender == "Masculino") {
				    		var user_image = '<img class="img-circle img-sm" src="/assets/media/male.png" alt="User Image">'; 	
				    	} else if(gender == "Femenino") {
				    		var user_image = '<img class="img-circle img-sm" src="/assets/media/female.png" alt="User Image">';
				    	}
				    	
				    	var message = '<div class="comment-text">'+
				    		'<span class="username">Persona consultante<span class="text-muted pull-right">'+
				    		data.created_at+
				    		'</span></span>'+data.msg+
				    		'</div>';
				    }

			    	$("#request_comment").val("");
			    	var comments_counter = parseInt($("#info-box-number").text());

			    	$("#info-box-number").text(comments_counter++);
			    	$("#comments-box").append(user_image, message);

			    	$("#request_comment").removeAttr('readonly');
			    	$("#request_comment").removeAttr('disabled');
					$("#div-overlay").addClass('hidden');				
			    
			    } else {
			    	$("#request_comment").removeAttr('readonly');
			    	$("#request_comment").removeAttr('disabled');
					$("#div-overlay").addClass('hidden');
			    }
		    }
    	});
    });

    $('.get-attach').on('touchstart mousedown', function(e){
		event.preventDefault();

		var url = $("#save_log").attr('action');
		var attach_id = $(this).attr('rel');
		var document_id = $(this).attr('data-docid');

		$.ajax({
		    method: 'POST',
		    url: url,
		    data: {
		    	_token: CSRF_TOKEN, 
		    	attach_id: attach_id,
		    	document_id: document_id,		    	
		    },
		    dataType: 'JSON',
		    success: function (data) {
		    	if (data.success) {
			    	$("#span-"+attach_id).text("Documento descargado por primera vez: "+data.created_at);	
				}
		    }
    	});

		window.location = $(this).attr('href');

	});
	
});
$(document).ready(function() {
    $('#requestForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            // This option will not ignore invisible fields which belong to inactive panels
            excluded: ':disabled',
            fields: {
                lastname: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Apellido (s)" es requerido'
                        }
                    }
                },
                name: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Nombre (s)" es requerido'
                        }
                    }
                },
                gender: {
                    validators: {
                        notEmpty: {
                            message: 'Debe seleccionar su género'
                        }
                    }
                },
                occupation: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Ocupación" es requerido'
                        }
                    }
                },
                cell_phone: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Teléfono celular" es requerido'
                        }
                    }
                },
                person_address: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Domicilio" es requerido'
                        }
                    }
                },
                info_requested: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'Campo "Correo electrónico" es requerido'
                        }
                    }
                },
                notifications_target: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                document_number: {
                    validators: {
                        notEmpty: {
                            message: 'Campo requerido'
                        }
                    }
                },
                document_pdf: {
                    validators: {
                        notEmpty: {
                            message: 'Debe adjuntar el documento de identidad'
                        }
                    }
                },
            }
        })
        .bootstrapWizard({
            tabClass: 'nav nav-pills',
            onTabClick: function(tab, navigation, index) {
                return validateTab(index);
            },
            onNext: function(tab, navigation, index) {
                var numTabs    = $('#requestForm').find('.tab-pane').length,
                    isValidTab = validateTab(index - 1);
                if (!isValidTab) {
                    return false;
                }

                if (index === numTabs) {
                    // We are at the last tab
                    // The hidden input for signature shoudn't be empty
                    if ($('#signature_upload').get(0).files.length === 0 && $("#signature-img").val() === "null") {
                        $("#signature-msg-empty").show("slow");
                        return false;
                    } else {
                        $('#requestForm').formValidation('defaultSubmit');
                    }
                    //$('#requestForm').formValidation('defaultSubmit');
                }

                return true;
            },
            onPrevious: function(tab, navigation, index) {            
                return validateTab(index + 1);
            },
            onTabShow: function(tab, navigation, index) {
                // Scroll to top
                $('html, body').animate({ scrollTop: $(".form-horizontal").offset().top }, 1000);

                // Update the label of Next button when we are at the last tab
                var numTabs = $('#requestForm').find('.tab-pane').length;
                $('#requestForm')
                    .find('.next')
                        .removeClass('disabled')    // Enable the Next button
                        .find('a')
                        .html(index === numTabs - 1 ? 'Finalizar' : 'Siguiente');
            }
        });

    function validateTab(index) {
        var fv   = $('#requestForm').data('formValidation'),
            // The current tab
            $tab = $('#requestForm').find('.tab-pane').eq(index);

        // Validate the container
        fv.validateContainer($tab);

        var isValidStep = fv.isValidContainer($tab);
        if (isValidStep === false || isValidStep === null) {
            // Do not jump to the target tab
            return false;
        }

        return true;
    }
});

$(function() {
    //Datepicker
    $('.datepicker').datepicker({ autoclose: true, language: 'es' });
    $('.datepicker').datepicker({ format: 'dd/mm/yyyy' });
    $('.datepicker').datepicker('update', new Date());
    $('.datepicker').datepicker('enableOnReadonly', true);

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false,
      minuteStep: 1,
    });

    $('#is_foreign').click(function(){
        if($(this).is(':checked')){
            $("#country_foreign_passport").fadeIn( "slow");
        } else {
            $("#country_foreign_passport").fadeOut("slow");
        }
    });

    $('#sne_notifications').click(function(){
        if($(this).is(':checked')){
            $("#sne_ceu_box").fadeIn( "slow");
        } else {
            $("#sne_ceu_box").fadeOut("slow");
        }
    });
    
    $('[data-toggle="tooltip"]').tooltip();

    $("#people_type").change(function(){
        var current_selection = $(this).val();
        switch (current_selection) {
            case "1":
                $("#have_representant").fadeOut("slow");
                break;
            case "2":
                $("#have_representant").fadeIn("slow");
                break;
            case "3":
                $("#have_representant").fadeIn("slow");
                break;
        }
    });

    $("#document_type").change(function(){
        $("#document_type option:selected").each(function(){
            var current_selection = $(this).val();
            switch (current_selection) {
                case "1":
                    $("#doc_number").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                case "2":
                    $("#doc_number").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                case "3":
                    $("#doc_number").fadeIn("slow");
                    $("#doc_student").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                case "4":
                    $("#checkbox_is_foreign").fadeIn("slow");
                    $("#doc_number").fadeIn("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;

                default:
                    $("#doc_number").fadeIn("slow");
                    $("#checkbox_is_foreign").fadeOut("slow");
                    $("#doc_student").fadeOut("slow");
                    $("#country_foreign_passport").fadeOut("slow");
                    break;
            }
        });
    });

    $(".phone-number").keypress(function(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    });

    var counter_check = 0;
    $(document).on('click','.check_no',function() {
        if ( counter_check < 2){
            counter_check++;
            $(this).removeClass("check_no").addClass("check_yes");
            if (counter_check == 2){
                $(".check_no").prop('disabled', true);
            }
        }
    });

    $(document).on('click','.check_yes',function() {
        $(this).removeClass("check_yes").addClass("check_no");
        counter_check--;
        $(this).removeClass("check_yes").addClass("check_no");
        $(".check_no").prop('disabled', false);
    });

});
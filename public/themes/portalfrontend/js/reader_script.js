$(document).on('click', '.view-doc', function(event) {
    event.preventDefault();
	var doc = $(this).attr("data-id")

    var parser = new UAParser();
    var result = parser.getResult();

    if (String(result.browser.name) == 'Edge' || String(result.browser.name) == 'IE'){
        window.open("/lectura/"+doc, "_blank");
    } else {
        $("#reader-iframe").attr("src", "");
        document.getElementById('reader-iframe').contentWindow.location.reload();
        $("#reader-iframe").attr("src", "/pdfviewer/web/viewer.html?file=/documento/"+doc);
        $('#docModal').modal('show');   
    }
});

$('[data-toggle="popover"]').popover();
$('[data-toggle="tooltip"]').tooltip();

$('.button-folder').on("click",function(event) {

    var folder_id = $(this).attr("rel");

    if ($("#div-folder-"+folder_id).hasClass("close")) {
        $("div").removeClass("box-success box-warning");
        $("div").addClass("box-primary");
        
        $("#div-folder-"+folder_id).css({"display":"block"});

        $("#i-"+folder_id).removeClass("fa-plus");
        $("#i-"+folder_id).addClass("fa-minus");

        $("#div-folder-"+folder_id).removeClass("close");
        $("#div-folder-"+folder_id).addClass("open");

        $("#parent-div-"+folder_id).removeClass("box-primary box-success collapsed-box");
        $("#parent-div-"+folder_id).addClass("box-warning");

        $("#parent-icon-"+folder_id).removeClass("fa-folder-o").addClass("fa-folder-open-o");

        $("div", "#div-folder-"+folder_id).removeClass("box-primary");
        $("div", "#div-folder-"+folder_id).addClass("box-success");

        $(".blank-space").remove();
        $("h3", "#div-folder-"+folder_id).prepend("<span class='blank-space'>--&nbsp;</span>");

    } else {

        $("div").removeClass("box-success box-warning");
        $(".blank-space").remove();

        $("div").addClass("box-primary");

        $("#div-folder-"+folder_id).css({"display":"none"});

        $("#i-"+folder_id).removeClass("fa-minus");
        $("#i-"+folder_id).addClass("fa-plus");

        $("#div-folder-"+folder_id).removeClass("open");
        $("#div-folder-"+folder_id).addClass("close");

        $("#parent-div-"+folder_id).removeClass("box-warning box-success");
        $("#parent-div-"+folder_id).addClass("box-primary collapsed-box");
        
        $("#parent-icon-"+folder_id).removeClass("fa-folder-open-o").addClass("fa-folder-o");
    }

});

/* Ajax Call for Document Attachments */
$('.see-attachments').on('click', function(e){
        var doc_id = $(this).attr('data-id');
        $.ajax({
            type: 'GET',
            url: "/retrieveAttachs/"+doc_id,
            dataType: 'JSON',
        })
        .done(function(data) {
            if (data.success) {
                var attachs = "";
                var type = "";
                
                $.each( data.data, function( attach, value ) {
                    
                    // The is_public must be 1
                    if (parseInt(value.is_public) == 1) {

                        attachs += "<tr>";

                        $.each( value, function( attr, val ) {
                            //if ((attr == "is_public") && attr === '1') {
                                if (attr == "file_type") {
                                    type = val;
                                    if (val === 1) {                                
                                        attachs += "<td class='align-center'><i class='fa fa-file-pdf-o fa-2x color-red'></i></td>";
                                    } else if (val === 2) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-word-o fa-2x color-blue'></i></td>";
                                    }  else if (val === 3) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-excel-o fa-2x color-green'></i></td>";
                                    }  else if (val === 4) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-powerpoint-o fa-2x color-orange'></i></td>";
                                    }  else if (val === 5) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-word-o fa-2x color-blue'></i></td>";
                                    }  else if (val === 6) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-excel-o fa-2x color-green'></i></td>";
                                    }  else if (val === 7) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-powerpoint-o fa-2x color-orange'></i></td>";
                                    }  else if (val === 8) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-zip-o fa-2x color-brown'></i></td>";
                                    }  else if (val === 9) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-zip-o fa-2x color-brown'></i></td>";
                                    }  else if (val === 10) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-audio-o fa-2x color-gray'></i></td>";
                                    }  else if (val === 11) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-audio-o fa-2x color-gray'></i></td>";
                                    }  else if (val === 12) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-movie-o fa-2x color-gray'></i></td>";
                                    }  else if (val === 13) {
                                        attachs += "<td class='align-center'><i class='fa fa-file-movie-o fa-2x color-gray'></i></td>";
                                    } else {
                                        attachs += "<td class='align-center'><i class='fa fa-file fa-2x color-skyblue'></i></td>"
                                    }
                                    
                                } else if (attr == "name") {
                                    attachs += "<td class=''>"+ val +"</td>";                            
                                } else if (attr == "comment") {
                                    //var trimmed = jQuery.trim(val).substring(0, 50).split(" ").slice(0, -1).join(" ") + "...";
                                    attachs += "<td class=''>" + val + "</td>";
                                } else if (attr == "download") {
                                    attachs += "<td class=''><a href='/downloadattach/"+doc_id+"/"+type+"/"+val+"' class='btn btn-success'><i class='fa fa-download'></i></td>";    
                                }
                                        
                        });

                        attachs += "</tr>";
                    }
                    
                });

                $("#tbody-attachments").empty();
                $("#tbody-attachments").append(attachs);
                $('#attachsModal').modal('show');
            } else {
                console.log("File not exists");
            }
        })
        .fail(function(data) {
            console.log( "Error ocurred" );
        });
});
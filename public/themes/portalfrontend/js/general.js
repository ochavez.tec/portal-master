function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

$(".phone-number").keypress(function(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
	    return false;
	return true;
});

$("#cc_form").on("submit", function(){
	$("#div-overlay").removeClass("hidden");
});

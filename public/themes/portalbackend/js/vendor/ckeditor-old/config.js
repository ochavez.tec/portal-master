/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
    config.skin = 'bootstrapck';

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'styles' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		
		{ name: 'colors' },
		
		{ name: 'forms' },
		{ name: 'others' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'document', 'doctools', 'mode' ] }
		
	];


	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	config.bodyId = 'contents_id';
	
	config.defaultLanguage = 'es';
	
	config.height = 790;        // 790 pixels.

	config.extraPlugins = 'sourcearea';

	config.contentsCss = [	'/themes/portalbackend/vendor/bootstrap/dist/css/bootstrap.min.css', 
							'/themes/portalbackend/vendor/font-awesome/css/font-awesome.min.css',
							'/themes/portalbackend/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css',
							'/themes/portalbackend/vendor/admin-lte/dist/css/AdminLTE.css',
							'/themes/portalbackend/vendor/admin-lte/dist/css/skins/_all-skins.min.css',
							'/themes/portalbackend/css/general.css',
						];

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	config.allowedContent = true;
	config.filebrowserBrowseUrl =  Asgard.mediaGridCkEditor;
};

/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.dtd.$removeEmpty.span = false; 
CKEDITOR.dtd.$removeEmpty.i = false;

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
	// The toolbar groups arrangement, optimized for two toolbar rows.
	/*config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'styles' },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		
		{ name: 'colors' },
		
		{ name: 'forms' },
		{ name: 'others' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'document', 'doctools', 'mode' ] }
		
	];*/

	config.toolbarGroups = [
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert'] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'tools', groups: [ 'tools', 'ckawesome' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] },
		{ name: 'document', groups: [ 'document', 'doctools', 'mode' ] },
	];

	config.removeButtons = 'Underline,Subscript,Superscript,Templates,Save,NewPage,Preview,Print,About,PasteFromWord,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Subscript,Superscript,BidiLtr,BidiRtl,Language,Flash,Smiley,PageBreak,Iframe,ShowBlocks';

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';

	config.bodyId = 'contents_id';
	
	config.defaultLanguage = 'es';
	
	config.height = 790;        // 790 pixels.

	config.extraPlugins = 'sourcearea,tableresize,youtube,ckawesome,htmlwriter,codemirror';
	config.youtube_responsive = true;
	config.youtube_related = true;
	config.youtube_privacy = false;
	config.youtube_autoplay = false;
	config.youtube_controls = true;

	// Especial: ALLOW <i></i>
	//config.protectedSource.push(/<i[^>]*><\/i>/g);

	config.fontawesomePath = '/themes/portalbackend/vendor/font-awesome/css/font-awesome.min.css';
	

	config.contentsCss = [	
		'/themes/portalbackend/vendor/bootstrap/dist/css/bootstrap.min.css',
		'/themes/portalbackend/vendor/bootstrap/dist/css/bootstrap-theme.min.css',		
		'/themes/portalbackend/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css',
		'/themes/portalbackend/vendor/admin-lte/dist/css/AdminLTE.css',
		'/themes/portalbackend/vendor/admin-lte/dist/css/skins/_all-skins.min.css',
		'/themes/portalbackend/css/general.css',
	];

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
	config.allowedContent = true;
    config.extraAllowedContent = 'p(*)[*]{*};div(*)[*]{*};li(*)[*]{*};ul(*)[*]{*}';
	config.filebrowserBrowseUrl =  Asgard.mediaGridCkEditor;

	// HTML formatting
	config.sourceAreaTabSize = 2;

	config.codemirror = {

	    // Set this to the theme you wish to use (codemirror themes)
	    theme: 'monokai',

	    // Whether or not you want to show line numbers
	    lineNumbers: true,

	    // Whether or not you want to use line wrapping
	    lineWrapping: true,

	    // Whether or not you want to highlight matching braces
	    matchBrackets: true,

	    // Whether or not you want tags to automatically close themselves
	    autoCloseTags: true,

	    // Whether or not you want Brackets to automatically close themselves
	    autoCloseBrackets: true,

	    // Whether or not to enable search tools, CTRL+F (Find), CTRL+SHIFT+F (Replace), CTRL+SHIFT+R (Replace All), CTRL+G (Find Next), CTRL+SHIFT+G (Find Previous)
	    enableSearchTools: true,

	    // Whether or not you wish to enable code folding (requires 'lineNumbers' to be set to 'true')
	    enableCodeFolding: true,

	    // Whether or not to enable code formatting
	    enableCodeFormatting: true,

	    // Whether or not to automatically format code should be done when the editor is loaded
	    autoFormatOnStart: true,

	    // Whether or not to automatically format code should be done every time the source view is opened
	    autoFormatOnModeChange: true,

	    // Whether or not to automatically format code which has just been uncommented
	    autoFormatOnUncomment: true,

	    // Define the language specific mode 'htmlmixed' for html including (css, xml, javascript), 'application/x-httpd-php' for php mode including html, or 'text/javascript' for using java script only
	    mode: 'htmlmixed',

	    // Whether or not to show the search Code button on the toolbar
	    showSearchButton: true,

	    // Whether or not to show Trailing Spaces
	    showTrailingSpace: true,

	    // Whether or not to highlight all matches of current word/selection
	    highlightMatches: true,

	    // Whether or not to show the format button on the toolbar
	    showFormatButton: true,

	    // Whether or not to show the comment button on the toolbar
	    showCommentButton: true,

	    // Whether or not to show the uncomment button on the toolbar
	    showUncommentButton: true,

	    // Whether or not to show the showAutoCompleteButton button on the toolbar
	    showAutoCompleteButton: true,
	    // Whether or not to highlight the currently active line
	    styleActiveLine: true
	};
};


